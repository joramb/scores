# A LilyPond Project

Einleitung:

- Was ist LilyPond, Vorteile
- Welche Art von Musik, setzen alter Noten

### Struktur und Ausgaben

- Stimmen und Sätze
- Klavier
- Titelseiten, LaTeX?

### Layout

- s. layout (generell)
    - Titel
    - Header, Footer
    - Ränder
    - Schriftarten und -größen, Ligaturen, onum, smcp
    - Systeme
    - Notensymbole
- Edition Engraver

### Musikalischer Inhalt

- aktuelle Syntax verwenden
- Funktionen, die nicht in LilyPond sind
    - ExtendLV
    - after grace?
    - after funktion
    - tightSpacing
    - auto-extender in Lyrics
    - lyric snap
    - Effekt durch includes
- Handarbeit
    - Auflösungszeichen in Alternativen

### Verwaltung

- install + fonts
- git, Versionierung
- make tool
- ly-version update
- visual diff
- lynt linting
