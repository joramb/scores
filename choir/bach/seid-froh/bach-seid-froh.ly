\version "2.25.22"
\language deutsch

#(set-global-staff-size 18.5)

\header {
  author = "Joram Berger"
  copyright = "© 2016 Joram Berger"
  date = "2016-12-08"
}

\include "settings/layout.ily"

\header {
  title = "Seid froh, dieweil"
  subtitle = "35. Choral – Weihnachtsoratorium"
  composer = "J. S. Bach"
  shortcomposer = "Bach"
  pdftitle = "J. S. Bach: Seid froh dieweil"
  subject = "Weihnachtsoratorium, BWV248"
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "BachJS"
  mutopiapoet = "Kaspar Füger"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "Choir (SATB)"
  date = "1734"
  lastupdated = "2013-12-25"
}

global = {
  \key fis \minor
  \time 4/4
  \partial 4
  \autoBeamOff
  \accidentalStyle modern-voice
}

fe = \fermata

la = { \once \override Lyrics.LyricText.self-alignment-X = #-0.8 }

soprano = \relative c' {
  \global
  fis8[ gis] | a4 g fis\fe fis8[ gis] |
  a4 gis fis\fe cis' | h a gis\fe gis |
  a4 a h h | cis cis \la h a |
  gis2 fis4\fe cis'4 | h a8[ gis] gis4\fe cis |
  h4 a gis\fe gis | a a h h |
  cis4 cis h a8[ gis] | gis2 fis4\fe
  \bar "|."
}

alto = \relative c' {
  \global
  cis4 | fis eis fis\fe fis |
  fis eis cis\fe fis | fis8[ eis] fis4 eis\fe eis |
  fis fis fis8([ gis16 a]) gis4 | gis8[ fis] eis[ fis] \la gis4. fis8 |
  fis4( eis) cis\fe fis | fis8[ eis] fis4 eis\fe fis8[ e] |
  dis8[ e] fis[ dis] e4\fe gis8[ eis] | fis4 a8[ fis] d4 h'8[ gis] |
  a8[ gis] fis4 fis8[ eis] fis4 | fis8([ eis16 dis] eis4) cis\fe
}

tenor = \relative c' {
  \global
  a8[ h] | cis4 cis8[ h] a4\fe h |
  cis cis8[ h] a4\fe a | h cis cis\fe cis |
  cis4 d d8[ e16 fis] e8[ d] | cis4 h8([ a]) d([ cis]) cis([ d]) |
  d8([ h gis cis]) a4\fe a | h8[ cis] dis4 cis\fe cis |
  fis,4 h h\fe cis | cis8([ a]) d4 d8([ h]) e4 |
  e8[ cis] fis[ e] d[ cis] dis4 | cis8([ gis cis h]) a4\fe
}

bass = \relative c {
  \global
  fis4 | fis16([ gis a h cis8]) cis, d4\fe d |
  cis8[ h] cis4 fis,\fe fis' | gis a8[ h] cis4\fe cis, |
  fis8[ e] d[ cis] d[ h] e4 | a,8([ a']) gis[ fis] fis([ eis]) fis[ d] |
  h8([ gis] cis4) fis,\fe fis'8[ e] | d[ cis] his4 cis\fe a |
  h8[ cis] dis[ h] e4\fe eis8[ cis] | fis4 fis8([ d]) g4 gis8[ e] |
  a4 ais8[ fis] h4 his8([ gis]) | cis4( cis,) fis\fe
}


text = \lyricmode {
  Seid froh die -- weil, seid froh die -- weil,
  dass eu -- er Heil ist hie ein Gott
  und auch ein  Mensch ge -- bo -- ren,
  der, wel -- cher ist der Herr und Christ
  in __ Da -- vids Stadt,
  von vie -- len aus -- er -- ko -- ren.
}

\layout {
    \Lyrics
    \override LyricExtender.minimum-length = #1.5
}

\book{
  \bookOutputName "Bach_SeidFroh"
  \score {
    \new ChoirStaff \with { midiInstrument = "choir aahs" } <<
      \new Staff \with { instrumentName = "Sopran" } \new Voice = "S" \soprano
      \new Lyrics \lyricsto "S" \text
      \new Staff \with { instrumentName = "Alt" } \alto
      \addlyrics \text
      \new Staff \with { instrumentName = "Tenor" } { \clef "treble_8" \tenor }
      \addlyrics \text
      \new Staff \with { instrumentName = "Bass" } { \clef "bass" \bass } 
      \addlyrics \text
    >>
    \layout { }
    \midi {
      \tempo 4 = 100
    }
  }
}

%{
% MIDI-Dateien zum Proben:
\book {
  \bookOutputSuffix "Sopran"
  \score {
    \rehearsalMidi "soprano" "oboe" \text
    \midi { }
  }
}

\book {
  \bookOutputSuffix "Alt"
  \score {
    \rehearsalMidi "alto" "oboe" \text
    \midi { }
  }
}

\book {
  \bookOutputSuffix "Tenor"
  \score {
    \rehearsalMidi "tenor" "oboe" \text
    \midi { }
  }
}

%\midifor #"bass" #"oboe" #\text

\book {
  \bookOutputSuffix "Bass"
  \score {
    \rehearsalMidi "bass" "oboe" \text
    \midi { }
  }
}
%}
