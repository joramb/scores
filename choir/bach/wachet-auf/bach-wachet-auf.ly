\version "2.25.22"
\language deutsch
\pointAndClickOff

\header {
  title = "Wachet auf, ruft uns die Stimme"
  subtitle = "Cantata 140"
  subsubtitle ="27. Sonntag nach Trinitatis"
  poet = "Philipp Nicolai, 1599"
  composer = "Johann Sebastian Bach, 1731"
  %meter ="Allegro moderato"
  %arranger ="Joram"
  piece = \markup{\bold "1. Chorus"}
  opus = "BWV 140"
  tagline = \markup {
    "Lilypond"
    \with-url "http://www.lilypond.org" "www.lilypond.org"
  }
  copyright = \markup {
    "© 2013"
    \with-url "http://joramberger.de" "Joram Berger"
    "– Lizenz:" \small
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/" "CC-BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "BachJS"
  mutopiapoet = "Johann Rist"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "choir"
  date = "2013-12-25"
  %footer = "Mutopia-2012/12/00-000"
  %tagline = \markup { \override #'(box-padding . 1.0) \override #'(baseline-skip . 2.7) \box \center-column { \small \line { Sheet music from \with-url "http://www.MutopiaProject.org" \line { \teeny www. \hspace #-1.0 MutopiaProject \hspace #-1.0 \teeny .org \hspace #0.5 } • \hspace #0.5 \italic Free to download, with the \italic freedom to distribute, modify and perform. } \line { \small \line { Typeset using \with-url "http://www.LilyPond.org" \line { \teeny www. \hspace #-1.0 LilyPond \hspace #-1.0 \teeny .org } by Joram Berger \hspace #-1.0 . \hspace #0.5 Reference: \footer } } \line { \teeny \line { This sheet music has been placed in the public domain by the typesetter, for details see: \hspace #-0.5 \with-url "http://creativecommons.org/licenses/publicdomain" http://creativecommons.org/licenses/publicdomain } } } }
}

#(set-global-staff-size 18.0)

\paper {
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  last-bottom-spacing.basic-distance = 12
  myStaffSize = #18.0
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
}

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L italic"
  \override Staff.ClefModifier.extra-offset = #'(-0.3 . 0.07)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
}

%TODO: einzeln midi, stimmen anordnen, Text ([] einführen), als gesang d.h. auflösungszeichen pro stimme, Taktnummern richtig, jede Zeile mit Notenwert beginnen, keine unnötigen Notenwerte, | octavecheck pro seite, zeile pro zeile, mybreak, abschlusstaktstrich, DS

br = {} % {\break}

trackAchannelA =  {
  \time 3/4
  \key es \major
  \tempo "Allegro moderato" 4 = 80
}

%trackA = <<
%  \context Voice = sop \trackAchannelA
%>>

%Sopran
sopranonotes = \relative c {
  \time 3/4
  \key es \major
  R2.*16 | \br
  % 17
  es'2. g b b | \br
  % 21
  b b c~ | \br
  % 24
  c b~b | \br
  % 27
  R R b | \br
  % 30
  es b es | \br
  % 33
  g f es | \br
  % 36
  d( c) b~ | \br
  % 39
  b R R | \br
  % 41
  R b2. es | \br
  % 44
  b c g | \br
  % 47
  as f es~ | \br
  % 50
  es~es~es | \br
  % 53
  R2.*15 | \br
  es2. g b b | \br
  b b c~ | \br
  c b~b | \br
  R R b | \br
  es b es | \br
  g f es | \br
  d( c) b~| \br
  b R R | \br
  R b es | \br
  b c g | \br
  as f es~ | \br
  es~es~es | \br
  R2.*12 | \br
  b'2. b as | \br
  g f es~| \br
  es2 r4 R2.*3 | \br
  b'2. b as | \br
  g f es2.~ | \br
  es2 r4 | R2.*15 | \br
  f2. g as~| \br
  as g~ g~ | \br
  g R2.*4 | % kein br
  b2. c | \br
  d es~ es | \br
  R2.*4 | % kein br
  es2. g | \br
  f es~es | \br
  R2.*4 | % kein br
  b2. es | \br
  b c g | \br
  as f es~ | \br
  es~ es~ es2 r4 | R2.*16 \bar"|."
}

%nsopran = <<
%  \context Voice = channelA \trackBchannelA
%>>


%trackCchannelA =  {
%
%  % [SEQUENCE_TRACK_NAME] Instrument 2
%
%}

%Alt
altonotes = \relative c {
  \key es \major
  R2.*18 |
  % 19
  r4 b'8 c d b | es4 b' es, |
  % 21
  f4 as2 | r4 g8 f es d  | g f es d c b |
  % 24
  f'4 g8 f es f | d es f4 r4 |
  % 26
  R2.*5 |
  % 31
  r8 d es f g as | b2.~ |
  % 33
  b4 a b | c8 a f4 r8 a | b g c2~ |
  % 36
  c4 f,4 b~ | b a8 g a4 | b8 f d4 r4 |
  % 39
  R2.*4 |
  % 43
  b4 g' r | g c r |
  % 45
  r8 es,8 b' as g f | es des c d es f | g b, es2~ |
  % 48
  es8 c as' g f es | d c b4 d | es r r8 es8 |
  % 51
  des' c b4 es8 g, | as4 es4. as8 | g4 r r |
  % 54
  R2.*17 |
  %71
  r4 b,8 c d b | es4 b' es, |
  % 73
  f as2 | r4 g8 f es d | g f es d c4 |
  % 76
  f g8 f es f | d es f4 r |
  % 78
  R2.*5 |
  % 83
  r8 d es f g as | b2.~ |
  % 85
  b4 a b | c8 a f4 r8 a | b g c2~ |
  % 88
  c4 f,4 b~ | b a8 g a4 | b8 f d4 r |
  % 91
  R2.*4 |
  % 95
  b4 g' r | g c r |
  % 97
  r8 es, b' as g f | es des c d es f g b, es2~ |
  % 100
  es8 c as' g f es | d c b c d4 | es8 b g4 r8 es' |
  % 103
  des'8 c b4 es8 g, | as4 es4. as8 | g4 r r |
  % 106
  R2.*11|
  % 117
  r4 r r8 f |
  % 118
  b8 g es4 r | r8 g g f f es | es es b'4 r8 b, |
  % 121
  b'4 r8 b, es4 | r8 des des c c b | b g' c4 r8 c, |
  % 124
  g'4 r r | R2.*2 | r4 r r8 b, |
  % 128
  b'8 g es'4 r | r8 g, g f f es | es es b'4 r8 b, |
  % 131
  b'4 r8 b, es4 | r8 des des c c b | b g' c4 r8 c, |
  % 134 (oder as in 136?)
  g'4 r r | r r g~ | g4. fis16 g a g fis e | d e fis8~fis16 g16 a8~a16 b16 c8 |
  % 138
  b8 a16 b g8 g16 a b c d8 | g,16 f es f g a g a b a g f | es d c d es2~ |
  % 141
  es16 d c d es d es f g f g a | b8 f d f g16 as b g | es4 r8 es g b |
  % 144
  c16 b as b c as b c f,4 | r8 f d b f'\trill es16 f | g es d es b'2~ |
  % 147
  b8 a4 g f8 | f2.~ | f8 f4 es d8 |
  % 150
  d16 es d es f2~ | f8 e16 d e f g as b c b c | as g f g as b as b c b as g |
  % 153
  f16 es d es f g f g as g f es | d8 f4 es d8 | c es4 d16 c d8 f |
  % 156
  es4 r r | R2.*4 |
  % 161
  r4 r r8 g | es g as4 r8 es |
  % 163
  f8 h d4 r8 h | c4 r4 r8 es,8 | b'4 r8 b g es |
  % 166
  c'4 r r | R2.*2|
  % 169
  r4 r b | c as2 | g4 r g |
  % 172
  as8 g g2 | g4 r8 g g c | b4 g es |
  % 175
  es c'8 b as b | c4 a f | f d' r |
  % 178
  R2. | g,4 r r | g r r |
  % 181
  r8 es b' as g f | es des c d es f | g b, es2~|
  % 184
  es8 c as' g f es | d c b c d4 | es8 b g4 r8 es' |
  % 187
  des'8 c b4 es8 g, | as4 es4. as8 | g4 r r| R2.*16 \bar"|."
}

%nalt = <<
%%  \context Voice = channelA \trackCchannelA
%  \context Voice = channelB \trackCchannelB
%>>


%trackDchannelA =  {
%
%}

%Tenor
tenornotes = \relative c {
  \clef "G_8"
  R2.*19 |
  % 20
  r4 es8 f g es |
  % 21
  b'4 f' b, | g' es2 | r4 g8 f es d |
  % 24
  c4. c8 f,4 | f' d r | R2.*4|
  % 30
  r8 b c d es f | g2.~ | g~ |
  % 33
  g4 f8 es d es | c4 d8 es f d | b4 r r |
  % 36
  r8 b c d es f | g4 f es | d b r |
  % 39
  R2.*4 |
  % 43
  g4 b r | c g' r |
  % 45
  r r8 b, es des | c b as4 es | es8 f g as b g |
  % 48
  c4 f r8 c | b es d c b4 | b8 es, des' c b as |
  % 51
  g4 r8 g as b | es, es as g as c | b4 r r |
  % 54
  R2.*18 |
  % 72
  r4 es,8 f g es |
  % 73
  b'4 f' b, | g' es2 | r4 g8 f es d |
  % 76 (oder as in Takt 76?)
  c4. c8 a f | f'4 d r | R2.*4 |
  % 82
  r8 b c d es f | g2.~| g~ |
  % 85
  g4 f8 es d es | c4 d8 es f d | b4 r r |
  % 88
  r8 b c d es f | g4 f es | d4 b r |
  % 91
  R2.*4 |
  % 95
  g4 b r | c4 g' r |
  % 97
  r4 r8 b, es des | c8 b as4 es | es8 f g as b g |
  % 100
  c4 f r8 c | b8 es d c b4~ | b8 es,8 des' c b as |
  % 103
  g4 r8 g as b | es,8 es as g as c | b4 r r |
  % 106
  R2.*11 |
  % 117
  r4 r8 b f' d |
  % 118
  es4 r r8 b | es4 r r8 b | es4 r8 b f'4 |
  % 121
  r8 es es des des c | c f, b4 r8 b | es4 r8 es as as, |
  % 124
  b4 r r | R2.*2 | r4 r8 b f' d |
  % 128
  es4 r r8 b | es4 r r8 b | es4 r8 b f'4 |
  % 131
  r8 es es des des c | c f, b4 r8 b | es4 r8 es as as, |
  % 134
  b4 r r | R2.*4 |
  % 139
  r4 r b~ | b4. a16 b c b a g |
  % 141
  f g a8~a16 b c8~c16 d es8 | d c16 d b8 c16 d es f g8 | c,16 b as b c d c d es d c b |
  % 144
  as g f g as2~ | as16 g16 f g as g as b c b c d | es8 b g as b16 c d b |
  % 147
  g'4 r8 g, b d | es16 d c d es c d es a,4 | r8 b a f c'\trill b16 c |
  % 150
  d8 f,8~f16 g16 f g as b c as | des8 b4 g c8 | c4 r8 f,16 g as b c8 |
  % 153
  c8 h16 c d es d es f es d c | g'16 g, a h c h c d es d c h | c8 c c h h c |
  % 156
  c4 r r | R2.*4 |
  % 161
  r8 b es d es4 | r8 es as, c f4 |
  % 163
  r8 d h g g'4~ | g r r8 as | es4 r8 g es g |
  % 166
  es4 r r | R2.*2 |
  % 169
  r4 r g | as c,2 | b4 r es |
  % 172
  d h4. c16 d | c4 r8 es c g' | es4 b b |
  % 175
  as4 es'8 des es des | c4 c c | b4 f' r |
  % 178
  R2. | b,4 r r | c4 r r |
  % 181
  r4 r8 b es des | c8 b as4 es | es8 f g as b g |
  % 184
  c4 f r8 c | b es d c b4~ | b8 es, des' c b as |
  % 187
  g4 r8 g as b | es,8 es as g as c | b4 r r | R2.*16 \bar"|."
}

%ntenor = <<
%
%  \clef "treble_8"
%
%  \context Voice = channelA \trackDchannelA
%  \context Voice = channelB \trackDchannelB
%>>


%trackEchannelA =  {
%
%}

%Bass
bassnotes = \relative c {
  \clef bass
  R2.*20
  % 21
  r4 b8 c d b es4
  b'8 as g f es4 es'2 r4 es, f
  b b, r
  R2.*6
  r8 g' as b c d
  es1
  d8 c d4. es16 d c8 f,
  g a b4. a8 g f
  es c f es f4 |
  b b, r |
  R2.*4
  es4 es' r |
  c, c' r8 es, |
  b' as g f es4 as
  as,8 b c d es4 r8 c
  c' b as g f g as f
  b4 b,2 es8 r4 es8 des' c b as g f
  es des c4 as8 b c as
  es'4 r r |
  R2.*19 |
  r4 b8 c d b | es4 b'8 as g f | es4 es'2 |
  r4 es, f | b b, r |
  R2.*6 |
  r8 g' as b c d |
  es2.~ | es4 d8 c d4~ | d8 es16 d c8 f, g a |
  b4. a8 g f| es c f es f4 | b b, r |
  R2.*4 |
  es4 es' r | c, c' r8 es, |
  b'8 as g f es4 | as as,8 b c d es4 r8 c c' b |
  as8 g f g as f | b4 b,2 | es4 r8 es des' c |
  b8 as g f es des | c b as b c as | es'4 r r
  R2.*11 |
  r8 f b d b4 |
  r8 b b as as g | g b es4 r | r8 g, f es es d |
  es c c'4 r | r8 b b as as g | g es as4 r8 as, |
  es'4 r r | R2.*2| r8 f b d b4 |
  r8 b b as as g | g b es4 r | r8 g, f es es d |
  es c c'4 r | r8 b b as as g | g es as4 r8 as, |
  es'4 r r |
  R2.*8 |
  r4 r es~ |
  es4. d16 es f es d c | b c d8~d16 es16 f8~f16 g16 as8 | g f16 g es8 es16 f g as b8 |
  es,16 d c d es f es f g f es d | c b a b c8 f16 g a b c8 | f,16 es d es f g f g a g f es |
  d16 c b c d es d es f es d c | b8 g c4 c | f,8 f'4 es d8 |
  d d'4 c h8 | h4 r16 g, a h c h c d | es d es f g8 f16 es f8 g |
  c,4 r r |
  R2.*4 |
  r4 r8 b' g es | as4 r8 es' d c |
  h4 r8 d h g | c4 r r8 c | g4 r8 b es es, |
  as4 r r |
  R2.*3 |
  r4 r8 as c, d | es16 f g8. f16 g as b as b c |
  d4 d8 h g h | c4 c,8 c es c | g'4 es g |
  as as,8 b' c b | a4 f a | b b, r |
  R2. | es'4 r r | c r r8 es, |
  b'8 as g f es4 | as as,8 b c d | es4 r8 c c' b |
  as g f g as f | b4 b,2 | es4 r8 es des' c |
  b as g f  es des | c b as b c as | es'4 r r | R2.*16 \bar"|."
}

%nbass = <<
%
%  \clef bass
%
%  \context Voice = channelA \trackEchannelA
%  \context Voice = channelB \trackEchannelB
%>>


%trackFchannelA =  {
%
%  % [SEQUENCE_TRACK_NAME] Instrument 5
%
%}

trackFchannelB = \relative c {
  es8 s8 es' s8 d s8 c s8 |
  % 2
  c, s8 b s8 as s8 as' s8 |
  % 3
  f s8 b s8 d, s8 b s8 |
  % 4
  es, s8 es'8. es16 g8. g16 b8 s8 |
  % 5
  b,8. b16 d8. d16 f8 s8 f,8. f'16 |
  % 6
  es8. d16 c8 s8 c,8. c''16 b8. as16 |
  % 7
  g8 es g b g es as g |
  % 8
  f g f es d b es g |
  % 9
  es c f es d es d c |
  % 10
  h g c8. c16 b8. b16 a8. a16 |
  % 11
  a8. a16 a8. a16 as8. as16 as8. as16 |
  % 12
  as8. as16 g8. as16 b4 b |
  % 13
  es,8 s8 es' s8 d s8 c s8 |
  % 14
  b s8 as s8 g s8 g' s8 |
  % 15
  f s8 es s8 d s8 c s8 |
  % 16
  d s8 b s8 d s8 es s8 |
  % 17
  g s8 f s8 es s8 c s8 |
  % 18
  es s8 f s8 es s8 f s8 |
  % 19
  b, s8 b' s8 a s8 g s8 |
  % 20
  g, s8 f s8 es s8 es' s8 |
  % 21
  c s8 f s8 a, s8 f s8 |
  % 22
  b s8 b' s8 as s8 g s8 |
  % 23
  g, s8 f s8 es s1. c'8 d es f g a f |
  % 26
  b a g f g c a f |
  % 27
  g a b a g f es d |
  % 28
  es c f4 f, b8 f' |
  % 29
  b f d b es es, es' f |
  % 30
  g f e g c g e c |
  % 31
  f f, f' g as f d f |
  % 32
  b, c d b es, s8 es'8. es16 |
  % 33
  g8. g16 c8 s8 c,8. c16 es8. es16 |
  % 34
  g8 s8 es8. es16 g8. g16 as8 s8 |
  % 35
  as,8. as16 c8. c16 es8 s8 c' s8 |
  % 36
  b s8 as s8 f s8 as s8 |
  % 37
  b s8 b, s8 b' s8 es, s8 |
  % 38
  b s8 des s8 g es es' des |
  % 39
  c b as g as b c as |
  % 40
  es' s8 es' s8 d s8 c s8 |
  % 41
  c, s8 b s8 as s8 as' s8 |
  % 42
  f s8 b s8 d, s8 b s8 |
  % 43
  es, s8 es'8. es16 g8. g16 b8 s8 |
  % 44
  b,8. b16 d8. d16 f8 s8 f,8. f'16 |
  % 45
  es8. d16 c8 s8 c,8. c''16 b8. as16 |
  % 46
  g8 es g b g es as g |
  % 47
  f g f es d b es g |
  % 48
  es c f es d es d c |
  % 49
  h g c8. c16 b8. b16 a8. a16 |
  % 50
  a8. a16 a8. a16 as8. as16 as8. as16 |
  % 51
  as8. as16 g8. as16 b4 b |
  % 52
  es,8 s8 es' s8 d s8 c s8 |
  % 53
  b s8 as s8 g s8 g' s8 |
  % 54
  f s8 es s8 d s8 c s8 |
  % 55
  d s8 b s8 d s8 es s8 |
  % 56
  g s8 f s8 es s8 c s8 |
  % 57
  es s8 f s8 es s8 f s8 |
  % 58
  b, s8 b' s8 a s8 g s8 |
  % 59
  g, s8 f s8 es s8 es' s8 |
  % 60
  c s8 f s8 a, s8 f s8 |
  % 61
  b s8 b' s8 as s8 g s8 |
  % 62
  g, s8 f s8 es s1. c'8 d es f g a f |
  % 65
  b a g f g c a f |
  % 66
  g a b a g f es d |
  % 67
  es c f4 f, b8 f' |
  % 68
  b f d b es es, es' f |
  % 69
  g f e g c g e c |
  % 70
  f f, f' g as f d f |
  % 71
  b, c d b es, s8 es'8. es16 |
  % 72
  g8. g16 c8 s8 c,8. c16 es8. es16 |
  % 73
  g8 s8 es8. es16 g8. g16 as8 s8 |
  % 74
  as,8. as16 c8. c16 es8 s8 c' s8 |
  % 75
  b s8 as s8 f s8 as s8 |
  % 76
  b s8 b, s8 b' s8 es, s8 |
  % 77
  b s8 des s8 g es es' des |
  % 78
  c b as g as b c as |
  % 79
  es' s8 es,8. es16 g8. g16 b8 s8 |
  % 80
  b8. b16 d8. d16 fis8 s8 d8. d16 |
  % 81
  fis8. fis16 g8 s8 g8. g16 b8. b16 |
  % 82
  d8 b d f d b es d |
  % 83
  c d c b a f b d |
  % 84
  b g c b a b a g |
  % 85
  fis d g8. g16 f8. f16 e8. e16 |
  % 86
  e8. e16 e8. e16 es8. es16 es8. es16 |
  % 87
  es8. es16 d8. es16 f4 f, |
  % 88
  b8 s8 b' s8 as s8 g s8 |
  % 89
  g, s8 f s8 es s8 es' s8 |
  % 90
  d s8 c s8 d s8 b s8 |
  % 91
  es c c b b as as b |
  % 92
  b as as g g es as8. as16 |
  % 93
  c8. c16 es8 s8 es' s8 d s8 |
  % 94
  c s8 c, s8 b s8 as s8 |
  % 95
  as' s8 f s8 b s8 d, s8 |
  % 96
  b s8 es s8 g, s8 b s8 |
  % 97
  es, s8 es' s8 d s8 c s8 |
  % 98
  d s8 b s8 es c c b |
  % 99
  b as as b b as as g |
  % 100
  g es as8. as16 c8. c16 es8 s8 |
  % 101
  es' s8 d s8 c s8 c, s8 |
  % 102
  b s8 a s8 a' s8 g s8 |
  % 103
  fis s8 d' s8 d, s8 g s8 |
  % 104
  g, s8 f s8 es s8 es' s8 |
  % 105
  d s8 c s8 c' s8 b s8 |
  % 106
  a s8 f s8 f, s8 b s8 |
  % 107
  as s8 g s8 as s8 as' s8 |
  % 108
  g s8 f s8 f, s8 es s8 |
  % 109
  d s8 b' s8 b' s8 es, s8 |
  % 110
  es, s8 d s8 c s8 c' s8 |
  % 111
  b s8 a s8 f' s8 es s8 |
  % 112
  d s8 d' s8 c s8 b s8 |
  % 113
  b, s8 as s8 g s8 c s8 |
  % 114
  c, s8 f s8 f' s8 es s8 |
  % 115
  d s8 d' s8 c s8 h s8 |
  % 116
  g s8 f s8 es8. f16 g4 |
  % 117
  g, c8 s8 c' s8 b s8 |
  % 118
  as s8 as, s8 g s8 f s8 |
  % 119
  f' s8 d s8 g s8 h, s8 |
  % 120
  g s8 c s8 c, c'' b as |
  % 121
  g es g b g es as g |
  % 122
  f es d c h g h d |
  % 123
  h g c s8 b s8 as s8 |
  % 124
  g s8 es s8 es' s8 as s8 |
  % 125
  as, s8 g s8 f s8 f' s8 |
  % 126
  es s8 des s8 des' s8 b s8 |
  % 127
  es s8 g, s8 es s8 as s8 |
  % 128
  as,8. as16 c8. c16 es8 s8 es,8. es16 |
  % 129
  g8. g16 h8 s8 g'8. g16 h8. h16 |
  % 130
  c8 s8 c,8. c16 es8. es16 g8 b |
  % 131
  es b g es as as, as' b |
  % 132
  c b a c f c a f |
  % 133
  b b, b' as16 g f es d c d8 f |
  % 134
  b, c d b es, s8 es'8. es16 |
  % 135
  g8. g16 c8 s8 c,8. c16 es8. es16 |
  % 136
  g8 s8 es8. es16 g8. g16 as8 s8 |
  % 137
  as,8. as16 c8. c16 es8 s8 c' s8 |
  % 138
  b s8 as s8 f s8 as s8 |
  % 139
  b s8 b, s8 b' s8 es, s8 |
  % 140
  b s8 des s8 g es es' des |
  % 141
  c b as g as b c as |
  % 142
  es' s8 es' s8 d s8 c s8 |
  % 143
  c, s8 b s8 as s8 as' s8 |
  % 144
  f s8 b s8 d, s8 b s8 |
  % 145
  es, s8 es'8. es16 g8. g16 b8 s8 |
  % 146
  b,8. b16 d8. d16 f8 s8 f,8. f'16 |
  % 147
  es8. d16 c8 s8 c,8. c''16 b8. as16 |
  % 148
  g8 es g b g es as g |
  % 149
  f g f es d b es g |
  % 150
  es c f es d es d c |
  % 151
  h g c8. c16 b8. b16 a8. a16 |
  % 152
  a8. a16 a8. a16 as8. as16 as8. as16 |
  % 153
  as8. as16 g8. as16 b4 b |
  % 154
  es,2.
}

%trackF = <<
%
%  \clef bass
%
%  \context Voice = channelA \trackFchannelA
%  \context Voice = channelB \trackFchannelB
%>>


%trackGchannelA =  {
%
%  % [SEQUENCE_TRACK_NAME] Instrument 6
%
%}

trackGchannelB = \relative c {
  s2. <es' c >8. <es c >16 |
  % 2
  <es c >8. <es c >16 <es c >8. <es c >16 <es c >2 |
  % 3
  s4 <as d >8. <as d >16 <as f >8. <as f >16 <as f >8. <as f >16 |
  % 4
  <g es >2 s4 b16 c d8. es16 f8. d16 c b c f c as f8 s4. c'16
  d es8. f16 g8. f16 g as |
  % 7
  es8 s8 b s8 es s8 c s8 |
  % 8
  f8*5 s8 es s8 |
  % 9
  b s8 as s8 as2 |
  % 10
  d8 s8 c s8 c' s8 f, s16 g |
  % 11
  a b c8. d16 es c d f b, c d es f8. g16 as f <b es, >8.
  <as f >16 <g es >4 <f d >4 |
  % 13
  <es b >4 s16*11 <es, c >16 |
  % 14
  <es c >8. <es c >16 <es c >8. <es c >16 <es g >4 s8*5 es16
  f g as b8. as16 g as |
  % 16
  f4 s8*5 es'16 d |
  % 17
  es f g8. a16 b a b4*5 a4 |
  % 19
  <b f, >8. <b, f >16 <b f >8. <b f >16 <b f >8. <b f >16
  <b g >2 s4 <c g >8. <c g >16 <c b >8. <c b >16 |
  % 21
  <c b >8. <c b >16 <c a >2 s4. b16 c d es f8. as16 g
  f es8*21 d16 c b a g f c'8 d16 c |
  % 26
  b8 s2 g4 a16 b |
  % 27
  c d es c d es f8 s8*5 es8. d16 c b a g f8. g16 f es |
  % 29
  d8 s4. es16 f s8 es b'8. c16 des b <c g >8 b16 as
  g8 s4. |
  % 31
  f16 g as8. b16 c8. d16 es c f,8 s8 |
  % 32
  d' s8 b s8 b as16 b g b as b |
  % 33
  es b as b g' g, f g es g f g c g f g |
  % 34
  es'8 s8*5 <es, c >8. <es c >16 |
  % 35
  es8. <es es >4*0/1024 <es as >4*0/1024 <es as >16 <es g >2 |
  % 36
  s4 <f c >8. <f c >16 <f c >8. <f c >16 <f c >8. <f c >16 |
  % 37
  <f d >2 s4 es16 f s8 |
  % 38
  b s8 g b16 c des s16 g b es,8 s4. as16 b c8. des16
  s8 as es'16 f |
  % 40
  <g b, >8 s8*5 <es, c >8. <es c >16 |
  % 41
  <es c >8. <es c >16 <es c >8. <es c >16 <es c >2 |
  % 42
  s4 <as d >8. <as d >16 <as f >8. <as f >16 <as f >8. <as f >16 |
  % 43
  <g es >2 s4 b16 c d8. es16 f8. d16 c b c f c as f8 s4. c'16
  d es8. f16 g8. f16 g as |
  % 46
  es8 s8 b s8 es s8 c s8 |
  % 47
  <as' f >2. es8 s8 |
  % 48
  b s8 as s8 as2 |
  % 49
  d8 s8 c s8 c' s8 f, s16 g |
  % 50
  a b c8. d16 es c d f b, c d es f8. g16 as f <b es, >8.
  <as f >16 <g es >4 <f d >4 |
  % 52
  <es b >4 s16*11 <es, c >16 |
  % 53
  <es c >8. <es c >16 <es c >8. <es c >16 <es g >4 s8*5 es16
  f g as b8. as16 g as |
  % 55
  f4 s8*5 es'16 d |
  % 56
  es f g8. a16 b a b4*5 a4 |
  % 58
  <b f, >8. <b, f >16 <b f >8. <b f >16 <b f >8. <b f >16
  <b g >2 s4 <c g >8. <c g >16 <c b >8. <c b >16 |
  % 60
  <c b >8. <c b >16 <c a >2 s4. b16 c d es f8. as16 g
  f es8*21 d16 c b a g f c'8 d16 c |
  % 65
  b8 s2 g4 a16 b |
  % 66
  c d es c d es f8 s8*5 es8. d16 c b a g f8. g16 f es |
  % 68
  d8 s4. es16 f s8 es b'8. c16 des b <c g >8 b16 as
  g8 s4. |
  % 70
  f16 g as8. b16 c8. d16 es c f,8 s8 |
  % 71
  d' s8 b s8 b as16 b g b as b |
  % 72
  es b as b g' g, f g es g f g c g f g |
  % 73
  es'8 s8*5 <es, c >8. <es c >16 |
  % 74
  es8. <es es >4*0/1024 <es as >4*0/1024 <es as >16 <es g >2 |
  % 75
  s4 <f c >8. <f c >16 <f c >8. <f c >16 <f c >8. <f c >16 |
  % 76
  <f d >2 s4 es16 f s8 |
  % 77
  b s8 g b16 c des s16 g b es,8 s4. as16 b c8. des16
  s8 as es'16 f |
  % 79
  <g b, >8 s8 <b g >8 s8 <g es >8 s8 b,16 c s8 |
  % 80
  d16 es f8. es16 f g <a d, >8 s8 <fis d >8 s8 |
  % 81
  <d a >8 s8 g,16 a b8. c16 d8. c16 d es |
  % 82
  b8 s8 f s8 b s8 g s8 |
  % 83
  c8*5 s8 b s8 |
  % 84
  f s8 es s8 es2 |
  % 85
  d8 s8 d s8 d s8 c s16 d |
  % 86
  e f g8. a16 b g a c f, g a b c8. d16 es c <f b, >8.
  <es c >16 <d b >4 <c a >4 |
  % 88
  b s8*5 es,16 f |
  % 89
  g as b8. c16 d b b8 s2. c8 b s8 b s8 |
  % 91
  g es'16 d es8*5 f,8 |
  % 92
  des'8*5 c16 s16 c des es8. c16 b as <b es, >4 s2 |
  % 94
  <es, c >8. <es c >16 <es c >8. <es c >16 <es c >8. <es c >16
  <es c >2 s4 <as d >8. <as d >16 <as f >8. <as f >16 |
  % 96
  <as f >8. <as f >16 <g es >8 s16 f g as b8. c16 d b |
  % 97
  es,8 s2. c'8 |
  % 98
  b s8 b s8 g es'16 d es8*5 f,8 des'2 |
  % 100
  d8 c16 s16 c des es8. c16 b as <b es, >4 |
  % 101
  s16*11 <g es >16 <g es >8. <g es >16 |
  % 102
  <g es >8. <g es >16 <g es >2 s16*7 <fis d >16 <fis d >8.
  <fis d >16 <fis d >8. <fis d >16 <g d >8. <b g >16 |
  % 104
  <b g >8. <b g >16 <b g >8. <b g >16 <b g >2 |
  % 105
  s16*7 <es g, >16 <es g, >8. <es es, >16 <es c >8. <es c >16 |
  % 106
  <es c >2 s2 |
  % 107
  b'16*9 des,16 c es as8 s8*5 f16*9 b,16 as g f8 s4. es8.
  es16 |
  % 110
  es8. <es es >4*0/1024 <es es >4*0/1024 <es es >4*0/1024
  es2 |
  % 111
  s4 f8. f16 f8. <f f >4*0/1024 <f f >4*0/1024 <f f >4*0/1024 |
  % 112
  f2 s4 <b f >8. <b f >16 |
  % 113
  <b f >8. <b f >16 <b f >8. <b f >16 <b g >4 s2 <f' c >8.
  <f c >16 <f c >8. <f c >16 <f c >8. <f c >16 |
  % 115
  f,8 s16*11 c'16 h a |
  % 116
  g8 s4. <g' c, >8. <f as, >16 <es g, >4 |
  % 117
  <d g, >4 <c g >8. <c g >16 <c g >8. <c g >16 <c g >8. <c g >16 |
  % 118
  <c c, >2 s4 <d as, >8. <d as, >16 |
  % 119
  <d c >8. <d c >16 <d c >8. <d c >16 <d h >2 |
  % 120
  s4 c16 d es8. f16 g8. f16 g as |
  % 121
  es8 s8*7 |
  % 122
  as2. g8*5 es,16 f g as b8. as16 b c |
  % 124
  des s16 g as b c s16 es f es f g <as c, >4 |
  % 125
  s2 <as, f >8. <as f >16 <as f >8. <as f >16 |
  % 126
  <as f >8. <as f >16 <as f >2 s4 |
  % 127
  <des g >8. <des g >16 <des b >8. <des b >16 <des b >8.
  <des b >16 <c as >2 s4. es16 f g as b8. g16 f es f as
  f d h8 s2 c16 d es f g8. f16 g as b as g f |
  % 131
  es8 s4. as,16 b c8. des16 es8. f16 fis es <f c >8 es16
  d c8 s4. |
  % 133
  b16 c d8. es16 f8. g16 as f <b f >8 as16 g |
  % 134
  d8 s8 b s8 b as16 b g b as b |
  % 135
  es b as b g' g, f g es g f g c g f g |
  % 136
  es'8 s8*5 <es, c >8. <es c >16 |
  % 137
  es8. <es es >4*0/1024 <es as >4*0/1024 <es as >16 <es g >2 |
  % 138
  s4 <f c >8. <f c >16 <f c >8. <f c >16 <f c >8. <f c >16 |
  % 139
  <f d >2 s4 es16 f s8 |
  % 140
  b s8 g b16 c des s16 g b es,8 s4. as16 b c8. des16
  s8 as es'16 f |
  % 142
  <g b, >8 s8*5 <es, c >8. <es c >16 |
  % 143
  <es c >8. <es c >16 <es c >8. <es c >16 <es c >2 |
  % 144
  s4 <as d >8. <as d >16 <as f >8. <as f >16 <as f >8. <as f >16 |
  % 145
  <g es >2 s4 b16 c d8. es16 f8. d16 c b c f c as f8 s4. c'16
  d es8. f16 g8. f16 g as |
  % 148
  es8 s8 b s8 es s8 c s8 |
  % 149
  f8*5 s8 es s8 |
  % 150
  b s8 as s8 as2 |
  % 151
  d8 s8 c s8 c' s8 f, s16 g |
  % 152
  a b c8. d16 es c d f b, c d es f8. g16 as f <b es, >8.
  <as f >16 <g es >4 <f d >4 |
  % 154
  <es b >2.
}

%trackG = <<
%  \context Voice = channelA \trackGchannelA
%  \context Voice = channelB \trackGchannelB
%>>


%trackHchannelA =  {
%
%}

trackHchannelB = \relative c {
  <es' b >8. <es b >16 <es b >8. <es b >16 <es b >8.
  <es b >16 <es c >2 s4 <f c >8. <f c >16 <f es >8. <f es >16 |
  % 3
  <f es >8. <f es >16 <f d >2 s4 |
  % 4
  es16 f g8. as16 b8. g16 f es f b f d |
  % 5
  b8 s4. f'16 g as8. b16 c8. h16 c d c8 s8 g s8 g s8 |
  % 7
  es c16 d es f g as b c des b c c, d es |
  % 8
  f g s16 b c d es c f d, es f g as s16 c |
  % 9
  d es f g as s16 g, as b c d es f g as f |
  % 10
  g g, a h c d es f g a b g c es, d es |
  % 11
  f, g a b c d es c d c d f b, c d es |
  % 12
  f g as f <b g, >8. <as f >16 <g es >4 <f d >4 |
  % 13
  <es b >8. <es, b >16 <es b >8. <es b >16 <es b >8.
  <es b >16 <es c >2 s4. b16 c d es f8. g16 as f g4 s8*5 b16
  c d es f8. g16 as f g4 |
  % 17
  s8*5 c,16 d es f g8. a16 b g c b c d c b a g f es d
  c |
  % 19
  d es d c b b' a g f es d c <b g >8. <b g >16 |
  % 20
  <b g >8. <b g >16 <b g >8. <b g >16 <b g >2 |
  % 21
  s4 <es a >8. <es a >16 <es c >8. <es c >16 <es c >8. <es c >16 |
  % 22
  <d b >2 s4. es16 d |
  % 23
  es f g8. as16 b as b8*13 s2. f,4 g16 a b c d b es
  d c d es8 f16 g |
  % 27
  a b c a f es d8. c16 b8. a16 g f |
  % 28
  es8 s8*5 b'16 c d8. es16 f8. g16 as f g f es d es8 s4. c16
  d e8. f16 g8. as16 b g |
  % 31
  c,8 s8*5 d8 a16 b |
  % 32
  d, b' a b as' b, a b g' g, f g es g f g |
  % 33
  b g f g es es' d es c es d es g es d es |
  % 34
  b8 s8*11 <es, b >8. <es b >16 <es c >8. <es c >16 |
  % 36
  <es g, >8. <es g, >16 <es c >2 s4 |
  % 37
  <as d >8. <as d >16 <as d >8. <as d >16 <as d >8. <as d >16
  <g b, >2 s4 es16 f g8. as16 b8. c16 des b es f es des
  c des c b as f es d |
  % 40
  <es b >8. <es b >16 <es b >8. <es b >16 <es b >8.
  <es b >16 <es c >2 s4 <f c >8. <f c >16 <f es >8. <f es >16 |
  % 42
  <f es >8. <f es >16 <f d >2 s4 |
  % 43
  es16 f g8. as16 b8. g16 f es f b f d |
  % 44
  b8 s4. f'16 g as8. b16 c8. h16 c d c8 s8 g s8 g s8 |
  % 46
  es c16 d es f g as b c des b c c, d es |
  % 47
  f g s16 b c d es c f d, es f g as s16 c |
  % 48
  d es f g as s16 g, as b c d es f g as f |
  % 49
  g g, a h c d es f g a b g c es, d es |
  % 50
  f, g a b c d es c d c d f b, c d es |
  % 51
  f g as f g,8 g16 <as' f >16 <g es >4 <f d >4 |
  % 52
  <es b >8. <es, b >16 <es b >8. <es b >16 <es b >8.
  <es b >16 <es c >2 s4. b16 c d es f8. g16 as f g4 s8*5 b16
  c d es f8. g16 as f g4 |
  % 56
  s8*5 c,16 d es f g8. a16 b g c b c d c b a g f es d
  c |
  % 58
  d es d c b b a g f es d c <b g' >8. <b' g >16 |
  % 59
  <b g >8. <b g >16 <b g >8. <b g >16 <b g >2 |
  % 60
  s4 <es a >8. <es a >16 <es c >8. <es c >16 <es c >8. <es c >16 |
  % 61
  <d b >2 s4. es16 d |
  % 62
  es f g8. as16 b as b8*13 s2. f,4 g16 a b c d b es
  d c d es8 f16 g |
  % 66
  a b c a f es d8. c16 b8. a16 g f |
  % 67
  es8 s8*5 b'16 c d8. es16 f8. g16 as f g f es d es8 s4. c16
  d e8. f16 g8. as16 b g |
  % 70
  c,8 s8*5 d8 a16 b |
  % 71
  d, b' a b as' b, a b g' g, f g es g f g |
  % 72
  b g f g es es' d es c es d es g es d es |
  % 73
  b8 s8*11 <es, b >8. <es b >16 <es c >8. <es c >16 |
  % 75
  <es g, >8. <es g, >16 <es c >2 s4 |
  % 76
  <as d >8. <as d >16 <as d >8. <as d >16 <as d >8. <as d >16
  <g b, >2 s4 es16 f g8. as16 b8. c16 des b es f es des
  c des c b as f es d |
  % 79
  es f g8. as16 b8. a16 b c <d b >8 s8 |
  % 80
  <f b, >8 s8 <d f, >8 s8 d,16 e fis8. g16 a8. b16 c a d,8
  s8 d s8 d s8 |
  % 82
  b g'16 a b c d es f g as f g g, a b |
  % 83
  c d es f g a b g c a,, b c d es s16 g |
  % 84
  a b c d es s16 d, es f g a b c d es c |
  % 85
  d d, es fis g a b c d e f d g b, a b |
  % 86
  c, d e f g a b g a g a c f, g a b |
  % 87
  c d es c <f d, >8. <es c >16 <d b >4 <c a >4 |
  % 88
  <b f >8 b16 c d es f8. d16 c b es s16 g, as |
  % 89
  b c d8. c16 b as b g es f g as b8. as16 g f es
  f g as b c d es f d es f |
  % 91
  g b,, c d es f g as b s16 f es c' c, des es |
  % 92
  f g as b c des es f b, b' as g as b c as |
  % 93
  es as g f <g b, >8. <es, b >16 <es b >8. <es b >16
  <es b >8. <es b >16 |
  % 94
  <es c >2 s4 <f c >8. <f c >16 |
  % 95
  <f es >8. <f es >16 <f es >8. <f es >16 <f d >2 |
  % 96
  s4. g16 as b c d8. c16 b as |
  % 97
  b g es f g as b8. as16 g f es f g as |
  % 98
  b c d es f d es f g b,, c d es f g as |
  % 99
  b s16 f es es' c, des es f g as b c des es f |
  % 100
  b, b' as g as b c as es as g f <g b, >8. <g, es >16 |
  % 101
  <g es >8. <g es >16 <g f >8. <g f >16 <g es >2 |
  % 102
  s16*7 <c es, >16 <c es, >8. <c c, >16 <c a >8. <c a >16 |
  % 103
  <c a >2 s16*19 <b g >16 <b g >8. <b g >16 |
  % 105
  <b f >8. <b f >16 s16*15 <a f >16 <a f >8. <a f >16 <a es, >8.
  <a es, >16 <b d, >2 s4 es16*9 b16 g b f'8 s8*5 |
  % 109
  b16*9 es,16 d c b8 s8*5 es,8. es16 es8. <es es >4*0/1024 |
  % 111
  <es es >4*0/1024 <es es >4*0/1024 es2 s4 |
  % 112
  f8. f16 f8. <f f >4*0/1024 <f f >4*0/1024 <f f >4*0/1024 f2
  s4 <b f >8. <b f >16 <b e >8. <b e >16 |
  % 114
  <b e >8. <b e >16 <as f >2 s4 |
  % 115
  <f' as, >8. <f as, >16 <f as, >8. <f as, >16 <f as, >8.
  <f as, >16 g,4 |
  % 116
  s16 c h a g d' es f <g g, >8. <f as, >16 <es g, >4 |
  % 117
  <d g, >4 <c g >2 s4 |
  % 118
  <c es, >8. <c es, >16 <c f, >8. <c f, >16 <c g >8. <c g >16
  <c as >2 s4 <f h, >8. <f h, >16 <f d >8. <f d >16 |
  % 120
  <f d >8. <f d >16 <es c >2 s16*5 b,16 c d es f g as b
  c des es c c, d es |
  % 122
  f g as b c d es f d d, es f g a h c |
  % 123
  d es f g es8*7 es16 f g as b8. c16 as b <c es,, >8.
  <as, es >16 |
  % 125
  <as es >8. <as es >16 <as es >8. <as es >16 <as f >2 |
  % 126
  s4 <b f >8. <b f >16 <b as >8. <b as >16 <b as >8.
  <b as >16 |
  % 127
  <b g >2 s4. as16 b |
  % 128
  c des es8. c16 b as b es b g es8 s2 g16 a h c d8.
  h16 a g |
  % 130
  g' f es d c8 s4. es,16 f g8. as16 b8. c16 des b c b
  as g as8 s4. f16 g a8. b16 c8. d16 es c |
  % 133
  d es f8. g16 as16*7 b,16 a b |
  % 134
  d, b' a b as' b, a b g' g, f g es g f g |
  % 135
  b g f g es es' d es c es d es g es d es |
  % 136
  b8 s8*11 <es, b >8. <es b >16 <es c >8. <es c >16 |
  % 138
  <es g, >8. <es g, >16 <es c >2 s4 |
  % 139
  <as d >8. <as d >16 <as d >8. <as d >16 <as d >8. <as d >16
  <g b, >2 s4 es16 f g8. as16 b8. c16 des b es f es des
  c des c b as f es d |
  % 142
  <es b >8. <es b >16 <es b >8. <es b >16 <es b >8.
  <es b >16 <es c >2 s4 <f c >8. <f c >16 <f es >8. <f es >16 |
  % 144
  <f es >8. <f es >16 <f d >2 s4 |
  % 145
  es16 f g8. as16 b8. g16 f es f b f d |
  % 146
  b8 s4. f'16 g as8. b16 c8. h16 c d c8 s8 g s8 g s8 |
  % 148
  es c16 d es f g as b c des b c c, d es |
  % 149
  f g s16 b c d es c f d, es f g as s16 c |
  % 150
  d es f g as s16 g, as b c d es f g as f |
  % 151
  g g, a h c d es f g a b g c es, d es |
  % 152
  f, g a b c d es c d c d f b, c d es |
  % 153
  f g as f <b g, >8. <as f >16 <g es >4 <f d >4 |
  % 154
  <es b >2.
}

%trackH = <<
%  \context Voice = channelA \trackHchannelA
%  \context Voice = channelB \trackHchannelB
%>>


%trackIchannelA =  {
%
%  % [SEQUENCE_TRACK_NAME] Instrument 8
%
%}

trackIchannelB = \relative c {
  g'8. g16 g8. g16 g8. g16 g8. g16 |
  % 2
  g8. g16 g4*0/1024 s8. g16 as8. as16 c8. c16 |
  % 3
  as8. as16 f8. f16 b8. b16 d8. d16 |
  % 4
  g,8 s4. b8 s8 <d b >8 s8 |
  % 5
  f s8 f s8 <f as, >8 s8 c s8 |
  % 6
  es s8 <es g >8 s8 <g c, >8 s8 <c c, >8 s8 |
  % 7
  <b g >8 s8 <es, b' >8 s8 <g b >8 s8 <es as >8 s8 |
  % 8
  c s8 f s8 <b f >8 s8 <b b, >8 s8 |
  % 9
  es, s8 <c as >8 s8 as' s8 f s8 |
  % 10
  <g h >8 s8 <g es >8 s8 <g c, >8 s8 c, s8 |
  % 11
  <c a >8 s8 <c f, >8 s8 b s8 <f' b, >8 s8 |
  % 12
  <b b, >8 s8 <es, b >8. c'16 b4 as |
  % 13
  g8. g,16 g8. g16 g8. g16 g4 |
  % 14
  g8. g16 c8. c16 b4 s4*17 d8. d16 d8. d16 d8. d16 d8. d16 |
  % 20
  d8. d16 d8. d16 es8. es16 g8. g16 |
  % 21
  es8. es16 c8. c16 f8. f16 a8. a16 |
  % 22
  f2 s4*25 d8 s8 |
  % 29
  b s8 f' s8 <b, es >8 s8 b s8 |
  % 30
  b s8 <g c >8 s8 c s8 g' s8 |
  % 31
  <as, f' >8 s8 c s8 c s8 <b f' >8 s8 |
  % 32
  <b' f >8 s8 <f b, >8 s8 <g b, >8 s8*17 as,8. as16 |
  % 35
  c8. c16 c8. c16 g8. g16 g8. g16 |
  % 36
  c8. c16 as8. as16 as8. as16 s8. f16 |
  % 37
  f8. f16 f8. f16 f8. f16 b8 s8 |
  % 38
  es s8 es s8 <es g, >8 s8 g s8 |
  % 39
  g s8 c, s8 <as' es >8 s8 es s8 |
  % 40
  es s16 g, g8. g16 g8 g16 s16 g8. g16 |
  % 41
  g8. g16 g4*0/1024 s8. g16 as8. as16 c8. c16 |
  % 42
  as8. as16 f8. f16 b8. b16 d8. d16 |
  % 43
  g,8 s4. b8 s8 <d b >8 s8 |
  % 44
  f s8 f s8 <f as, >8 s8 c s8 |
  % 45
  es s8 <es g >8 s8 <g c, >8 s8 <c c, >8 s8 |
  % 46
  <b g >8 s8 <es, b' >8 s8 <g b >8 s8 <es as >8 s8 |
  % 47
  c s8 f s8 f s8 <b b, >8 s8 |
  % 48
  es, s8 <c as >8 s8 as' s8 f s8 |
  % 49
  <g h >8 s8 <g es >8 s8 <g c, >8 s8 c, s8 |
  % 50
  <c a >8 s8 <c f, >8 s8 b s8 <f' b, >8 s8 |
  % 51
  <b b, >8 s8 <es, b >8. c'16 b4 as |
  % 52
  g8. g,16 g8. g16 g8. g16 g4 |
  % 53
  g8. g16 c8. c16 b4 s4*17 d8. d16 d8. d16 d8. d16 d8. d16 |
  % 59
  d8. d16 d8. d16 es8. es16 g8. g16 |
  % 60
  es8. es16 c8. c16 f8. f16 a8. a16 |
  % 61
  f2 s4*25 d8 s8 |
  % 68
  b s8 f' s8 <b, es >8 s8 b s8 |
  % 69
  b s8 <g c >8 s8 c s8 g' s8 |
  % 70
  <as, f' >8 s8 c s8 c s8 <b f' >8 s8 |
  % 71
  <b' f >8 s8 <f b, >8 s8 <g b, >8 s8*17 as,8. as16 |
  % 74
  c8. c16 c8. c16 g8. g16 g8. g16 |
  % 75
  c8. c16 as8. as16 as8. as16 s8. f16 |
  % 76
  f8. f16 f8. f16 f8. f16 b8 s8 |
  % 77
  es s8 es s8 <es g, >8 s8 g s8 |
  % 78
  g s8 c, s8 <as' es >8 s8 es s8 |
  % 79
  <es b >8 s8 <es' b, >8 s8 <b g >8 s8 f s8 |
  % 80
  <f d >8 s8 <b b, >8 s8 <a fis, >8 s8 <a a, >8 s8 |
  % 81
  <d a, >8 s8 <b g, >8 s8 <d, g >8 s8 g s8 |
  % 82
  <d f >8 s8 <b f' >8 s8 <d f >8 s8 <b es >8 s8 |
  % 83
  g s8 c s8 <f c >8 s8 <f f, >8 s8 |
  % 84
  b, s8 <g es >8 s8 es' s8 c s8 |
  % 85
  fis s8 <g, b' >8 s8 <g g' >8 s8 <g g' >8 s8 |
  % 86
  <g e' >8 s8 <g c >8 s8 <f f' >8 s8 <c' f >8 s8 |
  % 87
  f s8 <b, f' >8. g'16 f4 es |
  % 88
  d8 s8 b s8 d s8 g s8 |
  % 89
  b, s8 d s8 es s8 b s8 |
  % 90
  d s8 es s8 d s8 f s8 |
  % 91
  b, s8 g' s8 es s8 f s8 |
  % 92
  f2 g4 c,8 s8 |
  % 93
  as s8 g4 g8. g16 g8. g16 |
  % 94
  g8. g16 g8. g16 g4*0/1024 s8. g16 as8. as16 |
  % 95
  c8. c16 as8. as16 f8. f16 b8. b16 |
  % 96
  d8. d16 b8 s8 b s8 d s8 |
  % 97
  b s8 b s8 d s8 es s8 |
  % 98
  d s8 f s8 b, s8 g' s8 |
  % 99
  es s8 f s8 f2 |
  % 100
  g4 c,8 s8 as s8 g4 |
  % 101
  b8. b16 h8. h16 c4 c8. c16 |
  % 102
  c4*0/1024 s8. c16 c2 es8. c16 |
  % 103
  d2 a8. a16 b8. d16 |
  % 104
  d8. d16 d8. d16 es4 es8. es16 |
  % 105
  b8. b16 s8. b'16 b8. g16 g8. es16 |
  % 106
  f2 c8. c16 b2 s1*3 es8. es16 es2 |
  % 111
  es8. es16 es2 f8. f16 |
  % 112
  f2 f8. f16 d8. d16 |
  % 113
  d8. d16 d8. d16 des8. s16 g8. c16 |
  % 114
  c8. c16 as'8. as16 as8. as16 as8. as16 |
  % 115
  as8 s16 d d8. d16 d8. d16 d4 |
  % 116
  s2 <g, c >8. c16 <c c >4*0/1024 |
  % 117
  h4 es8. es16 es8. es16 es8. es16 |
  % 118
  c8. c16 d8. d16 es8. es16 f8. f16 |
  % 119
  as8. as16 f4*0/1024 s8. f16 d8. d16 g8. g16 |
  % 120
  h8. h16 es,8 s4. c'8 s8 |
  % 121
  b s8*19 es,8 s8 f s8 |
  % 124
  g s8 <g es >8 s8 <es b >8 s8 as,8. c16 |
  % 125
  c8. c16 c8. c16 c8. c16 c8. c16 |
  % 126
  c4*0/1024 s8. c16 des8. des16 f8. f16 des8. des16 |
  % 127
  b8. b16 es8. es16 g8. g16 es2 s2*5 es8 s8 |
  % 131
  es s8 b s8 <c as >8 s8 es s8 |
  % 132
  es s8 <c f, >8 s8 f, s8 c' s8 |
  % 133
  <d b >8 s8 f s8 as s8 f s8 |
  % 134
  <b f >8 s8 <f b, >8 s8 <g b, >8 s8*17 as,8. as16 |
  % 137
  c8. c16 c8. c16 g8. g16 g8. g16 |
  % 138
  c8. c16 as8. as16 as8. as16 s8. f16 |
  % 139
  f8. f16 f8. f16 f8. f16 b8 s8 |
  % 140
  es s8 es s8 <es g, >8 s8 g s8 |
  % 141
  g s8 c, s8 <as' es >8 s8 es s8 |
  % 142
  es s16 g, g8. g16 g8. g16 g8. g16 |
  % 143
  g8. g16 g4*0/1024 s8. g16 as8. as16 c8. c16 |
  % 144
  as8. as16 f8. f16 b8. b16 d8. d16 |
  % 145
  g,8 s4. b8 s8 <d b >8 s8 |
  % 146
  f s8 f s8 <f as, >8 s8 c s8 |
  % 147
  es s8 <es g >8 s8 <g c, >8 s8 <c c, >8 s8 |
  % 148
  <b g >8 s8 <es, b' >8 s8 <g b >8 s8 <es as >8 s8 |
  % 149
  c s8 f s8 <b f >8 s8 <b b, >8 s8 |
  % 150
  es, s8 <c as >8 s8 as' s8 f s8 |
  % 151
  <g h >8 s8 <g es >8 s8 <g c, >8 s8 c, s8 |
  % 152
  <c a >8 s8 <c f, >8 s8 b s8 <f' b, >8 s8 |
  % 153
  <b b, >8 s8 <es, b >8. c'16 b4 as |
  % 154
  g2.
}

%trackI = <<
%
%  \clef bass
%
%  \context Voice = channelA \trackIchannelA
%  \context Voice = channelB \trackIchannelB
%>>


%\score {
%  <<
%    \context Staff=Sopran \nsopran
%    \context Staff=Alt \nalt
%    \context Staff=Tenor \ntenor
%    \context Staff=Bass \nbass
%%    \context Staff=trackF \trackF
%%    \context Staff=trackG \trackG
%%    \context Staff=trackH \trackH
%%    \context Staff=trackI \trackI
%  >>
%  \midi {}
%  \layout {}
%}

global = {
  \key es \major
  \time 3/4
  \tempo "Allegro moderato" 4 = 80
  \dynamicUp
  \autoBeamOff
  \compressEmptyMeasures
}

sopranowords = \lyricmode {
  Wa -- chet auf, ruft uns die Stim -- me
  Der Wäch -- ter sehr hoch auf der Zin -- ne.
  Wach' auf, du Stadt Je -- ru -- sa -- lem!
  Mit -- ter -- nacht heißt die -- se Stun -- de;
  Sie ru -- fen uns mit hel -- lem Mun -- de:
  Wo seid ihr klu -- gen Jung -- frau -- en?
  Wohl auf, der Bräut' -- gam kommt;
  Steht auf, die Lam -- pen nehmt! Al -- le -- lu -- ja!
  Macht euch be -- reit
  Zu der Hoch -- zeit,
  Ihr müs -- set ihm ent -- ge -- gen gehn!
}
altowords = \sopranowords
tenorwords = \sopranowords
basswords = \sopranowords

\score {
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "soprano" << \global \sopranonotes >>
      \new Lyrics \lyricsto "soprano" \sopranowords
    >>
    \new Staff <<
      \new Voice = "alto" << \global \altonotes >>
      \new Lyrics \lyricsto "alto" \altowords
    >>
    \new Staff <<
      \new Voice = "tenor" << \global \tenornotes >>
      \new Lyrics \lyricsto "tenor" \tenorwords
    >>
    \new Staff <<
      \new Voice = "bass" << \global \bassnotes >>
      \new Lyrics \lyricsto "bass" \basswords
    >>
  >>
  \midi {}
  \layout {}
}
