\version "2.25.22"
\language deutsch
\include "bach-aus-tiefer-not.ily"

incipitwidth = 18

\header {
  title = "Aus tiefer Noth schrei ich zu dir"
  subtitle = "Dominica 21 post Trinitatis"
  subsubtitle = "Cantate 38"
  composer = "J. S. Bach"
  shortcomposer = "Bach"
  tagline = \markup \with-url "http://lilypond.org/"
  \line {
    "Lilypond"
    #(string-join
      (map (lambda (x) (if (symbol? x)
                           (symbol->string x)
                           (number->string x)))
        (list-head (ly:version) 2))
      ".")
    "– www.lilypond.org"
  }
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2013 Joram Berger, joramberger.de"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
  opus = "BWV 38"
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, 1857. Plate B.W. VII."
  mutopiacomposer = "BachJS"
  mutopiapoet = "Martin Luther (Ps 130)"
  mutopiainstrument = "choir"
  date = "2013-12-25"
  %footer = "Mutopia-2012/12/00-000"
}

#(set-global-staff-size 17)

\paper {
  evenHeaderMarkup = \markup \fill-line {
    \fromproperty #'page:page-number-string
    \fromproperty #'header:title
    \fromproperty #'header:shortcomposer
  }
  oddHeaderMarkup = \markup \fill-line {
    \unless \on-first-page \fromproperty #'header:shortcomposer
    \unless \on-first-page \fromproperty #'header:title
    \unless \on-first-page \fromproperty #'page:page-number-string
  }
  ragged-last-bottom = ##f
  last-bottom-spacing.basic-distance = 10
  top-markup-spacing.basic-distance = 3
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
  indent = 3.3\cm
  page-count = 4
}

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L italic"
  \override Staff.ClefModifier.extra-offset = #'(-0.3 . 0.07)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Staff.VerticalAxisGroup.minimum-Y-extent = #'(-3 . 3)
  \override Score.BarNumber.padding = #1.6
}


incipitsoprano = \markup{
  \score{
    {
      \set Staff.instrumentName="Sopran "
      \override NoteHead.style = #'neomensural
      \cadenzaOn
      \clef "soprano"
      \key c \major
      \time 2/2
      h'\breve
      \cadenzaOff
    }
    \layout {
      line-width = \incipitwidth
      indent = 0
    }
  }
}

incipitalto = \markup{
  \score{
    {
      \set Staff.instrumentName="Alt       "
      \override NoteHead.style = #'neomensural
      \cadenzaOn
      \clef "alto"
      \key c \major
      \time 2/2
      e'\breve
      \cadenzaOff
    }
    \layout {
      line-width = \incipitwidth
      indent = 0
    }
  }
}

incipittenor = \markup{
  \score{
    {
      \set Staff.instrumentName="Tenor   "
      \override NoteHead.style = #'neomensural
      \cadenzaOn
      \clef "tenor" %"petrucci-c3"
      \key c \major
      \time 2/2
      h\breve
      \cadenzaOff
    }
    \layout {
      line-width = \incipitwidth
      indent = 0
    }
  }
}

incipitbass = \markup{
  \score{
    {
      \set Staff.instrumentName="Bass     "
      \override NoteHead.style = #'neomensural
      \cadenzaOn
      \clef "bass"
      \key c \major
      \time 2/2
      e\breve
      \cadenzaOff
    }
    \layout {
      line-width = \incipitwidth
      indent = 0
    }
  }
}


\score {
  \new PianoStaff \with {
    fontSize = #-1
    \override StaffSymbol.staff-space = #(magstep -1)
  } <<
    \new Staff \with {
      \consists "Mark_engraver"
      \consists "Metronome_mark_engraver"
      \remove "Staff_performer"
    } {
      \accidentalStyle piano
      << \soprano \\ \alto >>
    }
    \new Staff \with {
      \remove "Staff_performer"
    } {
      \clef bass
      \accidentalStyle piano
      << \tenor \\ \bass >>
    }
  >>
  \midi { }
}

\score {
  \new ChoirStaff <<
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = \incipitsoprano
      \override InstrumentName.extra-offset = #'(0 . -0.5)
    } { \clef "treble" \soprano }
    \addlyrics { \sopranoVerse }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = \incipitalto
    } { \clef "treble" \alto }
    \addlyrics { \altoVerse }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = \incipittenor
    } { \clef "treble_8" \tenor }
    \addlyrics { \tenorVerse }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = \incipitbass
    } { \clef "bass" \bass }
    \addlyrics { \bassVerse }
  >>

  \layout{}
}


\score {
  \new ChoirStaff <<
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Sopran"
    } { \soprano }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Alt"
    } { \alto }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Tenor"
    } { \tenor }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Bass"
    } { \bass }
  >>
  \midi{ \tempo 4 = 200 }
}
