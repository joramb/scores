\version "2.25.22"
\language deutsch
\include "bach-aus-tiefer-not.ily"

\header {
  title = "Aus tiefer Noth schrei ich zu dir."
  subtitle = "Dominica 21 post Trinitatis"
  subsubtitle = "Cantate 38"
  composer = "Joh. Seb. Bach"
  opus = "BWV"
  copyright = "© 2011, Joram Berger"
  %   % Voreingestellte LilyPond-Tagline entfernen
  %   tagline = ##f
}


pianoReduction = \new PianoStaff \with {
  fontSize = #-1
  \override StaffSymbol.staff-space = #(magstep -1)
} <<
  \new Staff \with {
    \consists "Mark_engraver"
    \consists "Metronome_mark_engraver"
    \remove "Staff_performer"
  } {
    \accidentalStyle piano
    <<
      \soprano \\
      \alto
    >>
  }
  \new Staff \with {
    \remove "Staff_performer"
  } {
    \clef bass
    \accidentalStyle piano
    <<
      \tenor \\
      \bass
    >>
  }
>>

choirPart = <<
  \new ChoirStaff <<
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Sopran"
    } { \clef soprano \soprano }
    \addlyrics { \sopranoVerse }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Alt"
    } { \clef alto \alto }
    \addlyrics { \altoVerse }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Tenor"
    } { \clef tenor \tenor } %"treble_8"
    \addlyrics { \tenorVerse }
    \new Staff \with {
      midiInstrument = "acoustic grand"
      instrumentName = "Bass"
    } { \clef bass \bass }
    \addlyrics { \bassVerse }
  >>
  %   \pianoReduction
>>

bassFiguresPart = \new FiguredBass \bassFigures

\score {
  <<
    \choirPart
    \bassFiguresPart
  >>
  \layout {
    \context {
      % etwas kleiner, damit der Text näher am System sein kann
      \Staff
      \override VerticalAxisGroup.minimum-Y-extent = #'(-3 . 3)
    }
  }
  \midi {
    \tempo 4 = 200
  }
}
