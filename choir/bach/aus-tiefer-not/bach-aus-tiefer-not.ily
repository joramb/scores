\version "2.25.22"
\language deutsch

\header {
  title = "Aus tiefer Noth schrei ich zu dir"
  subtitle = "Dominica 21 post Trinitatis"
  subsubtitle = "Cantate 38"
  opus = "BWV 687" % wrong?
  composer = "Joh. Seb. Bach"
  tagline = ##f
}


global = {
  \key a \minor
  \time 2/2
  \compressEmptyMeasures
}

mBreak = { }

soprano = \relative c'' {
  \global
  R1*12 | \mBreak
  h='1  | e, | h'   | c    | h2( a)  | g1   | \mBreak
  a='1  | h~ | h    | R1*9 | \mBreak
  h='1  | c  | d    | c    | a  | g( | f) | \mBreak
  e='1~ | e  | R1*12| \mBreak
  h'='1 | e, | h'   | c    | h2( a) | g1    | \mBreak
  a='1  | h~ | h    | R1*9 | \mBreak
  h='1  | c  | d    | c    | a  | g( | f) | \mBreak
  e='1~ | e  | R1*11| \mBreak
  a='1  | g  | c    | \mBreak
  h='1  | a  | d2( c) | h1 | a~ | a  | R1*8 | \mBreak
  c=''1 | h  | \mBreak
  c=''1 | d  | g,2( a)| h1 | a  | g~ | g  | R1*10 | \mBreak
  g='1  | c  | h      | a  | e  | g( | f) | \mBreak
  e='1~ | e~ | e~     | e~ | e~ | e~ | e~ | e\fermata \bar"|."
}

alto = \relative c' {
  \global
  R1*4 | e1 | a,2 e' | f e4( d) | c2 d | \mBreak
  e='2 h | c cis | d e~ | e f~ | f e4( d) | c2 a' | g f | e c' | d1~ | d2 c4( h) | \mBreak
  c=''2 c, | d h | h' h~ | h a~ | a gis | a r | r e | g a | g e | d( c) | \mBreak
  h=4 g' c2~ | c4 a d2~ | d4 fis, gis2\trill | a r4 e | h'2. h,4 |
    e2. c4 | c'2.( h8 a | d2) a | r4 d,=' f d | \mBreak
  h'='2.( a8 gis | a4) d c2 | h r | R1*3 | e,1 | a,2 e' | f e4( d) | c2 d | \mBreak
  e='2 h | c cis | d e~ | e f~ | f e4( d) | c2 a' | g f | e c' | d1~ | d2 c4( h) | \mBreak
  c=''2 c,=' | d h | h' h~ | h a~ | a gis | a r | r e | g a | g e | d( c) | \mBreak
  h=4 g' c2~ | c4 a d2~ | d4 fis, gis2\trill | a r4 e | h'2. h,4 |
    e2. c4 | c'2.( h8 a | d2) a | r4 d,=' f d | \mBreak
  h'='2.( a8 gis | a4) d c2 | h e,=' | c f | e d | g4( f) e2 | d1~ | d4 h e e | \mBreak
  d='4 g f e | d a'='2 gis4 | a2 d,=' | e fis | g4 d g g | f1~ | f4 d g g | e a g fis | \mBreak
  g='4 d g2~ | g4 fis8( e) fis2~ | fis4 gis a2~ | a gis | a4 d,=' g g | f d'='' c h | c2 r | R1*5 | \mBreak
  r2 f,=' | e f | g c, | e d | \mBreak
  c='2 r4 a'=' | a( gis2) g4 | fis( f2) e4 | d e fis2~ | fis4 g a h | c g c2~ | c h | c r | \mBreak
  r4 c,=' g'2 | a4( h) c2~ | c4 g a2~ | a4 g f2 | e4 c'2 h4 | a d2 c4 | h2.( a8 gis | a4) a, fis' gis | \mBreak
  a='4( g f2~ | f4) h, c d | e1\trill | d4 g2 f4 | e4 c'2 h4 | a2( g4 f) | e2 e~ | e4 d2 c4 | \mBreak
  h=4 a h2 | a e'4( f) | g2 gis | a h | c h | a1~ | a2 gis4 fis | gis1\fermata
  \bar"|."
}

tenor = \relative c' {
  \global
  h=1 | e,2 h' | c h4( a) |g2 a | h h | c cis | d e( | f) a,~ |
  a=2 gis | a1~ | a4 d g, a | b2 a | g gis | a4( h) c2 | h4( c) d2 | c a' | g f | e1~ |
  e='2 e | h e | f e4( d) | c2 d | e h | c d | c a | g( f) | e4 g c2~ | c4 h f'2~ |
  f='4 e8( d) g2 | f2. a,4 | d2. h4 | e2. c4 | f2 e |
    r4 c g'2~ | g4 a, d2~ | d4 h e2~ | e4 d8( cis d4) f |
  gis,=2. a8( h) | c4 gis a2 | gis h | e, h' | c h4( a) |
    g2 a | h h | c cis | d e( | f) a,~ |
  a=2 gis | a1~ | a4 d g, a | b2 a | g gis |
    a4( h) c2 | h4( c) d2 | c a' | g f | e1~ |
  e='2 e | h e | f e4( d) | c2 d | e h | c d | c a | g( f)| e4 g c2~ | c4 h f'2~ |
  f='4 e8( d) g2 | f2. a,4 | d2. h4 | e2. c4 | f2 e |
    r4 c g'2~ | g4 a, d2~ | d4 h e2~ | e4 d8( cis d4) f |
  gis,=2. a8( h) | c4 gis a2 | gis r | R1*3 | r2 a | g c |
  h=2 a | d4( c) h2 | a4 e a a | g c h a | g a h cis | d a d d | h f' e d | c g a h8( c) |
  d='2 r | r4 a d d | d h e e | d f e d | c d e2(~ | e4 d8 e f4) f | e2 c | h c |
  d='2 g, | h a | g4 cis2 c4 | h( b2) a4 | g( a) h2 | c4 c h b | b2 a | h e |
  e='4 c f2~ | f4 e d2~ | d4 g,2 g4 | fis( g) a( h) | c2 c~ | c4 f2 es4 | d( c) d2 | e g, |
  c='2 h | a e | g( f) | e4 e'2 d4 | c a e'2 | f4 g a2~ | a4 e f2~ | f4 e d2 |
  c='4 h a d | g,2 a4( h) | e,( f) g a | h e2 d4 | c e e,2~ | e4( f) g( a) | b2 a~ | a4 h2 a4 |
  h=4 c d2~ | d4 c8 h c2 | h4 f' e d | c( h) a( gis) | a2( gis) | a4 e' d c | h1 | h\fermata \bar"|."
}

bass = \relative c {
  \global
  R1*8 |
  e=1 | a,2 e' | f e4( d) | c2 d | e1 | r2 e4 ( f) | g2 gis | a1 | h | c2 h |
  a=1~ | a2 gis4( fis) | gis2 gis | a f | e4( d) e2 | a r | R1*2 | r2 e | g a |
  g=2 e | d( c) | h r | r4 e a2~ | a4 gis h2~ | h4 a c2~ | c4 c, f2~ | f4 d a'2~ | a4 a d2~ |
  d='4 gis, c2~ | c4 h c a | e'2 e, | R1*7 |
  e=1 | a,2 e' | f e4( d) | c2 d | e1 | r2 e4 ( f) | g2 gis | a1 | h | c2 h |
  a=1~ | a2 gis4( fis) | gis2 gis | a f | e4( d) e2 | a r | R1*2 | r2 e | g a |
  g=2 e | d( c) | h r | r4 e a2~ | a4 gis h2~ | h4 a c2~ | c4 c, f2~ | f4 d a'2~ | a4 a d2~ |
  d='4 gis, c2~ | c4 h c d | e2 e, | r4 e a a | g c h a | g1~ | g4 g fis e | d2 r |
  R1 | r2 e=2 | c f | e d | g4( f) e2 | d1 | r | r4 e a a |
  g=4 h a g | d'2 r4 c | h8( a) h4 a2 | r4 d e e, | f2 r4 cis' | d2 d, | a' r | R1*2
  r2 f | e f | g c, | e d | c r4 d | e( f2) fis4 | g( gis2) e4 |
  a=2 a | b r4 h, | h( c2) cis4 | d2 dis~ | dis4 e e dis | e d( e) fis | g2 g, | c r |
  R1*3 | r2 d | a' g | f c | e( d) | c4 c'2 h4 |
  a=4 d2 c4 | h4( e2 d4) | c r c,2 | g'2 a4( h) | c1~ | c2 g~ | g a~ | a g~ |
  g=4( f) e d | c2 a' | h h, | c( h) | a4 e'2 d4 | c2 h4 a | e1 | e\fermata \bar"|."
}

sopranoVerse = \lyricmode {
  Aus tie -- fer Noth schrei ich zu dir, __
  Herr Gott, er -- hör’ mein Ru -- fen! __
  Dein’ gnä -- dig’ Ohr’ neig her zu mir, __
  und mei -- ner Bitt’ sie öff -- ne. __
  Denn so du willt das se -- hen an, __
  was Sünd’ und Un -- recht __ ist ge -- than, __
  wer kann, Herr, vor dir blei -- ben? __
}

altoVerse = \lyricmode {
  Aus tie -- fer Noth schrei ich zu dir,
  aus tie -- fer Noth schrei __ ich __ zu dir,
  aus tie -- fer Noth schrei ich __ zu dir,
  aus tie -- fer Noth schrei __ ich __ zu dir,
  Herr Gott, er -- hör’ mein Ru -- fen,
  Herr Gott, __ er -- hör’ __ mein Ru -- fen,
  Herr Gott, er -- hör’ mein Ru -- fen,
  Herr Gott, er -- hör’ __ mein Ru -- fen!
  Dein’ gnä -- dig’ Ohr’ neig her zu mir,
  dein’ gnä -- dig’ Ohr’ neig __ her __ zu mir,
  dein’ gnä -- dig’ Ohr’ neig her __ zu mir,
  dein’ gnä -- dig’ Ohr’ neig __ her __ zu mir,
  und mei -- ner Bitt’ sie öff -- ne,
  und mei -- ner Bitt’ __ sie öff -- ne,
  und mei -- ner Bitt’ sie öff -- ne,
  und mei -- ner Bitt’ __ sie öff -- ne.
  Denn so du willt das se -- hen an, __
  denn so du willt das se -- hen an, se -- hen an,
  denn so du willt das se -- hen an, __
  denn so du willt das se -- hen an,
  denn so __ du willt __ das se -- hen an,
  denn so du willt das se -- hen an,
  was Sünd’ und Un -- recht ist ge -- than,
  was Sünd’ und Un -- recht ist ge -- than, __
  was Sünd’ und Un -- recht ist __ ge -- than,
  wer kann, Herr, vor __ dir blei -- _ _ ben,
  wer kann, Herr, vor dir blei -- ben,
  vor dir blei -- ben, vor dir blei -- ben,
  wer kann, Herr, vor dir blei -- ben,
  wer __ kann, Herr, vor dir blei -- ben,
  wer kann, Herr, vor dir blei -- _ _ _ _ ben?
}

tenorVerse = \lyricmode {
  Aus tie -- fer Noth schrei ich zu dir,
  aus tie -- fer Noth schrei __ ich __ zu dir, __
  aus tie -- fer Noth schrei ich zu dir,
  aus tie -- fer Noth schrei ich zu dir, __
  aus tie -- fer Noth schrei ich zu dir,
  Herr Gott, er -- hör’ mein Ru -- fen,
  Herr Gott, __ er -- hör’ __ mein Ru -- fen,
  Herr Gott, er -- hör’ mein Ru -- fen,
  Herr Gott, __ er -- hör’ __ mein Ru -- fen, __
  Herr Gott, er -- hör’ mein Ru -- fen!
  Dein’ gnä -- dig’ Ohr’ neig her zu mir,
  dein gnä -- dig’ Ohr’ neig __ her __ zu mir, __
  dein gnä -- dig’ Ohr’ neig her zu mir,
  dein gnä -- dig’ Ohr’ neig her zu mir, __
  dein gnä -- dig’ Ohr’ neig her zu mir,
  und mei -- ner Bitt’ sie öff -- ne,
  und mei -- ner Bitt’ __ sie öff -- ne,
  und mei -- ner Bitt’ sie öff -- ne,
  und mei -- ner Bitt’ __ sie öff -- ne, __
  und mei -- ner Bitt’ __ sie öff -- ne.
  Denn so du willt das se -- hen an,
  denn so du willt das se -- hen an, das se -- hen an,
  denn so du willt das se -- hen an, das se -- hen an,
  denn so du willt, denn so du willt das se -- hen an, das se -- hen an,
  was Sünd’ und Un -- recht ist ge -- than,
  Sünd’ und Un -- recht ist ge -- than,
  was Sünd’ und Un -- recht ist ge -- than,
  was Sünd’ __ und Un -- recht,
  was Sünd’ und Un -- recht, __ Un -- recht ist ge -- than,
  wer kann, Herr, vor dir blei -- ben, wer kann vor dir,
  wer kann, Herr, vor __ dir blei -- _ _ ben,
  wer kann, Herr, vor dir blei -- ben,
  wer kann, Herr, vor dir, Herr, vor __ dir blei -- ben, __
  wer kann vor dir blei -- _ _ _ ben,
  wer kann, Herr, vor __ dir __ blei -- ben,
  Herr, vor dir blei -- ben?
}

bassVerse = \lyricmode {
  Aus tie -- fer Noth schrei ich zu dir,
  aus tie -- fer Noth schrei ich zu dir, __
  schrei ich zu dir, schrei ich zu dir,
  Herr Gott, er -- hör’ mein Ru -- fen,
  Herr Gott, __ er -- hör’ __ mein Ru -- fen,
  Gott, __ er -- hör’ __ mein Ru -- fen,
  Gott, __ er -- hör’ mein Ru -- fen!
  Dein’ gnä -- dig’ Ohr’ neig her zu mir,
  dein’ gnä -- dig’ Ohr’ neig her zu mir, __
  dein’ gnä -- dig’ Ohr’ neig her zu mir,
  und mei -- ner Bitt’ sie öff -- ne,
  und mei -- ner Bitt’ __ sie öff -- ne,
  mei -- ner Bitt’ __ sie öff -- ne,
  mei -- ner Bitt’ sie öff -- ne.
  Denn so du willt das se -- hen an, __ das se -- hen an,
  denn so du willt das se -- hen an,
  denn so du willt das se -- hen an,
  denn so du willt das se -- hen an, das se -- hen an,
  was Sünd’ und Un -- recht ist ge -- than,
  was Sünd’ und Un -- recht ist ge -- than,
  was Sünd’ und Un -- _ recht ist ge -- than, Un -- recht ist ge -- than,
  wer kann, Herr, vor dir blei -- ben,
  wer kann, Herr, vor dir blei -- ben,
  wer kann, Herr, vor __ dir __ blei -- _ ben,
  wer kann, Herr, vor dir blei -- ben,
  wer kann, Herr, vor dir blei -- ben?
}


bassFigures = \figuremode {
  \global
  % missing
}
