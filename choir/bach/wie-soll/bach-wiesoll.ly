\version "2.25.22"
\language deutsch
#(ly:set-option 'relative-includes #t)


\include "layout.ily"

global = {
  \key a \minor
  \time 4/4
  \partial 4
  \autoBeamOff
  \accidentalStyle modern-voice
}

fe = \fermata

soprano = \relative c' {
  \global
  \repeat volta 2 {
    e4 | a g f e | d2 e4\fe
    h'4 c c h8([ c16 d]) h4 | a2.
  } \break
  c4 | h8[ a] g4 a h | c2 c4\fe
  g4 | a g f8([e]) f4 | e2.\fe
  c'4 | h8([ c]) d4 c h | a2 h4\fe
  e,4 | f e d g8[ f] | e2.\fe
  \bar "|."
}

alto = \relative c' {
  \global
  \repeat volta 2 {
    e8[( d]) | c[( d]) d[( e]) e[( d]) d[( c]) | c4( h8[ a]) h4\fe
    e4 | e a a gis | e2.
  }
  a8[ g] | f4 e8[ d] c4 f | f( e8[ d]) e4\fe
  e4 | f b,8[( a]) a[( g]) a[( d]) | cis2.\fe
  d4 | d g g8[ fis] g4 | g( fis) g\fe
  g4 | c,8[ d] c4 f, d' | d( c h)\fe
}

tenor = \relative c' {
  \global
  \repeat volta 2 {
    gis4 | a h c8([ d]) g,4 | a8[( gis] a4) gis\fe
    gis4 | a8[ h] c4 f e8[ d] | c2.
  }
  e4 | d8[ c] h4 a8[( g]) f[( g]) | a4( g8[ f]) g4\fe
  c4 | c8[( d]) e4 d8[( cis]) d[( gis,]) | a2.\fe
  a4 | g8[ a] h4 c8[ d] e4 | e( d) d\fe
  c4 | c8[( h]) h[( a]) a4 g8[ a] | h4( a gis)\fe
}

bass = \relative c {
  \global
  \repeat volta 2 {
    e4 | f g a8[ h] c4 | f,2 e4\fe
    e4 | a8[ g] f[ e] d[ h] e4 | a,2.
  }
  a'4 | d, e f8[ e] d4 |  a8[( h] c4) c\fe
  c4 | f8[( e]) d[( cis]) d4 d | a2.\fe
  fis'4 | g4. fis8 e4. d8 | c[( a] d4) g,\fe
  c8[ h] | a[ h] c4 c8[ h] h[ a] | gis4( a e')\fe
}


text = \lyricmode {
  Wie soll ich dich emp -- fan -- gen und wie be -- gegn’ __ ich dir?
  O Je -- su, Je -- su, set -- ze mir selbst die Fa -- ckel bei,
  da -- mit was dich er -- göt -- ze, mir kund und wis -- send
  \tag #'textandline \lyricmode { sei! __ }
  \tag #'noline { sei! }
}

textB = \lyricmode {
  O al -- ler Welt Ver -- lan -- gen, o mei -- ner See -- len Zier!
}

\bookpart {
%\tocItem \markup \tocline 1 "J. S. Bach" "Wie soll ich dich empfangen"

\header {
  title = "Wie soll ich dich empfangen"
  subtitle = "5. Choral – Weihnachtsoratorium"
  composer = "J. S. Bach"
  shortcomposer = "Bach"
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "BachJS"
  mutopiapoet = "Paul Gerhardt"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "Choir (SATB)"
  date = "1734"
  lastupdated = "2013-12-25"
  %footer = "Mutopia-2014/01/00-000"
}


\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = "Sopran"
    } { \soprano }
    \addlyrics { \keepWithTag #'noline \text }
    \addlyrics { \textB }
    \new Staff \with {
      instrumentName = "Alt"
    } { \alto }
    \addlyrics { \text }
    \addlyrics { \textB }
    \new Staff \with {
      instrumentName = "Tenor"
    } { \clef "treble_8" \tenor }
    \addlyrics { \text }
    \addlyrics { \textB }
    \new Staff \with {
      instrumentName = "Bass"
    } { \clef "bass" \bass }
    \addlyrics { \text }
    \addlyrics { \textB }
  >>
  \layout { }
  \midi {
    \tempo 4 = 80
  }
}
}
%{
% MIDI-Dateien zum Proben:
\book {
  \bookOutputSuffix "Sopran"
  \score {
    \rehearsalMidi "soprano" "oboe" \text
    \midi { }
  }
}

\book {
  \bookOutputSuffix "Alt"
  \score {
    \rehearsalMidi "alto" "oboe" \text
    \midi { }
  }
}

\book {
  \bookOutputSuffix "Tenor"
  \score {
    \rehearsalMidi "tenor" "oboe" \text
    \midi { }
  }
}

%\midifor #"bass" #"oboe" #\text

\book {
  \bookOutputSuffix "Bass"
  \score {
    \rehearsalMidi "bass" "oboe" \text
    \midi { }
  }
}
%}
