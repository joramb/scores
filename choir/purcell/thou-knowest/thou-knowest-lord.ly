\version "2.25.22"
\language deutsch


%
\header {
  title = "Thou knowest, Lord"
  composer = "Henry Purcell"
  shortcomposer = "Purcell"
  tagline = \markup \with-url "http://lilypond.org/"
    #(format "LilyPond ~:@{~a.~a~} – www.lilypond.org" (ly:version))
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2014 Joram Berger, joramberger.de"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "PurcellH"
  mutopiapoet = "Purcell"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "Choir (SATB)"
  license = "cc-by-sa"
  date = "1734"
  lastupdated = "2014-10-11"
  %footer = "Mutopia-2014/01/00-000"
}

#(set-global-staff-size 19)

\paper {
  evenHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  oddHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  top-margin = 15\mm
  bottom-margin = 15\mm
  left-margin = 15\mm
  right-margin = 15\mm
  top-system-spacing.basic-distance = 10
  last-bottom-spacing.basic-distance = 10
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
}

%\include "stylesheets/fonts/font-register.ily"

sffz = #(make-dynamic-script "sffz")
pf = #(make-dynamic-script "pf")

\layout {
  \override Staff.Clef.font-family = #'profondo
}

%\include "stylesheets/fonts/profondo.ily"

%\include "stylesheets/fonts/profondo.ily"

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L italic"
  \override Staff.ClefModifier.extra-offset = #'(0 . 0.07)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Staff.NoteHead.style = #'baroque
}

soprano = \relative c'' {
  \set Staff.instrumentName = "Sopran"
  \key g \minor
  \time 2/2
  \set Timing.measureLength = \musicLength 2*4
  r2 g g g | f f g b | b a b1 | r2 d1 d2 |
  r2 c1 c4 c | c c c( d) b2 d2 | g,2. a4 fis2. fis4 | r2 a b2. b4 |
  b2 r a2. a4 | b2 g a2. a4 | r2 d cis1 | r2 f d2. cis4 | d2. d4 r2 d |
  es2. es 4 es2 d | c4 c d( es) h2. h4 | r2 h4 h c2 c | c h c2. c4 |
  r2 a2. a4 a2 | b1 r2 h ~ | h4 h h2 c c4 b | a2( g) fis b | c d a b |
  fis b c d | d4( a) b2 fis b  | a g g fis | g1 r2 es | d\breve
  \bar "|."
}

alto = \relative c' {
  \set Staff.instrumentName = "Alt"
  \key g \minor
  r2 es es es | d d es f | g f f1 | r2 f1 f2 |
  r2 f1 f4 f | d d d2 d d | d c d2. d4 | r2 fis g2. g4 |
  g2 r f2. f4 f2 e f2. f4 | r2 a a1 | r2 a a2. a4 | a2. a4 r2 g |
  g2. g4 g2 g | g4 g f2 g2. g4 | r2 g4 g g2 g | f2. g4 e2. e4 |
  r2 f2. f4 f2 | f1 r2 f ~ | f4 f f2 g g4 f | es1 c2 g' | a b fis g |
  d g a b | fis g d f | es d c d | h1 r2 c | h\breve
}

tenor = \relative c' {
  \set Staff.instrumentName = "Tenor"
  \clef "treble_8"
  \key g \minor
  r2 b b b | b b b4( c) d2 | es c d1 | r2 b1 b2 |
  r2 a1 a4 a | a a a2 g a | b g a2. a4 | r2 d d2. d4 |
  c2 r c2. c4 | d2 c c2. c4 | r2 f e1 | r2 d e2. e4 | f2. f4 r2 d |
  c2. c4 c2 d es4 es d( c) d2. d4 r2 es4 d es2 es d2. es4 c2. c4
  r2 c2. c4 c2 d1 r2 d ~ | d4  d d2 es es4 d c2( b) a r  | r1 r2 b |
  c d a d | c b a d | a b c b4( a) | g1 r2 g | g\breve
}

bass = \relative c {
  \set Staff.instrumentName = "Bass"
  \clef bass
  \key g \minor
  r2 es es es | b b es d | c f b,1 | r2 b'1 b2 |
  r2 f1 f4 f | fis fis fis2 g f | es es d2. d4 | r2 d g2. g4 |
  e2 r f2. f4 | b,2 c f2. f4 | r2 d a'1 | r2 d a2. a4 d2. d4 r2 h |
  c2. c4 c2 b! | as4 as as2 g2. g4 | r2 g4 f es 2 c | g2. g4 c2. c4 |
  r2 f2. f4 es!2 | d1 r2 d ~ | d4 d d2 c c4 d | es2( c) d1 | r r2 g |
  a b fis g d1. d2 | c b a d | g,1 r2 c | g\breve
}

text = \lyricmode {
  Thou know -- est, Lord, the se -- crets of our hearts;
  shut not, shut not thy mer -- ci -- ful ears un -- to our pray -- er;
  but spare us, Lord, spare us, Lord most ho -- ly, O God, O God most might -- y.

  O ho -- ly and most mer -- ci -- \tag "S" \tag "T" ful __ \tag "A" \tag "B" ful Sa -- viour,
  thou most wor -- thy Judge e -- ter -- nal,
  suf -- fer us not, suf -- fer us not at our \tag "S" \tag "T" \tag "B" last __ \tag "A" last hour, for a -- ny
  \tag "S" \tag "A" { pains of death, for a -- ny pains of death, to fall, }
  \tag "T"          { pains, for a -- ny pains of death, }
  \tag "B"          { pains of death, to fall, }
  to fall from thee. A -- men.
}

\score {
  \new ChoirStaff <<
    \new Staff \new Voice = sop \soprano
    \new Lyrics \keepWithTag "S" \lyricsto sop \text
    \new Staff \new Voice = alt \alto
    \new Lyrics \keepWithTag "A" \lyricsto alt \text
    \new Staff \new Voice = ten \tenor
    \new Lyrics \keepWithTag "T" \lyricsto ten \text
    \new Staff \new Voice = bas \bass
    \new Lyrics \keepWithTag "B" \lyricsto bas \text
  >>
}
