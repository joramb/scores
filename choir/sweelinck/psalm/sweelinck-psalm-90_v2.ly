\version "2.25.22"
\language deutsch
#(set-global-staff-size 17.3)

\include "stylesheets/joram/settings.ily"

\header {
  title = "Herr, du bist unsre Zuflucht"
  instrument = "Psalm 90"
  composer = "Jan Pieterszoon Sweelinck" % (1562 - 1621; Amsterdam)" 
  shortcomposer = "Sweelinck"
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2013 Joram Berger, joramberger.de"
    \with-url "http://www2.cpdl.org/wiki/index.php/User:Maximilian_Albert"
    "(Original: Maximilian Albert)"
    "– Lizenz:"
    \with-url "https://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = ""
  mutopiacomposer = "SweelinckJP"
  %mutopiapoet = ""
  %mutopiaopus = "BWV 248"
  mutopiainstrument = "choir"
  date = "2013-12-25"
  %footer = "Mutopia-2012/12/00-000"
}



\paper {
  page-count = 2
  system-count = 8
}

global = {
  %\set Staff.midiInstrument = "string ensemble 1"
  \dynamicUp
  %\set hairpinToBarline = ##f
  \key c \major
  \time 4/2
  % Change the time signature symbol (but don't change the actual time signature settings)
  \set Score.timeSignatureFraction = 4/4
}

mBreak = { }

sopMusic = \relative c'' {
  \set Staff.instrumentName = "Sopran "

  % Herr, du bist unsre Zuflucht
  a\breve |
  a\breve | \mBreak
  d1 c4 a c c |
  h4 \melisma a \melismaEnd gis2 a r | \mBreak
  e'2 d4 c h a r c |
  h4 a gis2 a r4 e'4 | \mBreak

  % ein sichrer Hort
  c4 a e' c a f c' a |
  f4 d a'1 a4 c | \mBreak
  h2 a4 d2 \melisma cis8 h \melismaEnd cis2 |
  d1. a2 |

  % Ehe die Berge
  b2 g a f | \mBreak
  g2 a d, f |
  e1 d2 a'~ |
  a2 h4 cis d2 a4 d | \mBreak
  d2 c4 c h1 |
  a\breve~ |
  a\breve | \mBreak

  % bist du, Gott Zebaoth
  r2 r4 a4. g8 a4 f d |
  a'2 b a g | \mBreak
  f4 a d c a2 r4 a |
  d4 c a1 r4 e | \mBreak
  f4 e c a'2 a4 f2 |
  a2 g f f4 e | \mBreak
  d2 r1 a'2~ |
  a4 d2 c4 a1 |
  f2. g4 d1 | \mBreak
  f4 g d2 a'2. a4 |
  fis2 a1 g2 |
  fis\breve \bar "|."
}

altoMusic = \relative c' {
  \set Staff.instrumentName = "Alt "

  % Herr, du bist unsre Zuflucht
  a2 a4 d c a c f |
  e4\melisma c\melismaEnd f2 e8\melisma d c h a2\melismaEnd |
  r2 r4 h e2. e4 |
  d4 c h2 a r2 |
  r4 c h a gis a r2 |

  % ein sichrer Hort
  r2 e'1 c2 |
  a2. e'4 r4 a f2 |
  r4 a f d e4.\melisma d8 c4\melismaEnd g'~ |
  g4 g fis\melisma g8\melismaEnd g a1~ |

  % Ehe die Berge
  a2 a,1 f'!2 |
  d2 e c d |
  e2 a,4 c h2 a |
  r2 a'2.\melisma g4 fis e |
  fis2\melismaEnd g4 g fis2 fis4 a |
  g2 e4 e e2 e4 e~ |
  e8 d e4 c a e'1 |
  f2 e d cis |
  r2 f e d |
  c8\melisma d e f g2\melismaEnd r4 c, d e |
  f1 r4 a,4 d c |
  a2 r4 c4 f e c2 |
  r2 r4 c2 d4 a2 |
  f'4 e d2 a'1~ |
  a2 g fis1 |
  d2 a' f e |
  d2. e4 f2 r |
  r2 f4 g c, d2 cis4 |
  d\breve~ |
  d\breve
}

tenorMusic = \relative c' {
  \set Staff.instrumentName = "Tenor "
  \clef "treble_8"

  % Herr, du bist unsre Zuflucht
  R\breve |
  r4 a a d c a c c |
  h\melisma a\melismaEnd gis2 a1 |
  r2 r4 e' e e d c |
  h4 a r e' e2 d4 e4~ |
  e4 c h2 a e'~ |

  % ein sichrer Hort
  e2 c1 a2 |
  d2 d c4 a e' e~ |
  e4 d d4. e8 f4\melisma e8 d\melismaEnd e2 |
  fis1 r4 a,2 d4~ |

  % Ehe die Berge
  d4 g,2 c a f4 |
  c'2 c4 a r d4.\melisma c8 a h |
  cis4 d2 cis4\melismaEnd d1 |
  d2 d4 e a,2 d4 d |
  h2 a4 a gis\melisma a2 gis4\melismaEnd |
  a1 r4 a4. g8 a4 |
  f4 d a'2 f' e~ |
  e2 d cis r |
  r4 c d e f2 r |
  r2 r4 a,4 d c a2 |
  r2 r4 a4 d c a2~ |
  a1 r2 c4 d |
  a4 c2 b4 a2 a4 a |
  d,4 d'2 cis4 d1 |
  r2 r4 a2 d c4 |
  a2 r r f4 g |
  d1 f2 e |
  d2 fis4 g a2 b4 b |
  a\breve
}

bassMusic = \relative c {
  \set Staff.instrumentName = "Bass"
  \clef "bass"

  % Herr, du bist unsre Zuflucht
  r1 a2 a4 d |
  c4 a d8\melisma e\melismaEnd f g a4.\melisma g8\melismaEnd f4 e |
  d4 c h2 a1 |
  r2 e' c'4 c h a |
  gis4 a r2 r4 c h a |
  gis4 a e2 a,1 |

  % ein sichrer Hort
  r2 a' f1 |
  d1 a'2 a4 e |
  g2 d4 b' a1 |
  d,1 r2 d |

  % Ehe die Berge
  g2 c, f d |
  c2 f g4 d d8\melisma e f g |
  a2\melismaEnd a, r d~ |
  d2 g4 e d2 d4 fis |
  g2 a4 a, e'1 |
  a,1. r2 |

  % bist du, Gott Zebaoth
  r1 r2 r4 a'~ |
  a8 g a4 f d a'2 b |
  a2 g f4 a b c |
  f,\breve~ |
  f1 r2 r4 a,4 |
  d4 c a2 f'1~ |
  f2 g d1 |
  f2 e d1 |
  r1 d2 a'4 a, |
  d2 d4 c b1 |
  b1 a |
  <<
    { \voiceTwo d,\breve ~ d\breve }
    \new Voice {\voiceOne d'2 d4 e fis2 g4 g, d'\breve }
  >>
}

sopWords = \lyricmode {
  Herr, du bist uns -- re Zu -- flucht, für __ und für
  Herr, uns -- re Zu -- flucht,
  Herr, uns -- re Zu -- flucht,

  ein sich -- rer Hort
  und uns -- re Hil -- fe für und für,
  Hort und Hil -- fe für __ und für.

  E -- he die Ber -- ge und die Mee -- re wur -- den,
  e -- he die Welt und die Er -- de ge -- schaf -- fen, __
  bist du, Gott Ze -- ba -- oth,
  von E -- wig -- keit zu E -- wig -- keit
  von E -- wig -- keit zu E -- wig -- keit,
  bist du Gott, im -- mer -- dar, im -- mer -- dar,
  von E -- wig -- keit, bist du Gott,
  im -- mer -- dar,  im -- mer -- dar, im -- mer -- dar.
}

altoWords =\lyricmode {
  Herr, du bist uns -- re Zu -- flucht, für __ und für __
  Herr, du bist uns -- re Zu -- flucht,
  Herr, uns -- re Zu -- flucht,

  ein sich -- rer Hort, Hil -- fe
  ein sich -- rer Hort __
  Hil -- fe für __ und für. __

  E -- he die Ber -- ge und die Mee -- re wur -- den,
  e -- he die Welt und die Er -- de ge -- schaf -- fen,
  bist __ du, Gott Ze -- ba -- oth,

  von E -- wig -- keit zu E -- wig -- keit, __
  von E -- wig -- keit zu E -- wig -- keit,
  zu E -- wig -- keit,
  bist du Gott, imm -- mer -- dar, im -- mer -- dar,
  von E -- wig -- keit, bist du Gott,
  im -- mer -- dar, im -- mer -- dar. __
}

tenorWords = \lyricmode {
  Herr, du bist uns -- re Zu -- flucht, für __ und für
  Herr, du bist uns -- re Zu -- flucht,
  Herr, du bist uns -- re Zu -- flucht,

  ein __ sich -- rer Hort, ein sich -- rer Hort,
  uns -- re Hil -- fe für __ und für.
  E -- he __ die Ber -- ge und die Mee -- re wur -- den,
  e -- he die Welt und die Er -- de ge -- schaf -- fen,
  bist du, Gott Ze -- ba -- oth,
  von E -- wig -- keit zu E -- wig -- keit,
  von E -- wig -- keit zu E -- wig -- keit, __
  bist du Gott, im -- mer -- dar,
  bist du Gott, im -- mer -- dar,
  zu E -- wig -- keit,
  im -- mer -- dar, im -- mer -- dar,
  bist du Gott im -- mer -- dar.
}

bassWords = \lyricmode {
  Herr, du bist uns -- re Zu-flucht, _ _
  Herr __ du bist uns -- re Zu -- flucht,
  Herr, du bist uns -- re Zu -- flucht,
  Herr, du bist uns -- re Zu -- flucht,

  ein sich -- rer Hort,
  Herr, du bist uns -- re Hil -- fe.
  E -- he die Ber -- ge und die Mee -- re wur -- den,
  e -- he die Welt und die Er -- de ge -- schaf -- fen,
  bist du, Gott Ze -- ba -- oth, von E -- wig -- keit
  zu E -- wig -- keit, __
  zu E -- wig -- keit, bist __ du Gott, im -- mer -- dar,
  zu E -- wig -- keit, im -- mer -- dar,
  im -- mer -- dar. __
}

\score {
  \new ChoirStaff <<
    \new Staff \new Voice = "sopranos" << \global \sopMusic >>
    \new Lyrics \lyricsto sopranos \sopWords
    \new Staff \new Voice = "altos" << \global \altoMusic >>
    \new Lyrics \lyricsto altos \altoWords
    \new Staff \new Voice = "tenors" << \global \tenorMusic >>
    \new Lyrics \lyricsto tenors \tenorWords
    \new Staff \new Voice = "basses" << \global \bassMusic >>
    \new Lyrics \lyricsto basses \bassWords
  >>
  \layout { }
  \midi{ \tempo 2 = 60 }
}
