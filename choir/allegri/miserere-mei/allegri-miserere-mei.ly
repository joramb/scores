\version "2.25.22"
\language deutsch

#(set-global-staff-size 18)
\header {
  %dedication = ""
  title = "Miserere mei"
  %subtitle = ""
  %subsubtitle ="27. Sonntag nach Trinitatis"
  poet = "Psalm 51"
  composer = "G. Allegri"
  %meter ="Allegro moderato"
  %arranger ="Joram"
  %piece = \markup{\bold "1. Chorus"}
  %opus = "BWV 140"
  %copyright = "© Berger"
  tagline = ##f
}

%TODO: einzeln midi, stimmen anordnen, Text ([] einführen), als gesang d.h. auflösungszeichen pro stimme, Taktnummern richtig, jede Zeile mit Notenwert beginnen, keine unnötigen Notenwerte, | octavecheck pro seite, zeile pro zeile, mybreak, abschlusstaktstrich, DS

br = {}

global = {
  \time 4/2
  \key g \minor
  \tempo "Allegro moderato" 2 = 80
  %\dynamicUp
  \autoBeamOff
  %\compressEmptyMeasures
}


%Cantus
cantusnotes = \relative c'' {
  \clef treble
  d2. d4 d2 d | d1 d | b2( es1 d2 | c1~ c) | b1 c2 c4 c | c1 c2 r |  \break
  s2 r1 s2 | r2 c2.( d4) b a | b2_( es1 d2~ | d c4 b a g) fis a | b1~ b |  a~ a \break
  \bar".." s1*2
  c1. c2 | c1 c2 c | c2. g4 g2.( d4) |
  \time 6/2 d2( a') b1 b | \time 4/2 a1. d2 | h1~ h \bar"|."
}

%Secundus
secundusnotes = \relative c'' {
  \clef treble
  b2. b4 b2 b | a1 b | g_( f2 b2~ | b2 a4 g a1) | b a2 a4 a | a1 a2 r |
  s2 r1 s2 | s2 r1 s2 | g2 c4 c f,2_( b~ | b a4 g fis g) a fis | g1(~ g~ | g2 fis4 e) fis1
  \bar"|." s1*2
  g1. g2 | g1 g2 g | g g b( g) |
  \time 6/2 a1 g g | \time 4/2 a1. a2 | g1~ g |
}
%Alt
altusnotes = \relative c'' {
  \clef treble
  g2. g4 g2 g | f1 f2 f(~ | f2 es4 g c,2 d | f1~ f) | f f2 f4 f | f1 f2 f(~ |
  f4 g) es d es2.( f4 | g1.) g2 | g1 f | es2 es d d | d1~ d | d~ d | s1*2
  es1. es2 | es1 es2 es | es es g( d) |
  \time 6/2 d1 d d | \time 4/2 d1. d2 | d1~ d |
}

%Tenor
tenornotes = \relative c' {
  \clef "G_8"
  b2. b4 b2 b |  f a d1( ~ | d2 c4 b a2 b | c1 ~ c) | d1 c2 a4 a | a1 a2 r |
  g1 g2. f4 | es2 es' d d~ | d1 c2 b | g a a1(~ |a2 g4 fis g1) | a1~ a | s1*2
  g1. g2 | g1 g2 g | g c d1~ |
  \time 6/2 d2 d, d1 d2 d(~ | \time 4/2 d4 e fis g fis2.) g4 | d1~ d |
}


%Bass
bassusnotes = \relative c {
  \clef bass
  g2. g4 g2 g | d'1 b | es( f2 \stemDown b, | f1 ~ f) | b f'2 f4 f | f1 f2 r |
  c1 c2. d4 | es2.( f4) g1 | g,2.( a4 b1) | c1( d | g,~ g) | d'~ d | s1*2
  c1. c2 | c1 c2 c | c c g1 |
  \time 6/2 d'1 g, g | \time 4/2 d'1. d2 | g,1~ g |
}


cantuswords = \lyricmode {
  Mi -- se -- re -- re me -- i, De -- us, se -- cun -- dum ma -- gnam
  mi -- se -- ri -- cor -- di -- am tu -- am
  tunc im -- pon -- ent su -- per al -- ta re -- tu -- um vi -- tu -- los.
}
%secunduswords = \lyricmode {
%	Mi -- se -- re -- re me -- i, De -- us, se -- cun -- dum ma -- gnam
%	mi -- se -- ri -- cor -- di -- am tu -- am
%}
altowords = \lyricmode {
  Mi -- se -- re -- re me -- i, De -- us, se -- cun -- dum ma -- gnam
  mi -- se -- ri -- cor -- di -- am, mi -- se -- ri -- cor -- diam tu -- am
  tunc im -- pon -- ent su -- per al -- ta re -- tu -- um vi -- tu -- los.
}
%tenorwords = \lyricmode {
%	Mi -- se -- re -- re me -- i, De -- us, se -- cun -- dum ma -- gnam
%	mi -- se -- ri -- cor -- di -- am, mi -- se -- ri -- cor -- diam tu -- am
%}
%basswords = \lyricmode {
%	Mi -- se -- re -- re me -- i, De -- us, se -- cun -- dum ma -- gnam
%	mi -- se -- ri -- cor -- di -- am tu -- am
%}

\score {
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "cantus" << \global \cantusnotes >>
      \new Lyrics \lyricsto "cantus" \cantuswords
    >>
    \new Staff <<
      \new Voice = "secundus" << \global \secundusnotes >>
      \new Lyrics \lyricsto "secundus" \cantuswords
    >>
    \new Staff <<
      \new Voice = "altus" << \global \altusnotes >>
      \new Lyrics \lyricsto "altus" \altowords
    >>
    \new Staff <<
      \new Voice = "tenor" << \global \tenornotes >>
      \new Lyrics \lyricsto "tenor" \altowords
    >>
    \new Staff <<
      \new Voice = "bassus" << \global \bassusnotes >>
      \new Lyrics \lyricsto "bassus" \cantuswords
    >>
  >>
  \midi {}
  \layout {}
}
