\version "2.25.22"
\language deutsch


\header {
  title = "Pues que jamás olvidaros"
  composer = \markup \right-column { "Juan del Enzina" "(1469 ­­– 1523)"}
  shortcomposer = "Enzina"
  tagline = \markup \with-url "http://lilypond.org/"
    #(format "LilyPond ~:@{~a.~a~} – www.lilypond.org" (ly:version))
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2013 Joram Berger, joramberger.de"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "BachJS"
  mutopiapoet = ""
  mutopiaopus = ""
  mutopiainstrument = "choir"
  date = "2013-12-25"
  %footer = "Mutopia-2012/12/00-000"
}

#(set-global-staff-size 18)

\paper {
  evenHeaderMarkup = \markup \fill-line {
    \fromproperty #'page:page-number-string
    \fromproperty #'header:title
    \fromproperty #'header:shortcomposer
  }
  oddHeaderMarkup = \markup \fill-line {
    \unless \on-first-page \fromproperty #'header:shortcomposer
    \unless \on-first-page \fromproperty #'header:title
    \unless \on-first-page \fromproperty #'page:page-number-string
  }
  score-markup-spacing.basic-distance = #20
  markup-system-spacing.basic-distance = #20
  top-system-spacing.basic-distance = #10
  score-system-spacing.basic-distance = #20
  system-system-spacing.basic-distance = #25
  last-bottom-spacing.basic-distance = #10
  top-markup-spacing.basic-distance = #12
  top-margin = 15\mm
  bottom-margin = 12\mm
  ragged-bottom = ##t
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
}

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L italic"
  \override Staff.ClefModifier.extra-offset = #'(0 . 0.07)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Score.BarNumber.padding = #1.6
}


global = {
  \tempo "Tempo I" 4 = 108
  \key f \major
  \time 2/2
  \dynamicUp
}

sopranoVoice = \relative c' {
  \global
  % 1
  f1 | f | e2.( f4) | g2( a2 ~ | a4 g f e) |
  % 6
  d1 | d( | e) | d2.( e4) | f2 f \mark \default | e1( | a) |
  % 13
  f | f2( e4 d) | c2( f ~ | f4 e8 d) e2 | f1 |
  % 18
  \mark \default
  d2 d | d1 | e | f2 f | e1 |
  % 23
  \mark \default
  f2 g | a1 ~ | a2 g4 f | e2( a2 ~ | a4 g) f2( ~ | f e) | f1 |
  \repeat volta 2 {
    % 30
    \mark \default
    e1 | e | d | c2.( d4) | e2 f( ~ | f e) | f1 |
    % 37
    \mark \default
    a2 a | f1 | g2 f4 es | d2.( e4 | f g a2 ~ | a g4 f) | e1 |
  }
}

sopranoTextI = \lyricmode {
  \set stanza = #"1. "
  Pues que ja -- más __ ol -- vi -- da -- ros
  no pue -- de mi __ co -- ra -- çon
  si me fal -- ta ga -- lar -- dón.
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra tal vis -- ta co -- brar,
  gran do -- lor y gran tris -- tu -- ra.
}

sopranoTextII = \lyricmode {
  \set stanza = #"2. "
  Mas si vos, __ por __ bien a -- ma -- ros
  que -- réis __ dar -- me __ ga -- lar -- dón
  no di -- ra mi co -- ra -- çon
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra tal vis -- ta pe -- nar,
  si me fa -- lle -- ce ven -- tu -- ra.
}

altoVoice = \relative c' {
  \global
  % 1
  c1 | d | g,( ~ | g2 f)  | f1 |
  % 6
  f | b4.( a8 f2 | c'1) | f, | f2 f | g1( | a) |
  % 13
  b | b | c2( b) | c1 | a |
  % 18
  b2 b | f1 | c'4.( b8) a4( g) | f1 | g |
  % 23
  d'2. e4 | f1  ~ | f | r2 f4. e8 | c2( d) | c1 | c |
  \repeat volta 2 {
    % 30
    c1 | c | f,2.( g4) | a( b c2) | c b( | c1) | c |
    % 37
    a2 a | b1 | g | f | f2.( g4) | a1 | g |
  }
}

altoTextI = \lyricmode {
  \set stanza = #"1. "
  Pues que ja -- más ol -- vi -- da -- ros
  no pue -- de mi co -- ra -- çon
  si me fal -- ta ga -- lar -- dón
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra tal __ vis -- ta co -- brar,
  gran do -- lor y gran tris -- tu -- ra.
}

altoTextII = \lyricmode {
  \set stanza = #"2. "
  Mas si vos, por bien a -- ma -- ros
  que -- réis dar -- me ga -- lar -- dón
  no di -- ra mi co -- ra -- çon
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra tal __ vis -- ta pe -- nar,
  si me fa -- lle -- ce ven -- tu -- ra.
}

tenorVoice = \relative c {
  \global
  % 1
  f2.( g4) | a2( b) | c1 ~ | c | a |
  % 6
  b2( a4 g) | f2( b | g a) | b1 | a2 b | c1 ~ | c |
  % 13
  d1 | d2( c4 b) | a2( f) | g1 | f |
  % 18
  f2 f | b1 | g2 c2( ~ | c4 h8 a) h2 | c1 |
  % 23
  a2 b | c1 ~ | c | c | a2 b | g1 | f |
  \repeat volta 2 {
    % 30
    g | g2( a) | b1 | a | g2 f( | g1) | f |
    % 37
    c'2 c | d1 | b | b | a2.( b4) | c1 | c |
  }
}

tenorTextI = \lyricmode {
  \set stanza = #"1. "
  Pues que __ ja -- más ol -- vi -- da -- ros
  no pue -- de mi __ co -- ra -- çon
  si me fal -- ta ga -- lar -- dón
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra __ tal vis -- ta co -- brar,
  gran do -- lor y gran tris -- tu -- ra.
}

tenorTextII = \lyricmode {
  \set stanza = #"2. "
  Mas si __ vos, __ por bien __ a -- ma -- ros
  que -- réis __ dar -- me __ ga -- lar -- dón
  no di -- ra mi co -- ra -- çon
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra __ tal vis -- ta pe -- nar,
  si me fa -- lle -- ce ven -- tu -- ra.
}

bassVoice = \relative c {
  \global
  % 1
  f1 | d | c2.( d4) | e2( f2 ~ | f4 e d c) |
  % 6
  b1 | b( | c) | b2.( c4) | d2 d | c1( | f) |
  % 13
  b,1 | b | f'2( d) | c1 | f, |
  % 18
  b2 b | b1 | c | d2 d | c1 |
  % 23
  d2 g | f1 ~ | f2 e4 d |c2( f ~ | f b,2) | c1 | f, |
  \repeat volta 2 {
    % 30
    c' | c | b1 | f'2( e4 d) | c2 d | c1 | r2 f2( ~ | f4 e) f2 |
    % 37
    b,1 | es2 d4 c | b2.( c4 | d e f2 ~ | f e4 d4) | c1 |
  }
}

bassTextI = \lyricmode {
  \set stanza = #"1. "
  Pues que ja -- más __ ol -- vi -- da -- ros
  no pue -- de mi co -- ra -- çon
  si me fal -- ta ga -- lar -- dón
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra tal vis -- ta co -- brar,
  gran do -- lor y gran tris -- tu -- ra.
}

bassTextII = \lyricmode {
  \set stanza = #"2. "
  Mas si vos, __ por __ bien a -- ma -- ros
  que -- réis __ dar -- me ga -- lar -- dón
  no di -- ra mi co -- ra -- çon
  ¡Ay, que mal __ hi -- ze~en mi -- ra -- ros!
  \set stanza = #"1.+2."
  Se -- ra tal vis -- ta pe -- nar,
  si __ me fa -- lle -- ce ven -- tu -- ra.
}


sopranoVoicePart = \new Staff \with {
  instrumentName = "Sopran"
  %midiInstrument = "choir aahs"
} { \transpose f b \sopranoVoice }
\addlyrics { \sopranoTextI }
\addlyrics { \sopranoTextII }

altoVoicePart = \new Staff \with {
  instrumentName = "Alt"
  %midiInstrument = "choir aahs"
} { \transpose f b \altoVoice }
\addlyrics { \altoTextI }
\addlyrics { \altoTextII }

tenorVoicePart = \new Staff \with {
  instrumentName = "Tenor"
  %midiInstrument = "choir aahs"
} { \clef "treble_8" \transpose f b  \tenorVoice }
\addlyrics { \tenorTextI }
\addlyrics { \tenorTextII }

bassVoicePart = \new Staff \with {
  instrumentName = "Bass"
  %midiInstrument = "choir aahs"
} { \clef bass \transpose f b \bassVoice }
\addlyrics { \bassTextI }
\addlyrics { \bassTextII }

\score {
  \new ChoirStaff <<
    \sopranoVoicePart
    \altoVoicePart
    \tenorVoicePart
    \bassVoicePart
  >>
  \layout { }
  \midi { }
}

\markup {

  \fill-line {
    %    \hspace #0.1
    %    \column { " " }
    \hspace #0.1
    \column {
      \bold "            Deutsche Übersetzung"
      \line {
        \bold "            1."
        \column {
          "Seit mein Herz dich niemals mehr vergessen kann,"
          "habe ich falsch gehandelt, dich anzublicken,"
          "wenn ich nicht den Preis gewinne."
          "Diesen Anblick werde ich dann mit"
          "großen Schmerzen und tiefer Traurigkeit teuer bezahlen."
        }
      }
      \hspace #0.1
      \line {
        \bold "            2."
        \column {
          "Aber wenn du mir wegen treuer Liebe zu dir den Preis geben willst,"
          "dann wird mein Herz nicht mehr klagen,"
          "dass ich falsch gehandelt habe, dich anzublicken!"
        }
      }
    }
    \hspace #0.1
    \column {
      \bold "Spanische Aussprache"
      \line {
        \column {
          \italic "j"
          \italic "ll"
          \italic "qu"
          \italic "c(o)"
          \italic "c(e)"
          \italic "z"
          \italic "v"
        }
        \hspace #0.2
        \column {
          "wie ch [x]"
          "wie j [ʎ]"
          "wie k [k]"
          "wie k [k]"
          "wie th [θ]"
          "wie th [θ]"
          "wie b [b]"
        }
      }
    }
    \hspace #0.1
  }
}
