\version "2.25.22"
\language deutsch

\include "layout.ily"
%\include "stylesheets/joram/choir.ily"

\header {
  title = "Christ ist erstanden"
  % https://www.youtube.com/watch?v=WDQIIXM_oxQ
}

\paper {
  %#(set-paper-size "letter")
  ragged-bottom = ##t
  indent = 0
  system-system-spacing.basic-distance = #17
  markup-system-spacing.basic-distance = #14
  %markup-system-spacing.stretchability = #400
  top-margin = 3\cm
  left-margin = 2.5\cm
  right-margin = 2.5\cm
  bottom-margin = 3\cm
}

% https://www.youtube.com/watch?v=WDQIIXM_oxQ

eins = \relative {
  \voiceOne
  \cadenzaOn
  a'2 g4 a c( d) a2
  a4 g a f e( f) d r8
  d8 g4 g \bar "" d c f( g) a2 \bar ""
  a4 g a f e( f) d
  e2 c4 d2 d1
  \bar "||"

  a'4 a g4 a c( d) a r8
  a8 a4 g a f e( f) d2
  g4 g \bar ""  d c f g a r8
  a8 a4 g8 g a4 f e f d
  e2 c4 d2 d1
  \bar "||" \break

  a'2 g4( f) g( b) a2
  c2 a4( c) a( g) f2
  a2 f4( d) e( f) d r8 \bar ""
  d8 g4 g d c f( g) a2 \bar ""
  a4 g a f e( f) d
  e2 c4 d2 d1
  \bar "|."
}

zwei = \relative {
  \voiceTwo
  d'2 h4 d f g d2 \breathe
  d4 h d a c a a r8
  a8 h4 c a g a c f2 \breathe % f
  d4 h d a  c a b \breathe
  a2 g4 h2 a1

  d4 d h d f g d r8
  d8 d4 h d a c a a2 \breathe
  h4 c a g a c f r8
  d8 d4 h8 h d4 a  c a b \breathe
  a2 g4 h2 a1
  %  d2' h4 d | f g d2 | d4 h d a | c a a4. 8 |
  % h4 c a g | a c f4. 8 | d4 h d a | c a b a ~ | a g h2 a1
}

chord = \chordmode {
  \set chordNameLowercaseMinor = ##t
  \set chordChanges = ##t
  \germanChords
  s1*20
  f2 c/e c f
  c/e f \tweak text \markup \concat { "C" \super "4-3" } c:sus d:m
  a:m b c d:m
  g:m c f4 c f2
  a:m b c d4:m
  a2.:m d2:m
}

text = \lyricmode {
  \set stanza = "1."
  Christ ist er -- stan -- den von der Mar -- ter al -- le;
  des solln wir al -- le froh sein,
  Christ will un -- ser Trost sein.
  Ky -- ri -- e -- leis.

  \set stanza = "2."
  Wär er nicht er -- stan -- den,
  So wär die Welt ver -- gang -- en;
  seit dass er er -- stan -- den ist,
  so lobn wir den Va -- ter Je -- su Christ’.
  Ky -- ri -- e -- leis.

  \set stanza = "3."
  Hal -- le -- lu -- ja,
  Hal -- le -- lu -- ja,
  Hal -- le -- lu -- ja!
  Des solln wir al -- le froh sein,
  Christ will un -- ser Trost sein.
  Ky -- ri -- e -- leis.

}

\score {
<<
  \new ChordNames \chord
  \new Staff <<
    \new Voice = "ein" \eins
    \new Voice \zwei
  >>
  \new Lyrics \lyricsto "ein" \text
>>
\midi { \tempo 2 = 60 }
\layout {
  \omit Staff.TimeSignature
  %\omit Staff.BarLine
  \override Rest.staff-position = 0
  %\override BreathingSign.
  \set Staff.midiInstrument = "church organ"
}
}

