\version "2.25.22"
\language deutsch
\include "stylesheets/joram/choir.ily"

\header {
  title = "Puer natus in Bethlehem"
  composer = "Barth. Gesius"
}

global = {
  \time 2/2
  \set Timing.measureLength = \musicLength 1.
  \override Staff.TimeSignature.style = #'single-number
  \key g \dorian
  \partial 4
}

sopran = \relative c'' {
  \set Staff.instrumentName = "S"
  g | g2 g4 a2 a4 | b2 g4 f2 a4 | b2( c4 b2) a4 | b2. ~ 2 \breathe
  b4 | b2 b4 a2 g4 | g2 fis4 g2 \breathe
  g4 | fis2 g4 a2 a4 | b2( a4 g2) fis4 | g2. ~ 2 \bar "|."
}

alt = \relative c' {
  \set Staff.instrumentName = "A"
  d4 | d2 d4 f2 f4 | f2 e4 d2 f4 | g2( a4 f2) f4 | d2. ~ 2 \breathe
  f4 | f2 f4 f2 d4 | es4 d2 d \breathe
  d4 | d2 e4 f2 f4 | f2. d2. | d2. ~ 2
}

tenor = \relative c' {
  \set Staff.instrumentName = "T"
  \clef "treble_8"
  b4 | b2 g4 c2 c4 | d2 c4 a2 d4 | d2( f4 d) c2 | b2. ~ 2 \breathe
  d4 | d2 d4 c2 h4 | c4 a2 b2 \breathe
  b4 | a2 c4 c2 d4 | d2( c4 b) a2 | g2. ~ 2
}

bass = \relative c' {
  \set Staff.instrumentName = "B"
  \clef "bass"
  g4 | g2 g4 f2 f4 | b,2 c4 d2 d4 | g2( f4 b) f2 | b,2. ~ 2 \breathe
  b'4 | b2 b4 f2 g4 | c,4 d2 g,4.( a8) \breathe
  b8( c) | d2 c4 f4.( e8) d[( c]) | b2( f'4 g) d2 | g,2. ~ 2
}

text = \lyricmode {
  Pu -- er na -- tus in Beth -- le -- hem, in Beth -- le -- hem, __
  un -- de -- gau -- det Je -- ru -- sa -- lem.
  Al -- le -- lu -- ia, al -- le -- lu -- ia! __
}

\score {
  \new ChoirStaff <<
    \new Staff \new Voice = "s" { \global \sopran }
    \new Lyrics \lyricsto "s" { \text }
    \new Staff \new Voice = "a" { \global \alt }
    \new Lyrics \lyricsto "a" { \text }
    \new Staff \new Voice = "t" { \global \tenor }
    \new Lyrics \lyricsto "t" { \text }
    \new Staff \new Voice = "b" { \global \bass }
    \new Lyrics \lyricsto "b" { \text }
  >>
}

\layout {
  \accidentalStyle modern-voice-cautionary
}

\markup \column {
  \line { \bold "2. " Hic iacet in praesepio, (in) praesepio, qui regnat sine termino. }
  \line { \bold "3. " Magi de Saba veniunt, (Magi) veniunt, aurum, thus, myrrham offerunt. }
  \line { \bold "4. " In hoc natali gaudio, (natali) gaudio, benedicamus Domino. }
  \line { \bold "5. " Laudetur sancta Trinitas, (sancta) Trinitas, Deo dicamus gratias. }
}
