\version "2.25.22"
\language deutsch

\header {
  title = "Come again! Sweet Love"
  composer = "John Dowland, 1562 – 1626"
  shortcomposer = "Dowland"
  tagline = \markup \with-url "http://lilypond.org/"
    #(format "LilyPond ~:@{~a.~a~} – www.lilypond.org" (ly:version))
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2014 Joram Berger, joramberger.de"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "MorleyT"
  mutopiapoet = "Morley"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "Choir (SATB)"
  license = "cc-by-sa"
  date = "1734"
  lastupdated = "2014-10-17"
  %footer = "Mutopia-2014/01/00-000"
}

#(set-global-staff-size 19)

\paper {
  evenHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  oddHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  top-margin = 15\mm
  bottom-margin = 15\mm
  left-margin = 15\mm
  right-margin = 15\mm
  top-system-spacing.basic-distance = 10
  last-bottom-spacing.basic-distance = 10
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
}

%\include "stylesheets/fonts/font-register.ily"

sffz = #(make-dynamic-script "sffz")
pf = #(make-dynamic-script "pf")

\layout {
  \override Staff.Clef.font-family = #'profondo
}

%\include "stylesheets/fonts/profondo.ily"

%\include "stylesheets/fonts/profondo.ily"

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L bold italic"
  \override Staff.ClefModifier.extra-offset = #'(0 . 0.04)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Staff.NoteHead.style = #'baroque
  \autoBeamOff
}

brk = { }

soprano = \relative c'' {
  \set Staff.instrumentName = "Sopran"
  \key g \major
  \time 2/2
  r2 h4. c8 | d1 | r2 d | e d | c2. c4 | h1 |
  r2 d | d c | h h | a1 \break | r2 a | h g | a2. a4 | a1 |
  \repeat volta 2 {
    r4 d, g2 | r4 e a2 | r4 fis h2 |r4 g c2 | r4 a d2 ~ | d1 ~ | d |
    r4 d c h | a2 r4 h | a g g2~ | g4 fis8[( e] fis2) | g1
  }
}

alto = \relative c'' {
  \set Staff.instrumentName = "Alt"
  \key g \major
  g2. g4 | g1 | r2 h a4 g g2 ~ | g fis | g1 |
  r2 g | g e4.( fis8) | g2. g4 | fis1 | r2 fis | g2. d4 | e2. e4 | fis1 |
  \repeat volta 2 {
    d1 | e2 r4 e | fis2 r4 fis | g2 r4 g | a2 r4 a | h1 ~ | h2 a |
    g4. fis8 e4 g | fis2. g4 | e2 h4( c) | d2. c4 | h1
  }
}

tenor = \relative c' {
  \set Staff.instrumentName = "Tenor"
  \clef "treble_8"
  \key g \major
  d2. d4 | h1 | r2 h | c d | e2. d8[ c] | d1 |
  r2 h | h a | g d' | d1 | r2 d | d2. d4 | d2 cis | d1 |
  \repeat volta 2 {
    g,1 | g2 r4 c | a2 r4 d | h2 r4 e | d2 r4 c | h g g a | h2 c |
    d2 r4 g, | d'2. d4 | c h h a8[ g] | a2. a4 | g1
  }
}

bass = \relative c' {
  \set Staff.instrumentName = "Bass"
  \hide BarLine
  \clef bass
  \key g \major
  g2 g | g1 | r2 g, | c h | a2. a4 | g1 |
  r2 g | g a | h g | d'1 | r2 d | g, h | a2. a4 | d1 |
  \repeat volta 2 {
    h1 | c2 r4 c | d2 r4 d | e2 r4 e | fis2 r4 fis | g2 g, | g a |
    h2 c | d h | c4( d) e2 | d2. d4 | g,1
  }
}

textA = \lyricmode {
  Come a -- gain! Sweet love doth now in -- vite
  thy gra -- ces that re -- frain to do me due de -- light,
  to see, to hear, to touch, to kiss, to
  \tag "S" { die __ }
  \tag "A" { die, __ to die }
  \tag "T" { die, to die with thee a -- gain }
  \tag "B" { die, to die }
  with thee a -- gain in \tag "S" \tag "T" \tag "B" { swee -- test } \tag "A" { sweet -- test __ } sym -- pa -- thy.
}

textB = \lyricmode {
  Come a -- gain! That I may cease to mourn
  Through thy un -- kind dis -- dain;
  For now left and for -- lorn
  I sit, I sigh, I weep, I faint, I
  \tag "S" { die __ }
  \tag "A" { die, __ I die }
  \tag "T" { die, I die in dead -- ly pain }
  \tag "B" { die, to die }
  In dead -- ly pain and \tag "S" \tag "T" \tag "B" { end -- less } \tag "A" { end -- less __ } mi -- se -- ry.
}

textC = \lyricmode {
  Gent -- le Love, draw forth thy wound -- ing dart,
  Thou canst not pierce her heart;
  For I, that do ap -- prove
  By sighs and tears more hot than are thy
  \tag "S" { shafts __ did }
  \tag "A" { shafts, __ did tempt, did }
  \tag "T" { shafts, did tempt, did }
  \tag "B" { shafts, did tempt while }
  tempt while she, while she
  \tag "S" \tag "B" for \tag "A" for __  \tag "T" { for tri -- umph, while she }
  tri -- umph laughs.
}

\score {
  \new ChoirStaff <<
    \new Staff \new Voice = "sop" \soprano
    \new Lyrics \keepWithTag "S" \lyricsto sop \textA
    \new Lyrics \keepWithTag "S" \lyricsto sop \textB
    \new Lyrics \keepWithTag "S" \lyricsto sop \textC
    \new Staff \new Voice = "alt" \alto
    \new Lyrics \keepWithTag "A" \lyricsto alt \textA
    \new Lyrics \keepWithTag "A" \lyricsto alt \textB
    \new Lyrics \keepWithTag "A" \lyricsto alt \textC
    \new Staff \new Voice = ten \tenor
    \new Lyrics \keepWithTag "T" \lyricsto ten \textA
    \new Lyrics \keepWithTag "T" \lyricsto ten \textB
    \new Lyrics \keepWithTag "T" \lyricsto ten \textC
    \new Staff \new Voice = bas \bass
    \new Lyrics \keepWithTag "B" \lyricsto bas \textA
    \new Lyrics \keepWithTag "B" \lyricsto bas \textB
    \new Lyrics \keepWithTag "B" \lyricsto bas \textC
  >>
  \layout {}
  \midi { \tempo 2 = 80}
}

