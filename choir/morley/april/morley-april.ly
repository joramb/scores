\version "2.25.22"
\language deutsch

\header {
  title = "April is in my mistress’ face"
  composer = "Thomas Morley, 1558 – 1603"
  shortcomposer = "Morley"
  tagline = \markup \with-url "http://lilypond.org/"
  #(format "LilyPond ~:@{~a.~a~} – www.lilypond.org" (ly:version))
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2014 Joram Berger, joramberger.de"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "MorleyT"
  mutopiapoet = "Morley"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "Choir (SATB)"
  license = "cc-by-sa"
  date = "1734"
  lastupdated = "2014-10-17"
  %footer = "Mutopia-2014/01/00-000"
}

#(set-global-staff-size 19)

\paper {
  evenHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  oddHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  top-margin = 15\mm
  bottom-margin = 15\mm
  left-margin = 15\mm
  right-margin = 15\mm
  top-system-spacing.basic-distance = 10
  last-bottom-spacing.basic-distance = 10
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
}

%\include "stylesheets/fonts/font-register.ily"

sffz = #(make-dynamic-script "sffz")
pf = #(make-dynamic-script "pf")

\layout {
  \override Staff.Clef.font-family = #'profondo
}

%\include "stylesheets/fonts/profondo.ily"

%\include "stylesheets/fonts/profondo.ily"

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L bold italic"
  \override Staff.ClefModifier.extra-offset = #'(0 . 0.04)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Staff.NoteHead.style = #'baroque
  \autoBeamOff
}

brk = { }

soprano = \relative c'' {
  \set Staff.instrumentName = "Sopran"
  \key g \dorian
  \time 2/2
  d4 c8 b c4 d | b c a2 | r d4 d8 d | d4 d es c | \brk
  d2 a | b g4( b) | a2 d4 c8 b | c4 a b c | a2 r4 a | \brk
  b8 a b c d4 b( a2) g | r4 b d8 c d e | f2 d4 es ~ | \brk
  es8 d[( c b] c2) | b r4 d | c d es2 | d r4 d | b d c2 | \brk
  b2 d | c d2.( c4 b a | b1) | a4 d d d | \brk
  f1 | r4 c c c | es2 es | b4 g b c | d1 ~ | \brk
  d1 | h | r4 d d d | f1 | r4 c c c | \brk
  es2 es | b4 g b c | d\breve | h1
  \override Staff.BarLine.transparent = ##f
  \bar "|."
}

alto = \relative c'' {
  \set Staff.instrumentName = "Alt"
  \key g \dorian
  b4 a8 g a4 d, | g a8[( g fis e] fis4) | g2 r | d4 d8 d g4 g |
  g2 fis | g r | r b4 a8 g | a4 fis g g | fis2 r4 f |
  g8 f g a b4 g( | fis2) g | r4 f b b | a8 b[( c a]) b4 g |
  f2. f4 | f2 r4 b | a b2 a4 | b2 f | g4 f f2 |
  f2 b  | a \once\slurDown b2.( a4 g fis | g1) | fis |
  R | r4 a a a | c2 g | g g | f4 d g2( ~ |
  g4 fis8[ e] fis2) | g4 g g g | b2 b | a1 | r4 a a a |
  c2 g | g g | f4 d g2.( fis8[ e] fis2) | g1
  \override Staff.BarLine.transparent = ##f
}

tenor = \relative c' {
  \set Staff.instrumentName = "Tenor"
  \clef "treble_8"
  \key g \dorian
  R1 | R | r2 b4 a8 g | a4 b c g( |
  a1) | g2 d'4 c8 b | c4 d b b | a d d es | d1 |
  R1 | r2 r4 b | d8 c d e f4 b, | c2 b4 \tieDown c( ~ |
  c8[ b]) b2( a4) | b2 r4 b | f' d c2 | b b | g4 b2 a4 |
  b2 f'1 f2 | d\breve | d2 r4 d |
  d4 d f1 f2 | c1 | r2 g | b b |
  a1 | g2 r4 g | g g d'2 | d f1 f2 |
  c1 | r2 g | b b | a1 | g
  \override Staff.BarLine.transparent = ##f
}

bass = \relative c' {
  \set Staff.instrumentName = "Bass"
  \hide BarLine
  \clef bass
  \key g \dorian
  R1 | R | r2 g4 fis8 e | fis4 g c, es |
  d1 | r2 b'4 a8 g | a4 fis g g | c, d g c, | d1 |
  R1 | r4 d es8 f g a | b2 b | f g4 es |
  f1 b, R r2 b | es4 b f'2 |
  b,1 | f'2 d | g\breve | d1 |
  r4 d d d | f1 | r 4 c c c | es2. es4 | b2. c4 |
  d1 | g,\breve | r4 d' d d | f1 |
  r4 c c c | es2. es4 | b2. c4 | d1 | g,
  \override Staff.BarLine.transparent = ##f
}

text = \lyricmode {
  A -- pril is in my mis -- \tag "S" \tag "B" tress’ \tag "A" \tag "T" tress’ __ face,
  A -- pril is in my mis -- tress’ face, \tag "S" \tag "T" \tag "B" { my mis -- tress face, }
  \tag "S" \tag "A" { A -- pril is in my mis -- tress’ face }

  and Ju -- ly in her eyes \tag "S" \tag "A" hath __ \tag "T" \tag "B" hath place,
  \tag "S" \tag "A" { and Ju -- ly in her eyes, } her eyes \tag "A" \tag "B" hath \tag "S" \tag "T" hath __ place,

  \tag "S" \tag "A" \tag "T" { with -- in her bo -- som, }
  with -- in her bo -- som is Sep -- tem -- ber,

  but in her heart, \tag "S" \tag "B" { but in her heart, } \tag "S" \tag "A" \tag "T" { her heart } a cold De -- cem -- ber,
  but in her heart, \tag "A" { her heart, } but in \tag "S" \tag "A" { her heart, } her heart
  a cold De -- cem -- ber.
}

\score {
  \new StaffGroup <<
    \new Staff \new Voice = "sop" \soprano
    \new Lyrics \keepWithTag "S" \lyricsto sop \text
    \new Staff \new Voice = "alt" \alto
    \new Lyrics \keepWithTag "A"
    \lyricsto alt \text
    \new Staff \new Voice = ten \tenor
    \new Lyrics \keepWithTag "T"
    \lyricsto ten \text
    \new Staff \new Voice = bas \bass
    \new Lyrics \keepWithTag "B" \lyricsto bas \text
  >>
  \layout {
    \override Staff.BarLine.transparent = ##t
  }
  \midi { \tempo 2 = 70 }
}

