\version "2.25.22"

#(set-global-staff-size 15.8)

italicas = \override LyricText.font-shape = #'italic
rectas = \override LyricText.font-shape = #'upright
incipitwidth = 18

\header {
  title = "Ne timeas Maria"
  subtitle = "in Annuntiatione Beatæ Mariæ"
  shortcomposer = "Victoria"
  composer = \markup \right-column {
    "Tomás Luis de Victoria" "(1548–1611)"
  }
  poet = "1572, 1583b, 1589a, 1589b, 1603"
  tagline = \markup \with-url "http://lilypond.org/"
  \line {
    "Lilypond"
    #(string-join
      (map (lambda (x) (if (symbol? x)
                           (symbol->string x)
                           (number->string x)))
        (list-head (ly:version) 2))
      ".")
    "– www.lilypond.org"
  }
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2013 Joram Berger, joramberger.de"
    \with-url "http://tomasluisdevictoria.org"
    "(Original: Nancho Alvarez)"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
}


\paper {
  top-system-spacing.basic-distance = #2
  last-bottom-spacing.basic-distance = #10
  top-markup-spacing.basic-distance = #5
  system-count = 8
  ragged-last-bottom = ##f
  indent = 3.3\cm
  foot-separation = 8\mm
  head-separation = 4\mm
  bottom-margin = 7\mm
  top-margin = 7\mm
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
  evenHeaderMarkup = \markup \fill-line {
    \fromproperty #'page:page-number-string
    \fromproperty #'header:title
    \fromproperty #'header:shortcomposer
  }
  oddHeaderMarkup = \markup \fill-line {
    \unless \on-first-page \fromproperty #'header:shortcomposer
    \unless \on-first-page \fromproperty #'header:title
    \unless \on-first-page \fromproperty #'page:page-number-string
  }
}

\layout {
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Score.BarNumber.padding = #1.6
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L italic"
  \override Staff.ClefModifier.extra-offset = #'(-0.3 . 0.07)
  \override Staff.LigatureBracket.padding = #1
  \override Lyrics.VerticalAxisGroup.minimum-Y-extent = #'(-0.0 . 0.0)
  \override Lyrics.LyricHyphen.minimum-distance = #0.33
}

global = {
  \key g \major
  \time 2/2
}

soprano = {
  d'1 |
  g'2. e'4 |
  e'2 a' |
  fis'4. g'8 a'4 g' ~ |
  %5
  g' fis' g'4. fis'16 e' |
  d'2 e' ~ |
  e'1 |
  r2 r4 g' |
  c''2. b'4 |
  %10
  b'1 |
  r4 d' g'2 ~ |
  g'4 e' e'2 |
  r4 d' e' g' ~ |
  g' fis' g'2 ~ |
  %15
  g' r |
  R1*4/4 |
  b'4. b'8 b'4 g' |
  a' b' c''4. c''8 |
  b'2 r |
  %20
  r r4 g' ~ |
  g'8\noBeam g' g'4 e' fis' |
  g'2 a'4. g'8 |
  a' b' c''4. b'8  b'[ a'16 g'] |
  a'4 a' b'2 ~ |
  %25
  b' r4 b' ~ |
  b' gis' a'2 ~ |
  a'4 a' fis'2 |
  fis' g'4 e' ~ |
  e' d' \[e'2 |
  %30
  d'2.\] d'4 |
  d'2 r |
  g' e'4 e' |
  fis'4. fis'8 g'2 |
  r4 g' a' b' |
  %35
  c''2. b'4 |
  b'2 g' ~ |
  g' a' ~ |
  a'4 a' b'2 ~ |
  b' r |
  %40
  r r4 a' ~ |
  a' d''4. c''8 b' a' |
  b'4 c''4. b'8 a'4 ~ |
  a'8 g' g'2 fis'4 |
  g'1 |
  %45
  r2 r4 d' |
  g'4. e'8 fis'4 a' ~ |
  a'8 g' a'4 b' c'' ~ |
  c''8 b' b'2 a'4 |
  b'2 r |
  %50
  R1*4/4 |
  R1*4/4 |
  b'1 |
  a'2 c'' |
  b'4 a'8 g' \[a'2 |
  %55
  b'2.\] b'4 |
  a'2 r4 g' |
  d''2 c''4 a' |
  b'2. a'4 |
  g' e' fis'2 |
  %60
  r4 g' c''2 |
  b'4 g' b' a' |
  g'2 r4 d' |
  g'2 fis'4 d' |
  e' d'4. b8 b'4 |
  %65
  a' g' fis' g' ~ |
  g' fis'8 e' fis'4 fis' |
  g'1 ~ |
  g' ~ |
  g' ~ |
  %70
  g'\breve*1/2
  \bar "|."
}

alto = {
  r2 g ~ |
  g c' ~ |
  c'4 a a2 |
  d' c'4 b |
  %5
  a2 b ~ |
  b r4 g |
  c'2. a4 |
  a2 r |
  r4 a d'2 ~ |
  %10
  d'4 b b g |
  b4.\melisma c'8 d'4 e'\melismaEnd |
  b2 r4 e' ~ |
  e'8 d' d'2 cis'4 |
  d'2 b4 d' ~ |
  %15
  d'8\noBeam d' d'4 b c' |
  d' e'4.\melisma d'8 e' fis' |
  g'4. fis'16 e' d'4 e' ~ |
  e' d' c'\melismaEnd a |
  b2 b4. b8 |
  %20
  b4 g a b |
  c'2. a4 |
  g g'2\melisma fis'8 e' |
  fis'4  e'8[ d' e' fis'] g'4 ~ |
  g'\melismaEnd fis' g'2 |
  %25
  r4 g'2 g'4 |
  fis' e'8 d' e'2 ~ |
  e'4 e' d'2 ~ |
  d' r4 g |
  a2 b4 c' |
  %30
  a2. a4 |
  b2 r |
  e' cis'4 cis' |
  d'4. d'8 b4 d' |
  g'4.\melisma fis'8 e'4 d' |
  %35
  c'\melismaEnd a d'2 |
  r4 d' e'4.\melisma d'8 |
  e' fis' g' e' fis'4 e'8 d' |
  e'4\melismaEnd fis' g'2 |
  d'1 |
  %40
  e'2. e'4 |
  fis'2 r4 d' |
  g'8\melisma fis' e' d' c'4 d' |
  e'\melismaEnd cis' d'2 ~ |
  d' r4 g |
  %45
  d'4. d'8 e'4 d' ~ |
  d' cis' d' fis' |
  fis'4. e'8 fis'4 g' ~ |
  g'8[\melisma fis']  fis'[ e'16 d'] e'4\melismaEnd fis' |
  g' g d'4. d'8 |
  %50
  e'4 d'2 cis'4 |
  d'2 r |
  d'1 |
  d'2 \[e' |
  d'2.\] d'4 |
  %55
  d'1 |
  r4 d' g'2 |
  fis'4 d' e'\melisma d' ~ |
  d'8 c' b a g4 fis |
  g a4. g8 fis e |
  %60
  fis4 g2\melismaEnd fis4 |
  g d d'2 |
  b4 g a\melisma b ~ |
  b8 a b g a4 g ~ |
  g \melismaEnd fis g d' |
  %65
  e'2 d'4 cis' |
  d'2. d'4 |
  b2 r4 d' |
  g' e' d'2 |
  e'2. e'4 |
  %70
  d'\breve*1/2
}

tenor = {
  R1*4 |
  %5
  d1 |
  g2. e4 |
  e2 a |
  fis4. g8 a4 g ~ |
  g fis g2 |
  %10
  r4 g d'2 ~ |
  d'4 b b2 |
  e'4.  d'8[ c' b] c'4 |
  b a \[g2 |
  a\] g |
  %15
  b4. b8 b4 g |
  a b c'8\melisma b c' d' |
  e'4  d'8[ c' b a] b4 |
  a g2 \melismaEnd fis4 |
  g d'4. d'8 d'4 |
  %20
  b c' d'2 |
  e'2. d'8 c' |
  b2 r4 d' ~ |
  d' c'8 b a4 g |
  d'2 r4 d' ~ |
  %25
  d' b2 e'4 ~ |
  e'8 \melisma d' d'2 cis'8 b |
  cis'4 \melismaEnd cis' d' a ~ |
  a b2 c'4 ~ |
  c'8 \melisma b a4. g8 g4 ~ |
  %30
  g fis8 e fis4 \melismaEnd fis |
  g d'2 b4 ~ |
  b e'2 a4 ~ |
  a d' d'8 \melisma c' b a |
  g fis e4. fis8 g4 ~ |
  %35
  g \melismaEnd fis g2 ~ |
  g4 g c'4. \melisma b8 |
  c' d' e'4 d' cis'8 b |
  cis'4 \melismaEnd d' g2 |
  r4 g b8 \melisma c' d' b |
  %40
  c'4 b8 a b4 \melismaEnd cis' |
  d' a \[b2 \melisma |
  g\] a |
  b4 a8 g a4 \melismaEnd a |
  g b b4. g8 |
  %45
  a4 b4. \melisma a8  a[ g16 fis] |
  g4 \melismaEnd g a d |
  d'4. c'8 d'4 e' ~ |
  e'8[ \melisma d']  d'[ c'16 b] c'4 d' ~ |
  d'8 c' b4 a b ~ |
  %50
  b8[ a]  a[ g16 fis] g4 \melismaEnd g |
  a2 r |
  g1 |
  fis2 g4 a ~ |
  a8 g g2 fis4 |
  %55
  g2 r4 g |
  d'2 b4 g |
  a \melisma g2 fis4 |
  g4.  a8[ b c'] d'4 ~ |
  d' \melismaEnd cis' d' a |
  %60
  d'2 c'4 a |
  b4. \melisma a8 g4 fis |
  e \melismaEnd e d2 |
  r4 g d'2 |
  c'4 a \[b2 \melisma |
  %65
  c'4.\] b8 a4 \melismaEnd g |
  a2 r4 a |
  d'2 b4 g |
  b \melisma c'2 b4 |
  c'8 b c' d' e'4 c' ~ |
  %70
  c' \melismaEnd b8 a b\breve*1/4
}

bass = {
  R1*4 |
  %5
  r2 g, ~ |
  g, c ~ |
  c4 a, a,2 |
  d c4 b, |
  a,2 g, ~ |
  %10
  g, r4 g, |
  g2. e4 |
  e2 a |
  g4 fis \[e2 |
  d\] g, |
  %15
  g4. g8 g4 e |
  fis g a4. \melisma g16 fis |
  e8 fis g4. fis8 e d |
  c4 b, a, \melismaEnd a, |
  g,2 g4. g8 |
  %20
  g4 e fis g |
  c4. \melisma b,8 c4 d |
  e \melismaEnd e d2 |
  R1*4/4 |
  r2 g ~ |
  %25
  g e |
  \[b a-\shape #'((1 . 0) (1 . 0) (0 . 0) (0 . 0)) ~ \] |
  a4 a d2 |
  d e |
  f \melisma e4 c |
  %30
  d2. \melismaEnd d4 |
  g,2 g |
  e a |
  d4 d g4. \melisma fis8 |
  e d c2 b,4 |
  %35
  a, \melismaEnd a, g,2 ~ |
  g, r |
  R1*4/4 |
  r2 r4 g, |
  g4. \melisma fis8[ g a] b4 |
  %40
  a gis8 fis gis4 \melismaEnd a |
  d d g4. \melisma fis8 |
  e4 c f2 \melismaEnd |
  e d |
  r4 g, g4. e8 |
  %45
  fis4 g4. \melisma fis8  fis[ e16 d] |
  e4 \melismaEnd e d2 |
  R1*4/4 |
  r2 r4 d |
  g4. e8 fis4 g ~ |
  %50
  g8[ \melisma fis]  fis[ e16 d] e4 \melismaEnd e |
  d2 r |
  g,1 |
  d2 \[c |
  d2.\] d4 |
  %55
  g, g, g2 |
  fis4 d e2  \melisma |
  d4 b, c \melismaEnd d |
  g,2 r |
  r4 a, d2 |
  %60
  b,4 g, a,4. a,8 |
  g,2 r4 d |
  g2 fis4 d |
  e2 \melisma d4 b, |
  c \melismaEnd d g, g, |
  %65
  c2 d4 e |
  d2. d4 |
  g, g, g2 |
  e4 c \[g2 |
  c2.\] c4 |
  %70
  g,\breve*1/2
}

textsoprano = \lyricmode{
  Ne ti -- me -- as Ma -- ri -- _ _ _ _ _ _ _ _ a __
  ne ti -- me -- as
  ne ti -- me -- as
  Ma -- ri -- _ _ a __
  in -- ve -- ni -- sti e -- nim gra -- ti -- am
  in -- ve -- ni -- sti e -- nim gra -- _ _ _ _ _ _ _ _ _ ti -- am __
  a -- pud Do -- mi -- num,
  a -- pud Do -- _ _ _ mi -- num:
  ec -- ce con -- ci -- pi -- es
  con -- ci -- _ _ pi -- es in __ u -- te -- ro __
  in __ u -- _ _ _ _ _ _ _ _ _ te -- ro
  et pa -- ri -- es fi -- _ _ _ _ _ _ li -- um
  et vo -- ca -- _ _ _ _ _ bi -- tur
  al -- tis -- si -- mi
  fi -- _ _ li -- us
  al -- tis -- si -- mi fi -- li -- us
  al -- tis -- si -- mi
  fi -- _ _ _ _ _ _ _ _ _ _ li -- us. __
}

textalto = \lyricmode{
  Ne __ ti -- me -- as Ma -- ri -- _ _ a __
  ne ti -- me -- as
  ne ti -- me -- as Ma -- ri -- a
  Ma -- _ _ _ ri -- a,
  in -- ve -- ni -- sti e -- nim gra -- ti -- am,
  in -- ve -- ni -- sti e -- nim gra -- ti -- am,
  gra -- ti -- am
  a -- pud Do -- _ _ _ mi -- num __
  a -- pud Do -- _ _ mi -- num:
  ec -- ce con -- ci -- pi -- es,
  con -- ci -- pi -- es
  in u -- te -- ro,
  in u -- te -- ro
  in u -- te -- ro __
  et pa -- ri -- es fi -- li -- um,
  et pa -- ri -- es fi -- li -- um,
  et pa -- ri -- es fi -- li -- um
  et vo -- ca -- _ bi -- tur
  al -- tis -- si -- mi fi -- li -- us,
  al -- tis -- si -- mi fi -- li -- us,
  al -- tis -- si -- mi
  fi -- li -- us
  al -- tis -- si -- mi fi -- li -- us.
}

texttenor = \lyricmode{
  Ne ti -- me -- as Ma -- ri -- _ _ _ _ a
  ne ti -- me -- as Ma -- _ _ _ _ ri -- _ _ _ a,
  in -- ve -- ni -- sti e -- nim gra -- ti -- am,
  in -- ve -- ni -- sti e -- nim
  gra -- \italicas ti -- _ am \rectas %? tiam __ _  en 1572 ?
  gra -- _ _ _ ti -- am
  a -- pud Do -- mi -- num,
  a -- pud Do -- mi -- num:
  ec -- ce, ec -- ce con -- ci -- pi -- es __
  in u -- te -- ro
  in u -- te -- ro,
  in u -- te -- ro,
  et pa -- ri -- es fi -- li -- um,
  et pa -- ri -- es fi -- li -- um
  et vo -- ca -- _ _ _ bi -- tur
  al -- tis -- si -- mi
  fi -- li -- us,
  al -- tis -- si -- mi fi -- li -- us
  al -- tis -- si -- mi fi -- li -- us
  al -- tis -- si -- mi fi -- li -- _ us.
}

textbass = \lyricmode{
  Ne __ ti -- me -- as Ma -- ri -- _ _ a __
  ne ti -- me -- as Ma -- ri -- _ _ _ a,
  in -- ve -- ni -- sti e -- nim gra -- ti -- am,
  in -- ve -- ni -- sti e -- nim gra -- ti -- am
  a -- pud Do -- _ mi -- num,
  a -- pud Do -- mi -- num:
  ec -- ce,
  ec -- ce con -- ci -- pi -- es __
  in u -- te -- ro,
  in u -- te -- ro
  et pa -- ri -- es fi -- li -- um
  et pa -- ri -- es fi -- li -- um
  et vo -- ca -- _ bi -- tur al -- tis -- si -- mi fi -- li -- us
  al -- tis -- si -- mi fi -- li -- us
  al -- tis -- si -- mi fi -- li -- us,
  al -- tis -- si -- mi
  fi -- li -- us,
  al -- tis -- si -- mi fi -- _ li -- us.
}



incipitsoprano = \markup{
  \score{
    {
      \set Staff.instrumentName="Sopran "
      \override NoteHead.style = #'neomensural
      \override Rest.style = #'neomensural
      \override Staff.TimeSignature.style = #'neomensural
      \cadenzaOn
      \clef "petrucci-g"
      \key c \major
      \time 2/2
      g'\breve
      \cadenzaOff
    }
    \layout {
      \context {
        \Voice
        \remove Ligature_bracket_engraver
        \consists Mensural_ligature_engraver
      }
      line-width = \incipitwidth
      indent = 0
    }
  }
}


incipitalto = \markup{
  \score{
    {
      \set Staff.instrumentName="Alt       "
      \override NoteHead.style = #'neomensural
      \override Rest.style = #'neomensural
      \override Staff.TimeSignature.style = #'neomensural
      \cadenzaOn
      \clef "petrucci-c2"
      \key c \major
      \time 2/2
      c'\breve s4
      \cadenzaOff
    }
    \layout {
      \context {
        \Voice
        \remove Ligature_bracket_engraver
        \consists Mensural_ligature_engraver
      }
      line-width = \incipitwidth
      indent = 0
    }
  }
}


incipittenor = \markup{
  \score{
    {
      \set Staff.instrumentName="Tenor   "
      \override NoteHead.style = #'neomensural
      \override Rest.style = #'neomensural
      \override Staff.TimeSignature.style = #'neomensural
      \cadenzaOn
      \clef "petrucci-c3"
      \key c \major
      \time 2/2
      g\breve
      \cadenzaOff
    }
    \layout {
      \context {
        \Voice
        \remove Ligature_bracket_engraver
        \consists Mensural_ligature_engraver
      }
      line-width = \incipitwidth
      indent=0
    }
  }
}


incipitbass = \markup {
  \score{
    {
      \set Staff.instrumentName="Bass     "
      \override NoteHead.style = #'neomensural
      \override Rest.style = #'neomensural
      \override Staff.TimeSignature.style = #'neomensural
      \cadenzaOn
      \clef "petrucci-c4"
      \key c \major
      \time 2/2
      c\breve
      \cadenzaOff
    }
    \layout {
      \context {
        \Voice
        \remove Ligature_bracket_engraver
        \consists Mensural_ligature_engraver
      }
      line-width = \incipitwidth
      indent = 0
    }
  }
}


\score {
  \transpose g bes {
    \new ChoirStaff <<
      \new Staff \new Voice = "soprano" {
        \set Staff.instrumentName = \incipitsoprano
        \clef "treble"
        \global
        \soprano
      }
      \new Lyrics \lyricsto "soprano" \textsoprano

      \new Staff \new Voice = "alto" {
        \set Staff.instrumentName = \incipitalto
        \clef "treble"
        \global
        \alto
      }
      \new Lyrics \lyricsto "alto" \textalto

      \new Staff \new Voice = "tenor" {
        \set Staff.instrumentName = \incipittenor
        \clef "treble_8"
        \global
        \tenor
      }
      \new Lyrics \lyricsto "tenor" \texttenor

      \new Staff \new Voice = "bass" {
        \set Staff.instrumentName = \incipitbass
        \clef "bass"
        \global
        \bass
      }
      \new Lyrics \lyricsto "bass" \textbass
    >>
  } % transpose

  \layout{
    \context {
      \Staff
      \consists Ambitus_engraver
      \override InstrumentName.extra-offset = #'(0 . 0.6)
    }
  }
}

%{
  % MIDI-Dateien zum Proben:

  rehearsalMidi = #
  (define-music-function
  (name midiInstrument lyrics) (string? string? ly:music?)
  #{
  \unfoldRepeats <<
  \new Staff = "soprano" \new Voice = "soprano" { s1*0\f \cantus }
  \new Staff = "alto" \new Voice = "alto" { s1*0\f \altus }
  \new Staff = "tenor" \new Voice = "tenor" { s1*0\f \tenor }
  \new Staff = "bass" \new Voice = "bass" { s1*0\f \bassus }
  \context Staff = $name {
  \set Score.midiMinimumVolume = #0.5
  \set Score.midiMaximumVolume = #0.5
  \set Score.tempoWholesPerMinute = \musicLength 4*90
  \set Staff.midiMinimumVolume = #0.8
  \set Staff.midiMaximumVolume = #1.0
  \set Staff.midiInstrument = $midiInstrument
  }
  \new Lyrics \with {
  alignBelowContext = $name
  } \lyricsto $name $lyrics
  >>
  #})

  \book {
  \bookOutputSuffix "Sopran"
  \score {
  \rehearsalMidi "soprano" "oboe" \textocantus
  \midi { }
  }
  }

  \book {
  \bookOutputSuffix "Alt"
  \score {
  \rehearsalMidi "alto" "oboe" \textoaltus
  \midi { }
  }
  }

  \book {
  \bookOutputSuffix "Tenor"
  \score {
  \rehearsalMidi "tenor" "oboe" \textotenor
  \midi { }
  }
  }

  %\midifor #"bass" #"oboe" #\text

  \book {
  \bookOutputSuffix "Bass"
  \score {
  \rehearsalMidi "bass" "oboe" \textobassus
  \midi { }
  }
  }

%}