\version "2.25.22"
\language deutsch

\header {
  title = "Flow, my Tears"
  composer = "John Dowland, 1562 – 1626"
  %arranger = "Arr: Robin Doveton"
  shortcomposer = "Dowland"
  tagline = \markup \with-url "http://lilypond.org/"
  #(format "LilyPond ~:@{~a.~a~} – www.lilypond.org" (ly:version))
  copyright = \markup {
    \with-url "http://joramberger.de"
    "© 2014 Joram Berger, joramberger.de"
    "– Lizenz:"
    \with-url "http://creativecommons.org/licenses/by-sa/4.0/"
    \small "CC-BY-SA"
  }
  maintainer = "Joram Berger"
  maintainerWeb = "http://joramberger.de"
  style = "Baroque"
  source = "Breitkopf & Härtel, Leipzig, 1856"
  mutopiacomposer = "MorleyT"
  mutopiapoet = "Morley"
  mutopiaopus = "BWV 248"
  mutopiainstrument = "Choir (SATB)"
  license = "cc-by-sa"
  date = "1734"
  lastupdated = "2014-10-17"
  %footer = "Mutopia-2014/01/00-000"
}

#(set-global-staff-size 18)

\paper {
  evenHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  oddHeaderMarkup = \markup
  \fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    %\unless \on-first-page-of-part \fromproperty #'header:composer
    \unless \on-first-page-of-part \fromproperty #'header:title
    \if \should-print-page-number \fromproperty #'page:page-number-string
  }
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  top-margin = 13\mm
  bottom-margin = 8\mm
  left-margin = 13\mm
  right-margin = 13\mm
  top-system-spacing.basic-distance = 10
  last-bottom-spacing.basic-distance = 10
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
  %page-count = 3
  systems-per-page = 3
}

%\include "stylesheets/fonts/font-register.ily"

sffz = #(make-dynamic-script "sffz")
pf = #(make-dynamic-script "pf")

\layout {
  \override Staff.Clef.font-family = #'profondo
}

%\include "stylesheets/fonts/profondo.ily"

%\include "stylesheets/fonts/profondo.ily"

\layout {
  \override Staff.ClefModifier.font-name = #"Century Schoolbook L bold italic"
  \override Staff.ClefModifier.extra-offset = #'(0 . 0.04)
  \override Score.BarNumber.font-name = #"Century Schoolbook L"
  \override Staff.NoteHead.style = #'altdefault %#'baroque
  \override Lyrics.LyricText.font-size = #0.5
  \omit Slur
  \autoBeamOff
  \accidentalStyle voice
  \numericTimeSignature
}

brk = { }

soprano = \relative c'' {
  \tempo "Andante con moto" 2 = 52
  \set Staff.instrumentName = "Sopran"
  \key a \minor
  \time 4/2
  \repeat volta 2 {
    a2. g8[ f] e2 c' ~ | c h4 a gis1 | \break
    a2 e2. e4 g g | f2 d e2. h'4 | c2 a h gis | \break
    a4 c4. h8 a4 gis2 c | h4 a a2. gis8[( fis] gis2) | a\breve |
  }\pageBreak
  \repeat volta 2 {
    c2. h4 a g c2 ~ | c4 h8[ a] h2 c g | r4 a2 gis4 a f e2 |
    r4 d f2 r4 f a2 | r4 a c2  r4 g h4. c8 | d2 r4 d,
    f4. g8 a2 | r4 c2 h4 c4. h8 a[ gis] a4 | gis\breve |
  }\pageBreak
  \repeat volta 2 {
    gis2. a4 h2 c | h4 a a1 gis2 | a2. c4 h c a2 |
    gis1 r | r2 e'2. h4 d2 ~ | d4 a c2 h a |
    gis2 c h4 a a2 ~ | \time 2/2 a4 gis8[( fis] gis2) | \time 4/2 a\breve\fermata |
  }
}

alto = \relative c' {
  \set Staff.instrumentName = "Alt"
  \key a \minor
  \repeat volta 2 {
    c2. h4 c( d) e2( ~ | e4 d8[ c]) d4 d e1|
    c2 c2. c4 e e | d c( h a) h2 e ~ | e4 e( ~ e8[ d e fis] g4) d e e |
    e2 d4 d e2. g4 | f2 d e2. d4 | cis4. f8( e[ d cis h]) cis1 |
  }
  \repeat volta 2 {
    e2. e4 e e g2 ~ | g4 f( d) f e2 c | r r4 e c a a2 |
    r4 f a2 r4 a( c f) | e2 a,8[ h c d] e4.( f8 g2 ~ | g4.) f16[ e] d2
    r2 r4 f ~ | f e8[ d] e2 c8[ d] e2 d4 | e4. e8( d[ c]) h[( a]) h1
  }
  \repeat volta 2 {
    e2. e4 e2 e | e2. fis4 g2 e | e c8[ d e fis] g4 g, a2 |
    h1 r2 g' ~ | g4 c, e2( d2.) e4( | f2) e2. h4( c) d |
    e2.  g4 f2 d | e2. d4 | cis4. f8( e[ d] cis[ h]) cis1\fermata |
  }
}

tenor = \relative c' {
  \set Staff.instrumentName = "Tenor"
  \clef "treble_8"
  \key a \minor
  \repeat volta 2 {
    a2 e e'4( d) c( h) | a2. a4 h2 e, ~ |
    e4 e2 a4 g2 c4( h) | a1 ~ a4 a gis2 | a4 c2 c4 h2. h4 |
    a2 a4 a h2 e2 | d4 c( h a) h2 h | a2. e2 e4 a2 |
  }
  \repeat volta 2 {
    g2. g4 c2. g4 | a2 f g4 c4. h8 g4 | a c h2 a2. e4 |
    f2 r4 f a2 r4 a | c2( e4) a, g1 | r4 d f4. g8
    a1 | r4 a2 h4 e, e c' a | h2. e2 e4 e,2 |
  }
  \repeat volta 2 {
    h'2 e, gis2. a4 | h2 c h2. d4 | c4.( d8 e4) c d e2 d4 |
    e4 e2 h4 c2( h2 ~ | h4 a a1) gis2 | a1 e2 a |
    h2 g d'4 c( h a) | h2 h a a4 e2 e4 a2\fermata |
  }
}

bass = \relative c {
  \set Staff.instrumentName = "Bass"
  \hide BarLine
  \clef bass
  \key a \minor
  \repeat volta 2 {
    a2. a4 a2 a'4( g) | f2. f4 e1 |
    a,2 a4 a c2 c | d f e2. e4 | a,2. a'4 g2 e |
    f2 f4 f e2 c | d f e2. e4 | a,\breve |
  }
  \repeat volta 2 {
    c2. c4 c2 e | d d4 d c c2 e4 | c a e2 a4 d2( cis4) |
    d2 r4 d f2 r4 f | a2 r4 a, c2 r4 g | h4. c8 d2
    r4 d f4. g8 | a4 a,2 gis4 a4. g8 f4 f | e\breve |
  }
  \repeat volta 2 {
    e'2. e4 e2 e | e e e2. e4 | a,2 a' g4 e f2 |
    e1 e2. h4 c1 h | a g2 f |
    e2 c' d f | e e | a,\breve\fermata |
  }
}

textAS = \lyricmode {
  Flow, my __ tears, fall __ from your springs! %:?
  Ex -- iled for e -- ver, let me mourn,
  Where night’s black bird her sad in -- fa -- my sings,
  There let me live for -- lorn.

  Ne -- ver may my woes __ be re -- liev -- ed,
  Since pi -- ty is fled;
  And tears and sighs and groans my wea -- ry days, my wea -- ry days
  Of all joys have de -- priv -- ed.

  Hark! you sha -- dows that in dark -- ness dwell,
  Learn to con -- temn light
  Hap -- py, hap -- py they that in hell
  Feel not the world’s __ des -- pite.
}

textBS = \lyricmode {
  Down, vain __ lights, shine __ you no more!
  No nights are dark e -- nough for those
  That in des -- pair their last for -- tunes de -- plore.
  Light doth but shame dis -- close.

  From the high -- est spire __ of con -- tent -- ment
  My for -- tune is thrown;
  And fear, and grief, and pain, for my de -- serts, for my de -- serts
  Are my hopes, since hope __ is gone.
}

textAA = \lyricmode {
  Flow, my tears, __ fall __ from your springs! %:?
  Ex -- iled for e -- ver, let me __ mourn,
  Where __ night’s __  black bird her in -- fa -- my sings,
  There let me live for -- lorn, for -- lorn.

  Ne -- ver may my woes __ be __ re -- liev -- ed,
  Since pi -- ty’s fled;
  And tears and __ sighs my __ wea -- ry days,
  Of all __ joys have __ de -- priv -- ed, de -- priv -- ed.

  Hark! you sha -- dows that in dark -- ness dwell,
  Learn __ to con -- temn light
  Hap -- py, hap -- py __ they that __ in hell
  Feel not the world’s de -- spite, de -- spite.
}

textBA = \lyricmode {
  Down, vain lights, __ shine __ you no more!
  No nights are dark e -- nough for __ those
  That __ in __ des -- pair their for -- tunes de -- plore.
  Light doth but shame dis -- close, dis -- close.

  From the high -- est spire __ of __ con -- tent -- ment
  My for -- tune’s thrown;
  And fear and __ grief for __ my __ de -- serts,
  Are my __ hopes, since __ hope is gone, is __ _ gone.
}

textAT = \lyricmode {
  Flow, my tears, __ fall __ from your springs! %:?
  Ex -- iled for e -- ver, let __ me mourn,
  Where night’s black bird her in -- fa -- my sings,
  There let me __ live for -- lorn, live for -- lorn.

  Ne -- ver may my woes be re -- liev -- ed,
  Since pi -- ty is fled, is fled,
  And tears and sighs, __ and groans my wea -- ry days,
  Of all joys have de -- priv -- ed, de -- priv -- ed.

  Hark! you sha -- dows that in dark -- ness dwell, __
  Learn to con -- temn light
  Hap -- py, hap -- py they that in hell
  Feel not the __ world’s des -- pite, the world’s des -- pite.
}

textBT = \lyricmode {
  Down, vain lights, __ shine __ you no more!
  No __ nights are dark e -- nough __ for those
  That in des -- pair their for -- tunes de -- plore.
  Light doth but __ shame dis -- close, shame dis -- close.

  From the high -- est spire of con -- tent -- ment
  My for -- tune is thrown, is thrown,
  And fear and grief __ and pain for my de -- serts,
  Are my hopes, since hope is gone, hope is gone.
}

textAB = \lyricmode {
  Flow, my tears, fall __ from your springs! %:?
  Ex -- iled for e -- ver, let me mourn,
  Where night’s black bird her in -- fa -- my sings,
  There let me live for -- lorn.

  Ne -- ver may my woes be re -- liev -- ed,
  Since pi -- ty is fled, is __ fled,
  And tears and sighs and groans my wea -- ry days, my wea -- ry days
  Of all joys have de -- priv -- ed.

  Hark! you sha -- dows that in dark -- ness dwell,
  Learn to con -- temn light
  Hap -- py, hap -- py they that in hell
  Feel not the world’s des -- pite.
}

textBB = \lyricmode {
  Down, vain lights, shine __ you no more!
  No nights are dark e -- nough for __ those
  That in des -- pair their for -- tunes de -- plore.
  Light doth but shame dis -- close.

  From the high -- est spire of con -- tent -- ment
  My for -- tune is thrown, is __ thrown,
  And fear and grief and pain for my de -- serts, for my de -- serts
  Are my hopes, since hope is gone.
}

music = \transpose c c
\new ChoirStaff <<
  \new Staff \new Voice = "sop" \soprano
  \new Lyrics \lyricsto sop \textAS
  \new Lyrics \lyricsto sop \textBS
  \new Staff \new Voice = "alt" \alto
  \new Lyrics \lyricsto alt \textAA
  \new Lyrics \lyricsto alt \textBA
  \new Staff \new Voice = ten \tenor
  \new Lyrics \lyricsto ten \textAT
  \new Lyrics \lyricsto ten \textBT
  \new Staff \new Voice = bas \bass
  \new Lyrics \lyricsto bas \textAB
  \new Lyrics \lyricsto bas \textBB
>>

\score {
  \music
}

\score {
  \music
  \midi {}
}
