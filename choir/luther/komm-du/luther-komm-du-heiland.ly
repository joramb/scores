\version "2.25.22"
\language deutsch

\include "stylesheets/joram/choir.ily"

\header {
  title = "Komm, du Heiland aller Welt"
  composer = "Martin Luther (1483 – 1544)"
  opus = "Markus Jenny (1924 – 2001)"
}

sopran = \relative c'' {
  \key g \dorian
  g4 g f b | a8( g) a4 g2 | g4 b c4. b8 | c4 d b2 |
  b4 c d b | c b8( a) g2 | g4 g f b | a8( g) a4 g='2 |
  \bar "|."
}

alt = \relative c' {
  \key g \dorian
  d4 d d f | f8 d f4 d2 | d4 g g4. g8 | a4 a g2 |
  d4 f f g | g g8 fis g2 | d4 es d f | f8 d f4 d='2 |
}

tenor = \relative c' {
  \key g \dorian
  b4 b a d | c8 b a4 h2 | b4 d es4. d8 | f4 f d2 |
  g,4 a b b | es d d2 | h4 c a d c8 b a4 h=2 |
}

bass = \relative c {
  \key g \dorian
  g4 g d' b | f'8 g d4 g,2 | g'4 g c,4. g'8 | f4 d g2 |
  g4 f b, es | c d g,2 | g'4 c, d b | f'8 g d4 g,=,2 |
}

textA = \lyricmode {
  \set stanza = "1."
  Komm, du Hei -- land al -- ler Welt;
  Sohn der Jung -- frau, mach dich kund.
  Da -- rob stau -- ne, was da __ lebt:
  Al -- so will Gott wer -- den Mensch.
}

textB = \lyricmode {
  \set stanza = "2."

  Nicht nach ei -- nes Men -- schen Sinn,
  son -- dern durch des Geis -- tes Hauch
  kommt das Wort in un -- ser __ Fleisch
  und er -- blüht aus Mut -- ter -- schoß.
}

textC = \lyricmode {
  \set stanza = "3."
  Wie die Son -- ne sich __ er -- hebt
  und den Weg als Held durch -- eilt,
  so er -- schien er in der __ Welt,
  we -- sen -- haft ganz Gott und Mensch.
}

textD = \lyricmode {
  \set stanza = "4."
  Glanz strahlt von der Krip -- pe auf,
  neu -- es Licht ent -- strömt der Nacht.
  Nun ob -- siegt kein Dun -- kel __ mehr,
  und der Glau -- be trägt __ das Licht.
}

textE = \lyricmode {
  \set stanza = "5."
  Gott dem Va -- ter Ehr __ und Preis
  und dem Soh -- ne Je -- sus Christ;
  Lob sei Gott dem Heil -- gen Geist
  jetzt und e -- wig. A -- _ men.
}

\score {
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "sop" { \voiceOne \sopran }
      \new Voice { \voiceTwo \alt }
    >>
    \new Lyrics \lyricsto "sop" \textA
    \new Lyrics \lyricsto "sop" \textB
    \new Lyrics \lyricsto "sop" \textC
    \new Lyrics \lyricsto "sop" \textD
    \new Lyrics \lyricsto "sop" \textE
    \new Staff <<
      \new Voice { \voiceOne \tenor }
      \new Voice { \clef "bass" \voiceTwo \bass }
    >>
  >>
  \layout {
    \omit Slur
    \accidentalStyle modern-voice
  }
}


\score {
  \new ChoirStaff <<
    \new Staff \new Voice = "sop" { \sopran }
    \new Lyrics \lyricsto "sop" \textA
    \new Staff \new Voice = "alt" { \alt }
    \new Lyrics \lyricsto "sop" \textA
    \new Staff \new Voice = "ten" { \clef "treble_8" \tenor }
    \new Lyrics \lyricsto "sop" \textA
    \new Staff  \new Voice = "bas" { \clef "bass" \bass }
    \new Lyrics \lyricsto "sop" \textA
  >>
  \layout {
    \omit Slur
    \accidentalStyle modern-voice
  }
}

\markup \column {
  \line {
    \bold "2."  Nicht nach eines Menschen Sinn,
    sondern durch des Geistes Hauch
    kommt das Wort in unser Fleisch
    und erblüht aus Mutterschoß.
  }

  \line {
    \bold "3."
    Wie die Sonne sich erhebt
    und den Weg als Held durcheilt,
    so erschien er in der Welt,
    wesenhaft ganz Gott und Mensch.
  }


  \line{
    \bold "4."
    Glanz strahlt von der Krippe auf,
    neues Licht entströmt der Nacht.
    Nun obsiegt kein Dunkel mehr,
    und der Glaube trägt das Licht.
  }

  \line {
    \bold"5."
    Gott dem Vater Ehr und Preis
    und dem Sohne Jesus Christ;
    Lob sei Gott dem Heilgen Geist
    jetzt und ewig. Amen.
  }
}
