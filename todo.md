# Todo

+ import
    - compare all existing versions of same score
    - copy best
    - unify by removing layout and moving it to settings

+ layout
    - page
        - paper margins
        - page fit
        - header and footer
        - title page
    - font
        - common text font
        - common music font
        - common staff size
        - lyric font size
        - bar numbers
    - incipits
    - octavation eight
    - slurs and ties
    - 

+ source code
    - reformat
    - lylint
    - check mutopia for recommendations
    - order of file contents
        - version + language
        - rough header
        - include (makes real header)
        - edition engraver
        - music content
        - score
    - common footer (JB + LP)
    - edition engraver
    - naming scheme: composer-title

+ midi
    - common choir midi generation (SATB + single voices)
    - common piano midi generation

+ publish
    - license
    - mutopia upload
    - Program and rehearsal generation from md file
    - publish automatically in html for website

+ workflow
    - test naming scheme
    - check if all have the configured version (newer is ok)
    - test if all files compile
    - run lylint
    - export selected files to mutopia repo
    - export all files (selection?) to web folder
    - generate web index
    - generate commented style file

