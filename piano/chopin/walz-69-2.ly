\version "2.25.22"
\language deutsch

#(set-global-staff-size 19)

%{
Pedale in extra Zeile
Finger außen/innen
Scripts mit padding
Slurs tiefer

%}

%\include "layout.ily"
\include "editorial-tools/edition-engraver/definitions.ily"
\include "fingering-glissando.ly"

\paper {
property-defaults.fonts.music = "profondo"
property-defaults.fonts.serif = "Linux Libertine O"
property-defaults.fonts.sans = "Linux Biolinum O"
property-defaults.fonts.typewriter = "Ubuntu Mono"
}

\header {
  title = "Walzer"
  opus = "op. 69 Nr. 2"
}

\paper {
  system-count = 12
  ragged-last = ##t
}
\addEdition finger
\addEdition scripts
\addEdition dyn
\addEdition breaks

% fingering rule
\editionMod finger 1 1/4 edition.Staff.A \override Fingering.avoid-slur= #'inside
\editionModList finger edition.Staff.A \once \override Fingering.avoid-slur= #'outside
#'(
    ;(1 (ly:make-moment 1 4 -1 4))
    2
    (2 2/4)
    4
    (4 2/4)
    (5 5/8)
    6
    (6 2/4)
    (7 2/4)
    10
    (10 2/4)
    12
    14
    16
    (16 5/8)
    17
    (21 5/8)
    22
    26
    30
    (30 5/8)
    31
    33
    34
    35
    36
    (36 5/8)
    37
    38
    39
    (39 5/8)
    41
    (41 5/8)
    42
    43
    44
    (44 5/8)
    45
    (47 4/8)
    50
    (50 2/4)
    52
    (52 2/4)
    56
    58
    63

)

% finger down
\editionModList finger edition.Staff.A
  \once \override Fingering.staff-padding = ##f
  #'(
    (1 5/8) 4
    (9 5/8)
    (13 2/8) (14 2/8) (15 2/8) (15 5/8) (16 1/8) (16 4/8)
    (24 5/8) ; extra
    (25 5/8)
    (30 2/8) (31 2/8) (31 5/8)
    (34 4/8) (37 3/8) (38 4/8) (39 4/8) ; 40 not  exception
    (41 3/8) (41 4/8) ; exception
    (49 5/8)
    (57 5/8)
    (63 2/8) (63 5/8)
    )


% fingering manual
\editionModList finger edition.Staff.A
  \once \override Fingering.padding = 1
  #'(
      (49 5/8)
      (57 5/8)
    )

\editionMod finger 47 4/8 edition.Staff.A
  \shape #'((0 . 0) (2 . 0) (0 . 0) (-0.5 . -1.1)) Slur
\editionMod finger 56 0/8 edition.Staff.A
  \shape #'((0 . 0) (2 . 0) (0 . 0) (-0.5 . 0.5)) Slur

\editionModList scripts edition.Staff.A
  \shape #'((0 . -0.5) (0 . -0.4) (0 . 0) (0 . 0)) Slur
  #'(
      ;(23 2/4)
      35 39 41 43 31
    )

% script direction
\editionMod scripts 1 0/4 edition.Staff.A  \override Script.direction = #DOWN
\editionMod scripts 18 0/4 edition.Staff.A  \override Script.direction = #UP
\editionMod scripts 53 0/4 edition.Staff.A  \override Script.direction = #DOWN
\editionMod scripts 64 0/4 edition.Staff.A  \override Script.direction = #UP


%\editionModList breaks edition.Staff.A { \break \once \override NoteHead.color = #red }
%#'(6 12 16 23 (29 4/4))

right = \relative {
  \tempo "Moderato" 4=152
  \key h \minor
  \time 3/4
  \partial 4

  \override DynamicText.outside-staff-padding = 50
  \override TextScript.font-shape = #'italic
  \override TextScript.font-series = #'bold
  \override Script.staff-padding = 1
  \override Glissando.details.finger-slides = ##t
  %\override Glissando.details.bow-glissando-indices = #'(0 1)
  \override Glissando.breakable = ##t
  \override Glissando.after-line-breaking = #'()


  fis''4->-4( ~ |
  fis8\p g fis cis-2 d-3 h-1 |
  ais2-2) fis'4->-4( ~ |
  fis8 g fis cis-2 e-3 d |
  h2-1) fis'4->-2( ~ |
  fis8\< g eis-1 fis h-4 d-5\!) |
  d4-4( eis,)
  \shape #'((0 . 0) (0 . 0.5) (0 . -0.2) (0 . -1.1)) Slur
  d'4-5( ~ |
  d e,) cis'-4( ~ |
  cis8\> h ais g-1 fis-4 cis) |
  d-1\p( g-5 fis-4 cis-2 d-3 h-1 |
  ais2) fis'4->-4( ~ |
  fis8 g fis cis-2 e-3 d-2 |
  %\break
  h2-1)
  \shape #'((0 . -1.5) (0 . 1) (0 . 0) (0 . -1)) Slur
  fis'4->-1( ~ |
  %\override DynamicTextSpanner.
  fis8\cresc fis'-5 cis-3 d-4 ais-2 h-5) |
  \override TextSpanner.bound-details.left.text = \markup \italic "rit."
  \override TextSpanner.bound-details.right.text = \markup \italic "dim."
  a-4( g h,-1 cis_\startTextSpan d e) |
  g-5( fis-4 h,-1\stopTextSpan d-3 cis fis,-1) |
  \stemDown
  h-3( eis,-1 fis ais h-1 cis-2) \glissando \break
  %\override Script.direction = #UP
  %\break
  %\once \override DynamicText.outside-staff-priority = 4
  \once \override TextScript.outside-staff-priority = 1
  d-2(-"a tempo"\p g fis cis d h |
  \stemNeutral
  ais2) fis'4->( ~ |
  fis8 g fis cis e d |
  h2) fis'4( ~ |
  fis8\< g eis fis h-4 d-5) |
  d4-4\f( eis,) d'-5 ~ |
  d( e,) cis'4( ~ |
  cis8-"dim." h ais g-1 fis-4 cis-2) |
  d-1\p( g-5 fis cis-2 d h-1 |
  ais2) fis'4->( ~ |
  fis8 g fis cis e d |
  h2)
  \shape #'((0 . -1.5) (0 . 0) (0 . 0) (0 . -1)) Slur
  fis'4->( |
  \once \override Fingering.avoid-slur = #'outside
  fis'8-5\mf cis-3 d-4 ais-2 h-5 fis-2) |
  \override TextSpanner.bound-details.right.text = \markup \italic "tempo"
  a-4(_\startTextSpan g h,-1 cis e g-5) |
  g8-4( fis h,-1\stopTextSpan d-3 cis fis,-1) |
  h4. fis8-1( h-4 b-3)
  \bar ".|:-||"
  \repeat volta 2 {
    <>^\markup \italic \left-column { "a tempo" "  con anima" }
    b4.-2( a8 cis e) |
    g4.-5\>( fis8 cis-1 d\!) |
    %\shape #'((0 . -0.5) (0 . -0.4) (0 . 0) (0 . 0)) Slur
    fis4.-4( e8 h cis) |
    e4.-4( d8 cis h-1) |
    h4.-2( a8-1 cis e) |
    g4.-5( fis8 cis-1 d) |
    fis4.-4( e8 h-1 cis-4) | \break
    \override TextSpanner.dash-fraction = 0.1
    \override TextSpanner.dash-period = 5
    \override TextSpanner.padding = 2
    ais4.-2_\startTextSpan << { cis8 g-1 cis } \\ { s8 g4 } >> |
    fis4.-1(\stopTextSpan ais8-2 cis-3 e-4) |
    g4.-5( fis8\> cis-1 d)\! |
    fis4.-4\>( e8 h cis\!) |
    e4.-4\>( d8 cis h-1\!) |
    h4.-2( a8 cis e) |
    g4.\>( fis8 cis d\!) | \break
    fis4. e8-1 fis'-5\f( e |
    d8\> cis c-1 h-3 ais-2 a-1\! |
    gis8-3\> g fis-4 cis d h-1\! |
    ais4-2) r a'-5\sf( |
    gis8 g fis-2 cis-1 e-3 d-2 |
    h4-1) r a'-4\sf( | \break
    gis8\< g eis-1 fis h-4 d-5\! |
    d4-4 eis,-1) d'->-5 ~ |
    d4( e,)
    %\shape #'((-0.5 . 2.2) (0 . 2.5) (3.7 . 2.5) (4.2 . 2.2)) LaissezVibrerTie
    %\override Glissando.details.bow-glissando-indices = #'(0 1)
    \once \override Fingering.padding = 0.88
    cis'->-4 \glissando |
    \shape #'((0 . 0) (2 . 0) (0 . 0) (-0.5 . -1.1)) Slur
    c8-4( h ais\> a-1 gis-4 g |
    fis8 f-1 e-4 d cis
    \once \override Fingering.padding = 1
    h-1\! |
    ais2-2) g'4->( | \break
    fis8 g fis cis e d |
    h2) fis'4->( |
    fis'8\mf cis d ais-2 h-5 fis-2) |
    \override TextSpanner.dash-period = 0
    \override TextSpanner.bound-details.right.text = \markup \italic "a tempo"
    a8(_\startTextSpan g h, cis e g) |
    g-4( fis h,-1 d-3 cis fis,-1) |
  }
  \alternative {
    { h4.->(\stopTextSpan fis8 h b) \break }
    { \omit Accidental h4 r fis-1 }
  }

}

left = \relative {
  \clef bass
  \key h \minor
  \time 3/4
  \partial 4

  r4 | h, <fis' h d> q | cis <fis cis' e> q |
  ais, <fis' ais e'> q | h, <fis' h d> q | h, q q
  h <gis' h d> q | h, <g' h e> q |
  h, <fis' cis' e> <fis ais e'> | h, <fis' h d> q |
  cis <fis cis' e> q | fis, <fis' ais e'> <fis cis' e> |
  h, <fis' h d> q | d q q |
  e <h' cis g'> r | << { \voiceTwo fis <h d> <ais e'> }
                       \new Voice { \voiceOne fis2. } >> |
  \oneVoice
  <h d>4 h, r |
  h4 <fis' h d> q | cis <fis cis' e> q |
  ais,4 <fis' cis' e> <fis ais e'> |
  h, <fis' h d> q | h, q q |
  h <gis' h d> q | h, <g' h e> q |
  h, <fis' cis' e> <fis ais e'> |
  h, <fis' h d> q | cis <fis cis' e> q |
  fis, <fis' ais e'> q |
  h, <fis' h d> q |
  d <fis h d> q |
  e <g cis> r |
  fis <h d> <ais e'> |
  h, <fis' d'>
  \shape #'((1 . 0.6) (1 . 0.6) (-0.5 . 0.6) (-0.5 . 0.6)) Slur
  d_( |
  %\bar "||"
  \pageBreak
  \repeat volta 2 {
    cis) <g' a e'> q |
    d <fis a d> q |
    a, <a' cis g'> q |
    d, <a' d fis> q |
    cis, <a' e' g> q |
    d, <a' d fis> q |
    g, <g' h e> q |
    <fis, fis'> <fis' cis' e> <fis h e> |
    <fis, fis'> <fis' cis' e> <fis ais e'> |
    h, <fis' h d> q |
    a, <a' cis g'> q |
    d, <a' d fis> q |
    cis, <a' e' g> q |
    d, <a' d fis> q  |
    g, <g' e'> r |
    fis, <fis' e'> ais,( |
    h) <fis' d'> q |
    cis <fis cis' e> q |
    ais, <fis' cis' e> q |
    h, <fis' h d> q |
    h, q q |
    h <gis' h d> q |
    h, <g' h e> q |
    h, <fis' ais e'> q |
    h, <fis' h d> q |
    cis <fis cis' e> q |
    ais, <fis' ais e'> <fis cis' e> |
    h, <fis' h d> q |
    d <fis h d> q |
    e <g cis> r |
    fis <h d> <ais e'> | }
  \alternative {
    { h, <fis' d'> d | }
    { <h' d> h, r }
  }
 % \bar "||"



}

ped = { s4\sustainOn s s\sustainOff }

pedal = {
  \partial 4
  s4
  \repeat unfold 14 \ped
  s2.*2
  \repeat unfold 13 \ped
  s2.*3
  \repeat volta 2 {
    \repeat unfold 7 \ped
    s2.
    \repeat unfold 22 \ped
    s2.
  }
  \alternative { { s2. } { s2. } }
}

\score {
  %\articulate
  \new PianoStaff
  \with {
    instrumentName = \markup \abs-fontsize #20 "5"
    \accidentalStyle piano
    \consists \editionEngraver edition
  } <<
    \new Staff
    \with { \consists \editionEngraver edition }
    \right
    \new Staff
    \with { \consists \editionEngraver edition }
    \left
    \new Dynamics \pedal
  >>

  \layout {
   % \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = 80
      \override DynamicTextSpanner.style = #'none
      \override TupletNumber.font-series = #'bold
      \mergeDifferentlyHeadedOff
      \override StaffGrouper.staff-staff-spacing.basic-distance = 11.0
      \override StaffGrouper.staff-staff-spacing.stretchability = 0
      \override DynamicLineSpanner.staff-padding = #3.1
      
   %     \override Staff.StaffSymbol.thickness = #1.2
  %\override Staff.Beam.beam-thickness = #0.55
  %\override Staff.Slur.thickness = #1.5
  }
  \midi { }
}