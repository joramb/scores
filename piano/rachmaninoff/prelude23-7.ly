\version "2.25.22"
\language deutsch

#(set-global-staff-size 18)

\include "oll-core/package.ily"
%\include "edition-engraver/definitions.ily"
\loadPackage edition-engraver
%\edi


\addEdition always
\addEdition mystyle
\addEdition original

% dynamics
\editionMod always 15 0/4 edition.Staff.A \once \override DynamicText.extra-offset = #'(-1.5 . 0)


\editionMod always 2 0/4 edition.Staff.B \alterBroken extra-offset #'((-1 . -0.26) (0 . 0)) Tie
\editionMod always 5 2/4 edition.Staff.B \alterBroken extra-offset #'((0 . 0) (-1 . -0.26)) Tie
\editionMod always 23 2/4 edition.Staff.Voice.A \alterBroken positions #'((8 . 8) (0 . 0)) Slur
\editionMod always 18 0/4 edition.Staff.A \once \override Tie.details.height-limit = 2.8

% rest positions
\editionMod original 17 0/4 edition.Staff.B  \override Rest.staff-position = -6
\editionMod original 20 0/4 edition.Staff.B  \override MultiMeasureRest.staff-position = -2
\editionMod original 25 1/4 edition.Staff.B  \revert Rest.staff-position
%\editionMod original 25 1/4 edition.Staff.B  \revert MultiMeasureRest.staff-position
\editionMod original 27 0/4 edition.Staff.B  \once\override Clef.extra-offset = #'(1 . 0)
% accidentals


\header {
  dedication = \markup \smaller \italic "À Monsieur A. Siloti\n"
  title = "Prelude"
  composer = "S. Rachmaninoff"
}

\paper {
  %top-margin = 25\mm
  left-margin = 15\mm
  right-margin = 15\mm
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  %bottom-margin = 40\mm
  %#(set-paper-size "letter")
  top-margin = 8\mm                              %-minimum top-margin: 8mm
  top-markup-spacing.basic-distance = #6         %-dist. from bottom of top margin to the first markup/title
  markup-system-spacing.basic-distance = #5      %-dist. from header/title to first system
  top-system-spacing.basic-distance = #12        %-dist. from top margin to system in pages with no titles
  last-bottom-spacing.basic-distance = #18       %-pads music from copyright block
  ragged-last = ##f
  ragged-bottom = ##f
  ragged-last-bottom = ##f
  page-count = #3
  %scoreTitleMarkup = \markup \on-the-fly #'header
  property-defaults.fonts.music = "cadence"
  property-defaults.fonts.serif = "Linux Libertine O"
  property-defaults.fonts.sans = "Linux Biolinum O"
  property-defaults.fonts.typewriter = "Ubuntu Mono"
}

mg = { <> ^\markup \halign #-0.5 \italic "m.g." }
md = { <>^\markup \halign #-0.5 \italic \bold "m.d." }
% switch from upper staff (main droite) to lower (main gauche) and vice versa
up = { \change Staff = "upper" \voiceTwo  }
down = { \change Staff = "lower" \voiceOne  }
su = \stemUp
sd = \stemDown
sn = \stemNeutral

vi = \relative {
  \tempo Allegro  2 = 80
  \key c \minor
  \override NoteHead.color = #(x11-color "red")
  \override Stem.color = #(x11-color "red")
  \override Rest.color = #(x11-color "red")
  \sd c'16\p^( es fis g \su as\< g fis g \! es'\> d c g\! as[ g] \sd fis g) |
  \sd h,16\p^( es fis g \su as g fis g es' c h g as[ g] \sd fis g)  |
  \sd b,16^( es fis g \su as g fis g es' ces b g as[ g] \sd fis g)  |
  \sd b,16^( d fis g \su as g fis g d' c b g as[ g] \sd fis g)  |
  \sd b,16\p^( des e f \su ges\< f e f\! des'\> c b f\! ges[ f] \sd e f) |
  \sd a,16\p^( des e f \su ges f e f des' b a f ges[ f] \sd e f) |
  \sd as,16^( des e f \su ges f e f des' a as f ges[ f] \sd e f)|
  \sd as,16^( c e f \su g f e f c' b as f g[ f] \sd e f) |
  \sd as,16^( h\< cis d \su es d\! cis d h'\> g f d\! es d cis d) |

  s4
  \voiceOne
  g'2--( as4)
  s2 \once\voiceTwo h,4 s |

  \once \oneVoice r4  g'2--( as4) |
  <g es -\tweak #'extra-offset #'(-3 . 0) _\laissezVibrer>2--(  <des f>)
  \omit DynamicLineSpanner
  \override DynamicTextSpanner.style = #'none
  <a des>--(\dim <as c>)
  <as=' c>1--\pp( | <g h>--) |
  <c c'>--( %\p
  %\shape #'((0 . 0) (0 . 3) (0 . 3) (0 . 0)) Staff.TieColumn
  %\once \override Staff.TieColumn.control-points = #'((1 . -1) (3 . 0.6) (12.5 . 0.6) (14.5 . -1))

  <ces ces'>--) ~ |
  q2 \mg <b b'>--( \md <es es'>-- \mg <d d'>--) |
  \md

  <b b'>1--(

  <a a'>--) ~ q2 \mg

  %\once \alterBroken Slur.positions  #'((0.4 . 10) (0 . 0))
  <as as'>--( |
  \md <des des'>-- \mg  <c c'>--) |
  \md <as as'>1--\p |
  s4 g'2--( as4) |
  <as, as'>1-- %\p |
  s4 g'2--( as4) |
  <des, as' des>1--\mf( |
  <c es a c>1--)\dim
  <as c>2(\p <g h> <es c'>1)
  s1*2
  r4
}

vii = \relative {
  \voiceTwo
  \override NoteHead.color = #(x11-color "steel blue")
  \override Stem.color = #(x11-color "steel blue")
  \override Rest.color = #(x11-color "steel blue")
  s4 s es'' s |
  %\shape #'((0 . 1.5) (5 . 3) (-3 . 3) (0 . 1)) Slur
  s4 s es s |
  %\shape #'((0 . 1.5) (5 . 3) (-3 . 3) (0 . 1.5)) Slur
  s4 s es s |
  s4 s d s |
  s4 s des s |
  s4 s des s | %\noBreak
  s4 s des s | %\noBreak
  s4 s c s |
  s4 s h s |
  \oneVoice
  r4 s r as'16 h,( c es)

  \sd as,,16\<^( h cis d \sn es d\! cis d h'\> g f d\! es d cis d)

  s2 r4  as''16 h,( c es) |
  r4 s s r
  r4 s s r
  % page 2
  %\mergeDifferentlyHeadedOn
  \up
  s1*2 |
  %\override Rest.staff-position = -4
  %\once \override Rest.extra-offset = #'((0.4 . 0) (0 . 0))
  \oneVoice r4 s \su es='' s |
  \oneVoice r4 s \su es s |
  \oneVoice r4 s \once \voiceThree es s |
  \oneVoice r4 s  s s |
  r4 s \su des s |
  \voiceTwo r4 s \su des s |
  r4 s \su
  \once \override NoteColumn.force-hshift = #1.5
  des s |
  r4 s2. |
  r4 s2. |
  \once \oneVoice
  r4 s r \sd as'16 h,^( c es) |
  r4 s \su h s |
  \once \oneVoice
  r4 s r \sd as'16 h,^( c es) |
  s1

  % page 3
  s1*3 \down
  s2 f,16( as h^\p  \up d es as d, h) \down |
  es,( g \up b h d g c g) \down d,( f \up c' h as g h f') \down |
  c,16( es fis g) r4 b,16( d fis g) r4 |
  as,16( c \up es c') \down g,( b \up c des d f b e,) \down f,( as \up c as') \down |
  r8 f,16( fis \up g a h c) \showStaffSwitch \down d( f g \undo \showStaffSwitch \up h c f h, g)
}

viii = \relative {
  \override NoteHead.color = #(x11-color "forest green")
  \override Stem.color = #(x11-color "forest green")
    \override Rest.color = #(x11-color "forest green")
  s1*9
  \clef treble
  g16(^\< d' f h)\! \up g'16
  -\shape #'((1 . 6) (1 . 3) (-8 . 0) (-1 . 2))
  _( c, h g \down f^\> es g as\! \once \stemDown c8) s |
  \clef bass
  s1
  \clef treble
  g,16(^\< d' f h)\! \up g'16
    -\shape #'((1 . 7) (1 . 4) (-8 . 0) (-1 . 2))
  _( c, h g \down f^\> es g as\! \once \stemDown c8) s |
  \clef bass
  f,,,16( des' as' des \up g as c es)
  \tuplet 3/2 4 { des8
    -\shape #'((0.3 . -0.5) (0 . -1) (-1 . 0) (0 . -0.3))
    ( as f \down as des, as)  |
  ges,8( es' a } \up des16 es ges a)
  \tuplet 3/2 4 { as8( es c \down es as, es) } |

  g,=,16( f' c' \up d  es d c d as' g f c d \down c h c) |
  g,16( f' h \up d es d cis d g f d h c \down h b h) |
  c16( es fis g \up as\< g fis g\! es'\> d c g\! as g \down fis g) |
  ces,16( es fis g \up as g fis g es' c ces g as g \down fis g) |
  b,16( es fis g \up as g fis g \once \voiceFour es' ces b g as g \down f g) |
  b,16( d fis g \up as g fis g) d'^( c b g as g \down fis g) |
  b,16( des e f \up ges\< f e f des'\> c b f\! ges f \down e f) |
  %\once \override Slur.details.height-limit = 0.2
  a,16( des e f \up ges f e f des' b a f ges f \down e f) |
  as,16( des e f \up ges f e f)
  \once \override NoteColumn.force-hshift = #1.5
  des'^( a as f ges f \down e f) |
  as,16( c e f \up g f e f) c'^( b as f g f \down e f) |
  as,16( h cis d \up es\< d cis d h'\> g f d\! e d \down cis d) |
  \clef treble
  g,16(^\< d' f h\!) \up g'( c, h g \down f^\> es g as\! c8) s |
  \clef bass
  as,16( h cis d \up es d cis d h' g f d es d \down cis d) |
  \clef treble
  g,16(^\< d' f h\!) \up
  \shape #'((1 . 5) (1 . 3) (-8 . 0) (-1 . 2)) Slur
  g'( c, h g \down f^\> es g as\! \sd c8) \su s |
  \clef bass
  \shape #'((-0.4 . -5) (3 . 3) (-9 . 0) (-0.2 . 0)) Slur
  f,,,16( des' as' \up c es des c des \down f as, des f as \up b c des) \down |
  fis,,,16( es' a \up c d c h c \down es a, c es a \up h c es) \down |
  g,,,16( f' as \up c es d cis d) \down g,( d' f \up fis as g fis g) \down |
  c,,16( g' c d es \up g d' c es c g es c g \down es c) |
  g16( f' h \up c des d es e)
  \showStaffSwitch \down \clef treble  \voiceTwo
  f2---> |
  es2-- d-- c-- b--
  \clef bass
  as4-- g2-- f4-- \up \stemUp <es c' g'>--\arpeggio s
  \undo \showStaffSwitch
  \down \voiceTwo d'2-> |
  c2-- h-- | as-- g-- |
  f4-- es2-- des4-- \up \stemUp <c as' f'>4--\arpeggio s4 \down \voiceTwo h'4.( c8) |
  as2-- g-- | f-- es-- | d4-- c2-- as4--
}

viiii = \relative {
  \override NoteHead.color = #(x11-color "purple")
  \override Stem.color = #(x11-color "purple")
  \override Rest.color = #(x11-color "purple")
  \override MultiMeasureRest.color = #(x11-color "purple")
  \clef bass
  \key c \minor
  \override Script.direction = #UP
  %\override DynamicText.direction = #UP
  %\override Dynamics.direction = #UP
   r2 c,->\sf ~ 1 ~ 1 R |
   r2 c^>^\sf
   -\alterBroken extra-offset #'((0 . 0) (0.6 . -0.26))
   ~ 1 ~ 1 R |
   r2 c^>^\sf |
   s4 r s4. r8
   r2 c^> %<c c,>1 ~ q
   s4 r s4. r8 |
   s4 r r s
   s4 r r s
   %\voiceTwo \stemUp
   s1 | s1 |
   %\once \override Rest.staff-position = #-6
   \override Script.direction = #UP
   \once \voiceTwo
   r2 <c, c'>-- ~ |
   q1 ~ | q |
   \voiceTwo
   % \override Rest.direction = #DOWN
   R1 |
   r2
   \oneVoice
   <c c'>-- ~ | q1 ~ | q |
   \voiceTwo
   R1 |
   r2 \oneVoice c'-- |
   s4 r s s8 r |
   \once \voiceTwo
   r2 c-- |
   s4 r s s8 r |
   s1

   % page 3
   s1*7 |
   <g g'>1
   s1*3
   <f f'>1
   s1*3


}

dyna = {
  4\p 4\< 4\> 4\! 1\p 1 1
  4\p 4\< 4\> 4\!
}

\new PianoStaff
\with { \accidentalStyle piano
 %\consists \editionEngraver edition
}<<
  \new Staff = "upper"
  %\with { \consists \editionEngraver edition }
  <<
    { s1 s1 \repeat unfold 14 { s1*3 \break }}
    { s1*14 \pageBreak s1*15 \pageBreak s1*15 }
    \new Voice \vi
    \new Voice \vii
  >>
  \new Dynamics \dyna
  \new Staff = "lower"
  %\with { \consists \editionEngraver edition }
  <<
    \new Voice \viii
    \new Voice \viiii
    { s1 s1 \repeat unfold 14 { s1*3 \break }}
  >>
>>
\layout {
    \context {
      \Score
      %\omit TupletNumber
      \omit TupletBracket
      \override DynamicTextSpanner.style = #'none
      \override TupletNumber.font-series = #'bold
      \mergeDifferentlyHeadedOff
      \override StaffGrouper.staff-staff-spacing.basic-distance = 11
      \override DynamicLineSpanner.staff-padding = #3.5
      %\consists \editionEngraver my.test % set this to your "global" tag-path
    }
}

