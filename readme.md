# Scores

This is a collection of scores for piano and scores for choirs.

## Table of contents

* **Piano scores**
    * <span class="composer">Chopin</span>: Fantaisie Impromptu (op x)
    * Beethoven: Sonate pathéthique (op x)
    * Rachmaninov: Prelude No. 5 (op. 23)
    * Rachmaninov: Prelude No. 9 (op. 23)

* **Choir scores**
    * Allegri: Miserere mei
    * Attaingnant: Tourdion
    * Bach: Aus tiefer Not
    * Bach: Wie soll ich dich empfangen
    * Bach: Brich an, o schönes Morgenlicht
    * Bach: Seid froh dieweil
    * Bach: Wachet auf
    * Di Lasso: Mon cœur
    * Dowland: Flow my tears
    * Encina: Pues que
    * Franck: Kommt her zu mir
    * Gesius: Puer natus
    * Luther: Komm du Heiland
    * Morley: April
    * Morley: Come again
    * Purcell: Thou knowest Lord
    * Sweelinck: Psalm 90
    * Vittoria: Ne timeas


## Engraving

The free music engraving program *[LilyPond]* is used to produce these scores. This repository only contains the input files for *LilyPond* – the pure informational content so to speak. The output PDF files can be found on [my website] and in future at the *[Mutopia Project]* and perhaps later at *[IMSLP]*.

All scores require a global include file for layout settings. They compile
without it but do not look the way they are intended to look. In addition, the font *[Linux Libertine]* is required.

You can build all scores with different fonts, house-styles and for different
paper sizes. However, the scores have not been optimized for such changes and
it is likely that the line and page breaks look odd.


## Copyright

The author of all these scores is [Joram Berger].
These scores are old and the originals are free of copyright …
The engravings of this collection are licensed under the Creative Commons [CC BY-SA 4.0] license.
Please share them freely and distribute your modifications according to the license.


[LilyPond]: http://lilypond.org
[Linux Libertine]: x
[my website]: http://joramberger.de/scores
[Joram Berger]: http://joramberger.de
[Mutopia Project]: http://mutopiaproject.org
[IMSLP]: http://imslp.org
[CC BY-SA 4.0]: http://



- LilyPond repos:
    - Partituren
        - für Website (dry)
        - für Mutopia (stand-alone, automatisch)
        - englisch, deutsch (wo kommt das vor?)
    - Tools
        - lylint
        - sprachwechsel (input/output)
        - ersetzer für standalone (include, header fields?)
        - versionsupdater
        - visual diff
        - Programm/Notenbuch als pdf mit Latex
        - documented ly file -> html mit code
    - docs
        - visual index
        - cheat sheet
        - introduction slides
    - bugs

### LilyPond

`lydiff`

[Lyric extenders](https://www.mail-archive.com/lilypond-user@gnu.org/msg117118.html) [2](https://www.mail-archive.com/lilypond-user@gnu.org/msg116911.html)

[edition-engraver](https://github.com/openlilylib/edition-engraver/wiki)


Lilypond:
  scores
  prüfen, lint
  update mit convert-ly
  neue ly version installieren
  Schulvorlagen
  Artikel


Automatisiertes System:

* includes
   * style & git
      * page: header / margin
      * score
   * functions
   * edition engraver / OLL
   * pdf headers
* Stimmen und zip-Kollektionen
* Einzelstücke und Sammelband (nach Instrumenten)
   * test module (ily)
   * Buch:
      - Seitenzahlen, Titel, Impressum, Inhalt, einleidung, Doppelseitig
      * Titel (Header) gleich
      * Fußzeilen nur am Anfang & Ende
* Fonts
   * [Academico](https://github.com/dbenjaminmiller/academico-mirror) text font
   * [bmusicfonts](https://github.com/dbenjaminmiller/bmusicfonts) ~ Bravura


Lilypond Scores
===============

## scores

- stücke aus anderen Ordnern sammeln (copyright?)

## general layout

## tools and functions

## rachmaninoff

## LilyBlog artikel

- oly Schule
  - roter Faden, link zu vorherigem Post
  - Beispiele
  - Kontakt Urs
- tweaks s. "general layout"
- ly diff


Titel:

Stücke für vierstimmigen Chor
ly/gridly/grid-templates.ily
Links:
[Video tutorials]: https://www.youtube.com/playlist?list=PLHi8BvxILUV7hsUjn_Az5filU1TsNzq2R


lyArtikel:

Tabelle
Art    Papier (ISO), Papier (Recommended), Zeilenhöhe

Chor (4 Stimmen)
Orchester
Klavier
Solovioline
Gitarre
Band
Studienpartitur


# Comparing LilyPond Scores

What is the difference between two similar scores?
What is the visual effect of a LilyPond command?
How will my score change when I use a newer version of LilyPond?

In all these situations it would be helpful to compare the output on the same page.
Let's have a closer look at the three scenarios described above.

## Comparing different versions of a file
Imagine there is a LilyPond command which is supposed to have a desirable effect on your score but you are not sure and the difference is too small to spot it immediately.
Or you just found different versions of a score on our computer and you don't remember which one was the better version.
You might even use [git] and you can already track the changes in the source file easily but the changes in the *output* are harder to visualize.
Of course, one can switch between the two output files and check for moving objects, but a combined image would be more helpful.

[git]: http://lilypondblog.org/tag/version-control/

## Comparing LilyPond versions
Another use case is comparing LilyPond version.
Imagine you have engraved a score using LilyPond and you took great care to make it look perfect.
Now there is a new LilyPond version and you would like to stay up-to-date. But you are afraid that this could mess with your tweaks and spoil your score. Wouldn't it help to try and see if there is any change at all and by how many millimetres or micrometres objects have moved?

## Visual comparison
There is now a solution for that:

```bash
lydiff file1.ly file2.ly
```
or

```
lydiff file1.ly -v 2.18.2 2.19.53
```

Both files are processed in parallel following this chain of tools:
At first, they are converted to the required version using `convert-ly`. Subsequently both files are run through LilyPond producing PNG images.
All that happens on a temporary copy to keep your input files untouched.
Finally, the tool uses the [convert] tool from [ImageMagick]. Common pixels are coloured in black. Pixels only present in the first file/using the first version are coloured in red and from the second file/version only are coloured in green.

[convert]: https://imagemagick.org/script/convert.php
[ImageMagick]: http://www.imagemagick.org/script/index.php

## Example

The following example input file shows the usage of `lydiff`. The first part is only used to make these notes fit on a very small page.
The `pointandclick` functionality is turned off because even invisible changes in the source would affect the output file. The real content is contained in the last four lines.

```
\version "2.18.2"

\paper {
  indent = 0
  paper-height = 3\cm
  paper-width = 7\cm
}

\header {
  tagline = ##f
}

\pointAndClickOff

{
  c' d' e' f' |
  g'1
}
```

Now in a first step, we do some changes that should not affect the output: we switch to relative notation.

The SVG file is indeed identical (`diff notes1.ly notes2.ly` shows no difference, not even a timestamp). Also the presented `lydiff` tool shows an entirely black score without changes as expected.

In a second step, we now introduce a fermata on the last note and add a final bar line. The difference of the input files looks like this:

```
 \relative {
   c' d e f |
-  g1
+  g1_\fermata
+  \bar "|."
 }

```

The output SVG files are now clearly different. Let's see what `lydiff` tells us:

`lydiff notes2.ly notes3.ly`

diff img

The additional fermata is printed in green.
The final barline is visible in green and it compresses the horizontal spacing a little bit.
So you see all the secondary effects of a change, too.
Depending on your use case, this can be both an advantage and a disadvantage.

The tool accepts lists and [glob] expressions pointing to all installed LilyPond binaries.
Commandline arguments to `lilypond` are also accepted.
The `-h` option provides more help.

[glob]: glob

## Possible improvements

The pixel hinting produces some shades of red and green at the contours of the objects even for negligible differences. A diff of a vector graphic output would be preferable.
The vector graphic editor [inkscape] can create intersections, unions and differences of paths and here is the result as vector graphic (svg) and PDF.
While I was able to do that using [inkscape] by hand, I don't know if it's possible to write some sort of script or macro that does it in an automated way.

One major shortcoming is that only single page scores are correctly handled at the moment. But this will change soon.

I hope `lydiff` will be useful for LilyPond users, especially for those who care about the details.
This is currently version 0.1 and your comments are very welcome.

# md extension
# settings oll
# walk through score project

XXX ersetzen!

intro: alten noten abschreiben lp automatisch beautiful by default

LilyPond takes great care of the score quality. Most guidelines you can find online are futile as their recommendations are automatically considered by LilyPond and its set of rules is much more detailed than such guidelines could convey. However that does not mean that there is nothing left to think about.

The following article is therefore a walk through the preparation of a vocal score full of personal choices and subjective decisions. Of course, beauty is in the eye of the beholder and would like to know your comments. For those of you who mostly used the default settings, this article could also help you to tweak your scores to your liking.

One of the typical tasks of LilyPond users is the copying of old and [public domain]() scores. So for this example we need an [original score] and I chose "Brich an o schönes Morgenlicht" from Bach's [Christmas Oratorio].

[original score]: http://imslp.org "IMSLP"
[Christmas Oratorio]: https://wikipedia.org

It is a choral piece for a choir of four voices which are quickly entered separately and stored in music variables. Furthermore, a basic [SATB] score setup is used to combine the voices into a score with one voice per staff. The lyrics are the same for all four voices so they are entered only once. [This score] is now what I would call the default LilyPond output. It is produced purely from the necessary musical information and some meta data ([input file]). This is our starting point, no tweaks are applied yet and I dare say it already looks good thanks to the many well-designed decisions LilyPond makes behind the scenes.

[SATB]: XXX
[This score]: score1.pdf "LilyPond default output"
[input file]: score1.ly "Musical content"

So what could we still improve? In the remainder of this article we will take care about

1. staff sizes and spacing
1. text and music fonts
1. titles and meta data


## Size and Spacing

The distribution of notes over the page is a bit suboptimal so let's see what we can do.

### Paper size
First we have to [choose] the [paper] the score should be printed on.
While small pages (and staff sizes) are not readable from the distance, large formats are unwieldy. The compromise to be found depends on the use case: singers are usually closer to their scores than instrumentalists.
I use the [A4] paper size which is standard in most countries in the world and the LilyPond default.
While a wider page layout (e.g. 1 mm × 1 mm) would be beneficial for scores and such formats are being used by publishing houses (Quart, Bach, Oktav), I stick to A4 because it is easy to print and copy. The slightly larger format [B4] (250 mm × 353 mm) could be an alternative.

Klavierformat (piano and violin parts):

- Boosey & Hawkes (1992) piano Rachmaninoff
- Henle (1957) Bach, piano
- Henle (1977) piano
- Edition Peters violin Beethoven
- Edition Peters violin Mozart

N4 (piano):

- Breitkopf & Härtel 1913 piano Chopin

Choir scores:


These values are needed to enter my little statistic:

- staff size measured from the middle of the lowest staff line to the middle of the highest staff line in mm
- distance between staves in the same system

Font size measurements:

- total height of the forte symbol in mm
- height of a capital letter (like "H") in the score title
- height of a capital letter (like "H") in a metronome mark

Paper and margins:

- page height in mm
- page width in mm
- margins in mm
    - top
    - bottom
    - left (outer margin)
    - left (inner margin)
    - right (inner margin)
    - right (outer margin)

staff   paper   margins     font sizes
S,     SS,  ff, pagew, h, tit, mtr, cmp, txt x/H, finger, bar-n, margins, tb, l, r, inl, inr, clefmod, Source

(7.5, 15.0, 4.5, 232, 310, 6.0, 3.5, 3.0, 2.0,  nn, 2.0,  nn, 12, 25, 17, 16, 12, 12,  nn, "Boosey & Hawkes 1992 Piano Rachmaninoff"),



[choose]: http://lilypond.org/doc/v2.19/Documentation/notation/paper-size-and-automatic-scaling "Setting the paper size"
[paper]: http://lilypond.org/doc/v2.19/Documentation/notation/predefined-paper-sizes "Predefined paper size"
[A4]: https://en.wikipedia.org/wiki/Paper_size#A_series "ISO A4 format"
[B4]: https://en.wikipedia.org/wiki/Paper_size#B_series "ISO B4 format"


### Margins and Vertical Spacing
The default margins are very small. The usable line width is larger and closer to the traditional XXX format when using such small margins. More music can fit on the page. That might be a reason why margins of scores are traditionally smaller than for text books. I looked through all my scores by traditional publishing houses and the paper margins are between XXX mm and XXX mm. 15 mm is a reasonable value close to the mean.




### Staff Size
The piece is small enough to fit on a single page and as a choir member I enjoy it a lot if turning and switching pages is minimized. The [standard staff size](http://lilypond.org/doc/v2.19/Documentation/notation/setting-the-staff-size) of 20 pt is too large in this case. Choir works are traditionally engraved with [rastrum](https://en.wikipedia.org/wiki/Rastrum) 4 which is typically 6.5 mm (Gould) or 18.5 pt. The values vary mostly between 18 pt and 19 pt.
In historical practice,

table of typical staff sizes

For this vocal score sizes between 18 and 19 are in agreement with historical practice.
It is advisable to use a consistent staff size for all scores of a book or collection.

### Vertical Spacing
While the ragged bottom setting is useful for small scores comprising just a few staves, we want to use the whole page and rather have a visible separation between the choir staff groups (`ragged-last-bottom = ##f`). The spacing between the last staff group and the footer (`last-bottom-spacing`) can be larger and more stretchable.
The horizontal spacing is already fine and I see no need for improvements here.


## Fonts
The default text font in LilyPond is *Schola* from the TeX Gyre family. It is a good default close to the typical style of 19th century scores.
However, I prefer to give my scores a slightly more modern look by choosing *Linux Libertine* which is a free font. Titles and lyrics look a bit finer but not too flamboyantly. For numbers I still like the *Schola* font better and so it is used for tuplet numbers (triplets), bar numbers, the clef modification (the small 8 for the tenor voice), *ottava* brackets and for stroke fingering idications. This decision introduces some inconsistency but different fonts are often used for the time signature and triplets so this distinction is in line with traditional engraving.

Concerning the music font, the creators of LilyPond and the [*Emmentaler*](http://lilypond.org/doc/v2.19/Documentation/notation/the-feta-font) font have done an
extremely good job and I never found another font that would be as good or better than *Emmentaler*.
One exception is perhaps [*Cadence*](foundryXXX) which is derived from *Emmentaler* and imitates the effects of metal plate printing on the glyph shapes among other improvements. We keep *Emmentaler* here as *Cadence* is no longer free and I want you to be able to redo all the tweaks presented here.

Settings:

## Headers and Meta Data XXX metadata



This was a long list of detailed tweaks and while probably every reader would decide differently for some items, I guess most of you like the tweaked version better than the default output we started with.

What is your opinion? I would like to hear what you think about these settings and what you would have changed. What are the settings you change for your scores?

Checklist:

- paper size
- paper margins
- vertical spacing
- title, composer, poet, copyright and tagline
- metadata
- How should bar numbers be printed?
- Do you like the appearance of ottavation brackets?
- Do you like the appearance of octavated clefs?

Index by:

- object
    - visual
    - list
