\version "2.25.22"

%#(set-global-staff-size 40)

\include "layout.ily"

{ \clef "treble^8" a->  a(-- a-_ a-.) a2 a }
\addlyrics {
  a __ a __ a  a
}

clf = #(define-music-function (cname) (string?) #{ \override Staff.Clef.extra-offset = #'(2 . 2) #})

<<
\new Staff {
  %\once \override Staff.Clef.extra-offset = #'(0 . -4.5)
  %\revert Staff.Clef.font-family
  %\clf #"A"
  %\tweak color #red
  \clef "bass^8" a }
\new Staff {
  \override Staff.OttavaBracket.edge-height = 50
  \clef "treble_8" \ottava 1 a a \ottava 0 }
\new Staff { \clef "bass_8" a a }
>>