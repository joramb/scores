\version "2.25.22"

%% https://lists.gnu.org/archive/html/lilypond-user/2019-02/msg00109.html
%% https://lists.gnu.org/archive/html/lilypond-user/2019-02/txtOTLfdw8_wU.txt
%% After
%% http://lilypond.1069038.n5.nabble.com/Glissando-on-single-note-in-chords-tt34672.html#a34676


#(define (replace-gliss-with-tie which)
  (lambda (grob)
    (let* ((gliss-count (ly:grob-property grob 'glissando-index))
           (stil (ly:line-spanner::print grob))
           (sign
             (lambda (x)
               (if (= x 0)
                   0
                   (if (< x 0) -1 1)))))
      (if (and (member gliss-count which) (ly:stencil? stil))
          (let* ((x-ext (ly:stencil-extent stil X))
                 (x-length (interval-length x-ext))
                 (left-bound-info (ly:grob-property grob 'left-bound-info))
                 (left-padding (assoc-get 'padding left-bound-info))
                 (Y-left (assoc-get 'Y left-bound-info))
                 (X-left (assoc-get 'X left-bound-info))
                 (right-bound-info (ly:grob-property grob 'right-bound-info))
                 (Y-right (assoc-get 'Y right-bound-info))
                 (right-padding (assoc-get 'padding right-bound-info))
                 (X-right (assoc-get 'X right-bound-info))
                 (thick (ly:grob-property grob 'thickness 0.12)))
            (ly:stencil-translate
              (make-tie-stencil
                (cons (- (+ (* thick 2) left-padding)) 0)
                (cons (+ x-length right-padding (* thick 2)) (- Y-right Y-left))
                thick
                (if (negative? Y-left) DOWN UP))
              (cons (car x-ext) (+ Y-left (sign Y-left)))));)
          #f))))

fingeringSlideEngraver =
#(lambda (context)
   (let ((glissandi '())
         (fingerings '()))
     (make-engraver
       (acknowledgers
         ((glissando-interface engraver grob source-engraver)
           (let* ((details (ly:grob-property grob 'details))
                  (slides? (assoc-get 'finger-slides details #f))
                  (which-bow-gliss
                    (assoc-get 'bow-glissando-indices details #f)))
             (if slides?
                 (set! glissandi (cons grob glissandi)))
             (if (and which-bow-gliss slides?)
                 (ly:grob-set-property! grob 'stencil
                   (replace-gliss-with-tie which-bow-gliss)))))
         ((finger-interface engraver grob source-engraver)
           (set! fingerings (cons grob fingerings))))
       ((stop-translation-timestep translator)
         (for-each
           (lambda (gliss)
             (for-each
               (lambda (finger)
                 (if (eq? (ly:spanner-bound gliss LEFT)
                          (ly:grob-parent finger X))
                     (ly:spanner-set-bound! gliss LEFT finger))
                 (if (eq? (ly:spanner-bound gliss RIGHT)
                          (ly:grob-parent finger X))
                     (begin
                       (ly:spanner-set-bound! gliss RIGHT finger)
                       (set! glissandi
                             (remove (lambda (x) (eq? x gliss)) glissandi)))))
               fingerings))
           glissandi)
         (set! fingerings '())))))

\layout {
  \context {
    \Voice
    \consists \fingeringSlideEngraver
  }
}

%% \set glissandoMap = #'(pair-1 pair-2) ended with \unset glissandoMap is
%% more flexible but not really needed for the fingeringSlideEngraver, thus:
connectGliss =
#(define-music-function (which) (number-list?)
  #{ \set glissandoMap = #(map (lambda (w) (cons w w)) which) #})

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXAMPLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

m = \relative c'' {
  <fis-2 d'-4>4\glissando
  <g,-2 d'-4>

  <fis-1 d'-3>4\glissando
  <g'-1 d'-3>
  \bar "||"
}

{
  %% default behaviour
  \m
  %% activate slides for fingerings
  \override Glissando.details.finger-slides = ##t
  \m
  %% select certain glissandos, drop the other
  \connectGliss 0
  \m
  \connectGliss 1
  \m
  \unset glissandoMap
  \break
  %% bowed slides, select with the provided list, unsuitable list-settings
  %% will be ignored
  \override Glissando.details.bow-glissando-indices = #'(0 1)
  \m
  \override Glissando.details.bow-glissando-indices = #'(0)
  \m
  \override Glissando.details.bow-glissando-indices = #'(1)
  \m

  %% line-breaks and skipping NoteColumns
  \override Glissando.details.bow-glissando-indices = #'(0 1)
  \override Glissando.breakable = ##t
  \override Glissando.after-line-breaking = ##f
  <fis''-2 d'''-4>1\glissando
  \once \override NoteColumn.glissando-skip = ##t
  <g'-2 d''-4>
  \break
  <fis''-1 d'''-3>1
}

{
  % fake what I want
  \override Fingering.avoid-slur = #'inside
  cis''-4( c''-4) r
  % ties possible on one note
  c''^\markup \finger \overtie "43"

  \revert Fingering.avoid-slur
  %\override Glissando.details.finger-slides = ##t
  %\override Glissando.details.bow-glissando-indices = #'(0 1)
  cis''-4 \glissando c''-4
}
