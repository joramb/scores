\version "2.25.22"

\include "layout.ily"

\paper {
  ragged-last-bottom = ##t
  indent = 0
}

music = {
  \omit Staff.TimeSignature
  \cadenzaOn
  \override Staff.Clef.full-size-change = ##t

  \clef "treble_8" s4
  \clef "treble^8" s
  \clef "bass_8" s
  \clef "bass^8" s
  \clef "alto_8" s
  \clef "alto^8" s
  \clef "tenor_8" s
  \clef "tenor^8" s

  \clef "treble_15" s4
  \clef "treble^15" s
  \clef "bass_15" s
  \clef "bass^15" s
  \clef "alto_15" s
  \clef "alto^15" s
  \clef "tenor_15" s
  \clef "tenor^15" s
}

\new Staff {
  \revert Staff.Clef.font-family
  \revert Staff.ClefModifier.font-series
  \revert Staff.ClefModifier.extra-offset
  \tempo "LilyPond defaults"
  \music
}

\new Staff {
  \tempo "Custom settings"
  \music
}
