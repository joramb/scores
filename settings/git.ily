\version "2.25.22"

% provides:
%  * \year (of file modification date)
%  * git commit: \gitshort (tag.0+) and \gitlong (tag.0-g12ab34+)

% from snippet 197 (more to see at http://lsr.di.unimi.it/LSR/Search ):
#(define comml   (object->string (command-line)))
#(define istart  (+ (string-rindex comml #\space ) 2))
#(define ilength (- (string-length comml) 2))
#(define filen   (substring comml istart ilength))                   % filename
#(define year    (strftime "%Y" (localtime (stat:mtime (stat filen)))))  % Command line

% gitrev (from http://lilypondblog.org/2014/01/why-use-version-control-for-engraving-scores/#more-2151 )
#(use-modules (ice-9 popen))
#(use-modules (ice-9 rdelim))

#(define (strsystem_internal cmd)
   (let* ((port (open-input-pipe cmd))
          (str (read-line port)))
     (close-pipe port)
     str))

#(define gitshort (let* ((res (strsystem_internal "git describe --long --dirty='+' | sed 's/-/./' | sed -r 's/-g[0-9a-f]+//g'")))
                    (if (string? res) res "")))
#(define gitlong  (let* ((res (strsystem_internal "git describe --long --dirty='+' | sed 's/-/./'")))
                    (if (string? res) res "")))
#(display gitshort)