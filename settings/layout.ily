% # LilyPond Style Sheet

% by Joram Berger, 2011 – 2020

\version "2.25.22"

#(ly:set-option 'relative-includes #t)

\include "git.ily"

%{
In order to encourage cherry-picking from this style,
this file is dedicated to the public domain according to
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).

This style sheet does the following overrides:

- use *Linux Libertine* as text font (and *Linux Biolinum*, *Ubuntu Mono*)
  but still print numbers with the default *TeX Gyre Schola* font
- reformat ottava texts (extended for all octavations)
- use the clef from the Cadence font (more straight central line)
  and move the clef modifier and make it bold
- toolkit: squeeze, arrows, dynamictext
- general improvements: 8va, lyricsnap, autoextender
- my style: fonts, margins
- personal: tagline, author, pdfauthor
- choir: midi, staffsize?,

The version statement, language selection and global staff size should go before
this include. Header and custom paper and layout blocks after this style sheet,
followed by the music variables and the score layout.

order (A = original file, B = this style sheet)
A1. version
A2. language
A3. global staff size
B1. default header
B2. paper fonts
B3. include font register
B4. layout
B5. definitions
A4. header
A5. paper
A6. parts
A7. score(s)
A8. midi
%}


% ## Default header fields

% This is a section of personal entries for the header fields.

\header {
  % PDF meta data
  author = "Joram Berger"
  keywords = #(string-append "Lizenz: CC BY-SA 4.0; git " gitlong)  % misuse, but copyright field is not shown in evince
  pdfcopyright = "© 2017 Joram Berger, Lizenz: CC BY-SA 4.0"
  % Mutopia meta data
  license = "cc-by-sa 4.0"
  maintainer = "Joram Berger"
  maintainerEmail = "joram-git@mailbox.org"
  maintainerWeb = "https://joramberger.de"

  % footer
  copyright = \markup \override #'(font-features . ("onum")) \concat {
    "© " \year " "
    \with-url "https://joramberger.de" "Joram Berger · joramberger.de" " · "
    \with-url "https://creativecommons.org/licenses/by-sa/4.0/deed.de"
    \line { "Lizenz:" \override #'(font-features . ("smcp")) "cc by-sa" }
    #(if (string=? gitshort "") "" " · ")
    \with-url "https://github.com/joram-berger/scores"
    #(if (string=? gitshort "") "" (format #f "Version ~a" gitshort))
  }
  tagline = \markup \override #'(font-features . ("onum"))
    \with-url "https://lilypond.org"
    #(format #f "Lilypond ~:@{~a.~a~}" (ly:version))
}

% ## Text and music fonts

% The LilyPond default font *Emmentaler* is used as music font with one
% exception: the clefs are taken from the *Greyerzer* font similar to the
% *Cadence* font. Many thanks to Abraham Lee.

% The *Linux Libertine*/*Linux Biolinum* combination is used as text font
% instead of *TeX Gyre Schola* to give the scores a more modern look.
% Any graphical objects that usually only show numbers, keep using the default
% font *TeX Gyre Schola* as numbers are easier to read in this font.

\paper {
  property-defaults.fonts.music = "emmentaler"
  property-defaults.fonts.serif = "Linux Libertine O, Vollkorn, serif"  % add alternatives for all OS!
  property-defaults.fonts.sans = "Linux Biolinum O, Helvetica, Arial, sans-serif"
  property-defaults.fonts.typewriter = "Ubuntu Mono, Consolas, Monaco, mono"
  property-defaults.fonts.cadence = "cadence"
  property-defaults.fonts.default = "Tex Gyre Schola"
}

% ## Paper margins

% LilyPonds default margins are very small. The following margins are a
% compromise between a balanced look (larger margins) and more available space
% (smaller margins).

% The header markup is completely rewritten to give the title of the piece a
% more prominent position and to center the page number to avoid optical
% clashes with bar numbers. The default footer markup remains unchanged.

\paper {
  top-margin = 15\mm
  bottom-margin = 8\mm
  left-margin = 15\mm
  right-margin = 15\mm
  ragged-last-bottom = ##f
  last-bottom-spacing.basic-distance = 12
  last-bottom-spacing.stretchability = 100
  evenHeaderMarkup = \markup \fill-line {
    \fromproperty #'header:title
    \bold \fromproperty #'page:page-number-string
    \fromproperty #'header:shortcomposer
  }
  oddHeaderMarkup = \markup \fill-line {
    \unless \on-first-page \fromproperty #'header:shortcomposer
    \unless \on-first-page \bold \fromproperty #'page:page-number-string
    \unless \on-first-page \fromproperty #'header:title
  }

}

%The author, the license, the program, the tradition...

% ## Graphical object overrides

% * Dotted ottava bracket
% * Reset font for numbers to *TeX Gyre Schola*
% * Use *Cadence* for clefs

\layout {
    %\override Score.OttavaBracket.style = #'dotted-line
    \override Score.OttavaBracket.dash-period = 0.4
    \override Score.OttavaBracket.dash-fraction = 0.01
    \override Score.OttavaBracket.edge-height = #'(0 . 0.8)
    \override Score.OttavaBracket.thickness = #2

    % classical text font mainly used for numbers:
    \override StrokeFinger.font-family = #'default
    \override TupletNumber.font-family = #'default
    \override TupletNumber.font-series = #'bold
    \override TupletNumber.font-size =-2.4
    %\override TupletNumber.outside-staff-priority = 100

    \override Staff.ClefModifier.font-family = #'default
    \override Staff.ClefModifier.font-series = #'bold
    \override Score.BarNumber.font-family = #'default
    \override Score.OttavaBracket.font-family = #'default
    \override Score.OttavaBracket.font-series = #'bold
    \override Score.BarNumber.padding = #1.6
    % music font for treble clef
    \override Staff.Clef.font-family = #'cadence
    \override Staff.CueClef.font-family = #'cadence
    \override Staff.CueEndClef.font-family = #'cadence
    \override Lyrics.LyricExtender.left-padding = 0.3
    % Lyrics font-size -0.5 (Kieren) bis 1 (default) vllt 0.5

  \context {
    \PianoStaff
    connectArpeggios = ##t
    \accidentalStyle "piano"
  }
}

#(define (conditional-kill-lyric-extender-callback . args)
  (lambda (grob)
   (let* ((orig (ly:grob-original grob))
	  (siblings (ly:spanner-broken-into orig))
          (minimum-length
           (if (null? args)
            (ly:grob-property grob 'minimum-length 0)
            (car args)))
          (X-extent (ly:stencil-extent (ly:grob-property grob 'stencil empty-stencil) X))
          (natural-length (- (cdr X-extent) (car X-extent))))
    (if (and (> minimum-length natural-length)
	 (<= (length siblings) 1)) ;; never kill a broken extender
     (ly:grob-suicide! grob)))))

\layout {
  \context {
    \Lyrics
    \override LyricExtender.minimum-length = 2.0
    \override LyricExtender.after-line-breaking = #(conditional-kill-lyric-extender-callback)
  }
}
% cf. piano-shorthands.ily

% ## Ottava Brackets

% Redefinition of the ottava function with bold text and superscripts.
% This change includes the definition of shifts of more than two octaves.
ottava =
#(define-music-function (octave) (integer?)
   (_i "Set the octavation.")
   #{
     #(make-music 'OttavaMusic 'ottava-number octave)
     \set Staff.ottavation =
     #(if (< octave -1) #{ \markup \concat { #(number->string (+ 1 (* -7 octave))) \fontsize #-2 "mb" } #}
      (if (= octave -1) #{ \markup \concat { "8" \fontsize #-2 "vb" } #}
      (if (= octave +0) #f
      (if (= octave +1) #{ \markup \concat { "8" \fontsize #-2 \translate-scaled #'(0 . 0.85) "va" } #}
                        #{ \markup \concat { #(number->string (+ 1 (* 7 octave))) \fontsize #-2 \translate-scaled #'(0 . 0.85) "ma" } #}
      ))))
   #})

\layout {
  \override Staff.ClefModifier.extra-offset =
  #(lambda (grob)
    (let* ((clef (ly:grob-parent grob X))
           (glyph (ly:grob-property clef 'glyph))
           (dir (ly:grob-property grob 'direction))
           (text (markup->string (ly:grob-property grob 'text))))
      (cond
        ;; treble clef (no good touching position for G^15)
        ((equal? (list glyph dir text) (list "clefs.G" UP "8"))    '(0.06 . -0.05))
        ((equal? (list glyph dir text) (list "clefs.G" DOWN "8"))  '(0.22 . 0.08))
        ((equal? (list glyph dir text) (list "clefs.G" UP "15"))   '(0.04 . 0.00))
        ((equal? (list glyph dir text) (list "clefs.G" DOWN "15")) '(0.10 . 0.12))
        ;; bass clef
        ((equal? (list glyph dir text) (list "clefs.F" UP "8"))    '(0.00 . -0.24))
        ((equal? (list glyph dir text) (list "clefs.F" DOWN "8")) '(-0.33 . 1.25))
        ((equal? (list glyph dir text) (list "clefs.F" UP "15"))   '(0.10 . -0.24))
        ((equal? (list glyph dir text) (list "clefs.F" DOWN "15")) '(0.20 . 1.25))
        ;; alto clefs look better with default padding
        (else '(0 . 0)))))
}

% ## Toolkit
arrowDown = \markup \center-column {
  \combine
  \override #'(thickness . 1.8)
  \draw-line #'(0 . 2.8)
  \override #'(thickness . 2)
  \arrow-head #Y #DOWN ##f
}

arrowUp = \markup \rotate #180 \arrowDown
arrowLeft = \markup \rotate #270 \arrowDown
arrowRight = \markup \rotate #90 \arrowDown

chordBracket = \tweak #'stencil #ly:arpeggio::brew-chord-bracket \arpeggio
arpeggioUp = \tweak #'arpeggio-direction #UP \arpeggio
arpeggioDown = \tweak #'arpeggio-direction #DOWN \arpeggio

% lyrics
align = #(define-music-function (amount) (number?)
#{ \once \override LyricText.self-alignment-X = #amount #})

melOff = { \set melismaBusyProperties = #'() }
melOn = { \unset melismaBusyProperties }

extendLV =
#(define-music-function (further) (number?)
#{
  \once \override LaissezVibrerTie.X-extent = #'(0 . 0)
  \once \override LaissezVibrerTie.details.note-head-gap = #(/ further -2)
  \once \override LaissezVibrerTie.extra-offset = #(cons (/ further 2) 0)
#})


% ClefModifier position corrections (obsolete?)

% Density
spacingDensity =
#(define-music-function (parser location num) (number?)
#{
\newSpacingSection
\override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 num)
#})

% sequeeze

squeezeNotation = {
\temporary \override Staff.AccidentalPlacement.right-padding = #-0.05
%% TODO: should use narrow accidentals when they're available,
%% http://code.google.com/p/lilypond/issues/detail?id=2203
\temporary \override Staff.Accidental.stencil =
#(lambda (grob)
(ly:stencil-scale (ly:accidental-interface::print grob) 0.92 1))
%% TODO: design a narrow notehead glyph??
\temporary \override Staff.NoteHead.stencil =
#(lambda (grob)
(ly:stencil-scale (ly:note-head::print grob) 0.96 1.02))
%% also, change tracking between letters (there is some snippet doing this)
\temporary \override Lyrics.LyricText.stencil =
#(lambda (grob)
(ly:stencil-scale (lyric-text::print grob) 0.92 1))
}

squeezeLyrics = {
\temporary \override Lyrics.LyricText.stencil = #(lambda (grob)
(ly:stencil-scale (lyric-text::print grob) 0.92 1))
}

rehearsalMidi =
#(define-music-function
  (parser location melody vTwo vThree vFour tempo lyrics midiInstrument)
  (ly:music? ly:music? ly:music? ly:music? number-or-pair? ly:music? string?)
  #{
  \unfoldRepeats <<
  \new Staff = "mel" \new Voice = "melo" { <>\f $melody }
  \new Staff = "alto" \new Voice = "alto" { s1*0\f $vTwo }
  \new Staff = "tenor" \new Voice = "tenor" { s1*0\f $vThree }
  \new Staff = "bass" \new Voice = "bass" { s1*0\f $vFour }
  \context Staff = "mel" {
  \set Score.midiMinimumVolume = #0.5
  \set Score.midiMaximumVolume = #0.5
  \set Score.tempoWholesPerMinute = #(ly:make-moment 65 2 0 0)
  \set Staff.midiMinimumVolume = #0.8
  \set Staff.midiMaximumVolume = #1.0
  \set Staff.midiInstrument = $midiInstrument
  }
  %%\new Lyrics \with {
  %%alignBelowContext = "soprano"
  %%} \lyricsto $name $lyrics
  >>
  #})

% [view original file](http://joramberger.de/files/stylesheet.ily)
